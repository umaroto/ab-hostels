<?php
Router::connect('/', array('controller' => 'pages', 'action' => 'index'));
Router::connect('/noticias/:slug', array('controller' => 'blogs', 'action' => 'interna'), array('pass' => array('slug')));
Router::connect('/noticias', array('controller' => 'blogs', 'action' => 'index'));

Router::connect('/associados/reserva', array('controller' => 'associados', 'action' => 'reserva'));
Router::connect('/associados/interna', array('controller' => 'associados', 'action' => 'interna'));
Router::connect('/associados/getAssociados', array('controller' => 'associados', 'action' => 'getAssociados'));
Router::connect('/associados/:slug', array('controller' => 'associados', 'action' => 'interna'), array('pass' => array('slug')));
Router::connect('/destinos', array('controller' => 'destinos', 'action' => 'index'));

Router::connect('/admin', array('controller' => 'associados', 'action' => 'index', 'admin' => true));
Router::connect('/institucional', array('controller' => 'pages', 'action' => 'institucional'));

Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

Router::parseExtensions('json');

CakePlugin::routes();

require CAKE . 'Config' . DS . 'routes.php';