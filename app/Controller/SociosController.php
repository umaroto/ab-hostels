<?php
App::uses('AppController', 'Controller');

class SociosController extends AppController {

	public $paginate = array(
		'limit' => 20,
		'order' => array(
			'Socio.id' => 'desc'
		)
	);

	public function index() {
		if ($this->request->is('post') || $this->request->is('put')){
			if(isset($this->request->data['Socio']['alvara']) && !empty($this->request->data['Socio']['alvara']['name'])){
				$this->request->data['Socio']['alvara'] = $this->Socio->upload($this->request->data['Socio']['alvara'], 'images/socios');
			} else {
				unset($this->request->data['Socio']['alvara']);
			}

			if ($this->Socio->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'messages/sucess');
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'messages/error');
			}
		}
	}

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Socio.id' => $this->request->data['Socio']['id'],
						'Socio.nome LIKE "%'.$this->request->data['Socio']['nome'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$socios = $this->Paginator->paginate('Socio');

		$this->set(compact('socios'));
	}

	public function admin_add() {
		if ($this->request->is('post') || $this->request->is('put')){
			
			if(isset($this->request->data['Socio']['arquivo']) && !empty($this->request->data['Socio']['arquivo']['name'])){
				$this->request->data['Socio']['arquivo'] = $this->Socio->upload($this->request->data['Socio']['arquivo'], 'images/socios', 150, 150);
			} else {
				unset($this->request->data['Socio']['arquivo']);
			}

			if ($this->Socio->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'admin/sucess');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		}		
	}

	public function admin_edit($id = null) {
		$this->request->data = $this->Socio->find('first', array(
			'conditions' => array(
				'Socio.id' => $id,
			),
			'recursive' => -1
		));

		$this->render('admin_add');
	}

	public function admin_search() {
		$this->render(false);
		$socio = $this->Socio->find('all', array(
			'conditions' => array(
				'Socio.nome LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Socio.id', 'Socio.nome'),
			'recursive' => -1
		));
		$socio = Set::classicExtract($socio, '{n}.Socio');
		$socios = array_map(function($socio) {
			return array(
				'id' => $socio['id'],
				'value' => $socio['nome']
			);
		}, $socio);
		echo json_encode($socios);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			$foto = $this->Socio->findById($id);

			if($this->Socio->delete($id)){
				if(isset($foto['Socio']['arquivo']) && !empty($foto['Socio']['arquivo'])){
					@unlink(WWW_ROOT . 'images/socios/'.$foto['Socio']['arquivo']);
				}
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}