<?php
App::uses('AppController', 'Controller');

class BlogsController extends AppController {

	public $paginate = array(
		'limit' => 10,
		'order' => array(
			'Blog.id' => 'desc'
		)
	);

	private function checkSlug($slug, $id){
		$blog = false;

		do {
			$exist = $this->Blog->find('first', array(
				'conditions' => array(
					'Blog.slug' => $slug,
					'Blog.id != ' . $id
				)
			));

			if($exist){
				$slug .= '_';
				$blog = true;
			} else {
				$blog = false;
			}
		} while ($blog == true);

		return $slug;
	}

	public function index() {
		$blogs = $this->Blog->find('all', array(
			'order' => array('Blog.id DESC')
		));
		$this->set(compact('blogs'));
	}

	public function interna() {
		$blog = $this->Blog->findBySlug($this->request->params['slug']);
		$this->set(compact('blog'));

		$this->set('og_url', Router::url('/', array('full' => true)).'noticias/'.$blog['Blog']['slug']);
		$this->set('og_type', 'website');
		$this->set('og_title', $blog['Blog']['titulo']);
		$this->set('og_description', strip_tags($blog['Blog']['texto']));
		$this->set('og_image', Router::url('/', array('full' => true)).'images/blog/'.$blog['Blog']['arquivo']);
	}

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Blog.id' => $this->request->data['Blog']['id'],
						'Blog.titulo LIKE "%'.$this->request->data['Blog']['titulo'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$blogs = $this->Paginator->paginate('Blog');

		$this->set(compact('blogs'));
	}

	public function admin_add() {
		if ($this->request->is('post') || $this->request->is('put')){
			
			if(isset($this->request->data['Blog']['arquivo']) && !empty($this->request->data['Blog']['arquivo']['name'])){
				$this->request->data['Blog']['arquivo'] = $this->Blog->upload($this->request->data['Blog']['arquivo'], 'images/blog');
			} else {
				unset($this->request->data['Blog']['arquivo']);
			}

			$id = isset($this->request->data['Blog']['id']) && !empty($this->request->data['Blog']['id']) ? $this->request->data['Blog']['id'] : 0;
			$this->request->data['Blog']['slug'] = $this->checkSlug(strtolower(Inflector::slug($this->request->data['Blog']['titulo'], '-')), $id);

			if ($this->Blog->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'admin/sucess');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		}
	}

	public function admin_edit($id = null) {
		$this->request->data = $this->Blog->find('first', array(
			'conditions' => array(
				'Blog.id' => $id,
			),
			'recursive' => -1
		));

		$this->render('admin_add');
	}

	public function admin_search() {
		$this->render(false);
		$blog = $this->Blog->find('all', array(
			'conditions' => array(
				'Blog.titulo LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Blog.id', 'Blog.titulo'),
			'recursive' => -1
		));
		$blog = Set::classicExtract($blog, '{n}.Blog');
		$blogs = array_map(function($blog) {
			return array(
				'id' => $blog['id'],
				'value' => $blog['titulo']
			);
		}, $blog);
		echo json_encode($blogs);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			$foto = $this->Blog->findById($id);

			if($this->Blog->delete($id)){
				if(isset($foto['Blog']['arquivo']) && !empty($foto['Blog']['arquivo'])){
					@unlink(WWW_ROOT . 'images/blog/'.$foto['Blog']['arquivo']);
				}
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}