<?php
App::uses('AppController', 'Controller');

class ParceirosController extends AppController {

	public $paginate = array(
		'limit' => 20,
		'order' => array(
			'Parceiro.id' => 'desc'
		)
	);

	public function index() {
		$parceiros = $this->Parceiro->find('all', array(
			'order' => 'RAND()',
		));

		$this->set(compact('parceiros'));
	}

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Parceiro.id' => $this->request->data['Parceiro']['id'],
						'Parceiro.nome LIKE "%'.$this->request->data['Parceiro']['nome'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$parceiros = $this->Paginator->paginate('Parceiro');

		$this->set(compact('parceiros'));
	}

	public function admin_add() {
		if ($this->request->is('post') || $this->request->is('put')){
			
			if(isset($this->request->data['Parceiro']['arquivo']) && !empty($this->request->data['Parceiro']['arquivo']['name'])){
				$this->request->data['Parceiro']['arquivo'] = $this->Parceiro->upload($this->request->data['Parceiro']['arquivo'], 'images/parceiros', 255, 255);
			} else {
				unset($this->request->data['Parceiro']['arquivo']);
			}

			if ($this->Parceiro->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'admin/sucess');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		}
	}

	public function admin_edit($id = null) {
		$this->request->data = $this->Parceiro->find('first', array(
			'conditions' => array(
				'Parceiro.id' => $id,
			),
			'recursive' => -1
		));

		$this->render('admin_add');
	}

	public function admin_search() {
		$this->render(false);
		$parceiro = $this->Parceiro->find('all', array(
			'conditions' => array(
				'Parceiro.nome LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Parceiro.id', 'Parceiro.nome'),
			'recursive' => -1
		));
		$parceiro = Set::classicExtract($parceiro, '{n}.Parceiro');
		$parceiros = array_map(function($parceiro) {
			return array(
				'id' => $parceiro['id'],
				'value' => $parceiro['nome']
			);
		}, $parceiro);
		echo json_encode($parceiros);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			$foto = $this->Parceiro->findById($id);

			if($this->Parceiro->delete($id)){
				if(isset($foto['Parceiro']['arquivo']) && !empty($foto['Parceiro']['arquivo'])){
					@unlink(WWW_ROOT . 'images/parceiros/'.$foto['Parceiro']['arquivo']);
				}
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}