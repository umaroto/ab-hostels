<?php
App::uses('AppController', 'Controller');

class DepoimentosController extends AppController {

	public $paginate = array(
		'limit' => 20,
		'order' => array(
			'Depoimento.id' => 'desc'
		)
	);

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Depoimento.id' => $this->request->data['Depoimento']['id'],
						'Depoimento.nome LIKE "%'.$this->request->data['Depoimento']['nome'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$depoimentos = $this->Paginator->paginate('Depoimento');

		$this->set(compact('depoimentos'));
	}

	public function admin_add() {
		if ($this->request->is('post') || $this->request->is('put')){
			
			if(isset($this->request->data['Depoimento']['arquivo']) && !empty($this->request->data['Depoimento']['arquivo']['name'])){
				$this->request->data['Depoimento']['arquivo'] = $this->Depoimento->upload($this->request->data['Depoimento']['arquivo'], 'images/depoimentos', 150, 150);
			} else {
				unset($this->request->data['Depoimento']['arquivo']);
			}

			if ($this->Depoimento->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'admin/sucess');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		}
	}

	public function admin_edit($id = null) {
		$this->request->data = $this->Depoimento->find('first', array(
			'conditions' => array(
				'Depoimento.id' => $id,
			),
			'recursive' => -1
		));

		$this->render('admin_add');
	}

	public function admin_search() {
		$this->render(false);
		$depoimento = $this->Depoimento->find('all', array(
			'conditions' => array(
				'Depoimento.nome LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Depoimento.id', 'Depoimento.nome'),
			'recursive' => -1
		));
		$depoimento = Set::classicExtract($depoimento, '{n}.Depoimento');
		$depoimentos = array_map(function($depoimento) {
			return array(
				'id' => $depoimento['id'],
				'value' => $depoimento['nome']
			);
		}, $depoimento);
		echo json_encode($depoimentos);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			$foto = $this->Depoimento->findById($id);

			if($this->Depoimento->delete($id)){
				if(isset($foto['Depoimento']['arquivo']) && !empty($foto['Depoimento']['arquivo'])){
					@unlink(WWW_ROOT . 'images/depoimentos/'.$foto['Depoimento']['arquivo']);
				}
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}