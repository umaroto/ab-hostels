<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

	public $paginate = array(
		'limit' => 20,
		'order' => array(
			'User.name' => 'asc'
		)
	);

	public function admin_login() {
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['User']['username'] = $this->request->data['login-username'];
			$this->request->data['User']['password'] = $this->request->data['login-password'];

			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->loginRedirect);
			} else {
				$this->Session->setFlash(__('Usuário ou senha inválidos'), 'admin/error');
			}
		}

		$this->layout = 'login';
	}

	public function admin_logout() {
		$this->redirect($this->Auth->logout());
	}

	public function admin_index() {
		if($this->Session->read('Auth.User.role') > 1){
			$this->Session->setFlash(__('Erro Interno'), 'admin/error');
			return $this->redirect(array('controller' => 'users', 'action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')){
			if(isset($this->request->data['User']['id']) && !empty($this->request->data['User']['id'])){
				$this->Paginator->settings = array(
					'conditions' => array(
						'User.id' => $this->request->data['User']['id'],
						'User.role >= ' . $this->Session->read('Auth.User.role')
					)
				);
			} else {
				$this->Paginator->settings = array(
					'conditions' => array(
						'User.name LIKE "%'.$this->request->data['User']['name'].'%"',
						'User.role >= ' . $this->Session->read('Auth.User.role')
					)
				);
			}

			unset($this->request->data);
		} else {
			$this->Paginator->settings = array(
				'conditions' => array(
					'User.role >= ' . $this->Session->read('Auth.User.role')
				)
			);
		}

		$users = $this->Paginator->paginate('User');
		$this->set(compact('users'));
	}

	public function admin_add() {
		if($this->Session->read('Auth.User.role') > 1){
			$this->Session->setFlash(__('Erro Interno'), 'admin/error');
			return $this->redirect(array('controller' => 'users', 'action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')){
			if(empty($this->request->data['User']['password'])){
				unset($this->request->data['User']['password']);
			}
			
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('User salvo com sucesso.'), 'admin/sucess');
				return $this->redirect(array('controller' => 'users', 'action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		}
	}

	public function admin_edit($id = null) {
		if($this->Session->read('Auth.User.role') > 1){
			$this->Session->setFlash(__('Erro Interno'), 'admin/error');
			return $this->redirect(array('controller' => 'users', 'action' => 'index'));
		}

		$this->request->data = $this->User->find('first', array(
			'conditions' => array(
				'User.id' => $id,
			),
			'recursive' => -1
		));

		$nivel = $this->Session->read('Auth.User.role');

		if($this->request->data['User']['role'] < $nivel){
			$this->Session->setFlash(__('Erro Interno'), 'admin/error');
			return $this->redirect(array('controller' => 'users', 'action' => 'index'));
		}

		unset($this->request->data['User']['password']);

		$this->render('admin_add');
	}

	public function admin_search() {
		$this->render(false);
		$user = $this->User->find('all', array(
			'conditions' => array(
				'User.name LIKE "%'.$_GET['term'].'%"',
				'User.role >= ' . $this->Session->read('Auth.User.role')
			),
			'fields' => array('User.id', 'User.name'),
			'recursive' => -1
		));
		$user = Set::classicExtract($user, '{n}.User');
		$users = array_map(function($user) {
			return array(
				'id' => $user['id'],
				'value' => $user['name']
			);
		}, $user);
		echo json_encode($users);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			if($this->User->delete($id)){
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}