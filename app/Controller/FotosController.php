<?php
App::uses('AppController', 'Controller');

class FotosController extends AppController {

	public $paginate = array(
		'limit' => 20,
		'order' => array(
			'Foto.id' => 'desc'
		)
	);

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Foto.id' => $this->request->data['Foto']['id'],
						'Foto.nome LIKE "%'.$this->request->data['Foto']['nome'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$fotos = $this->Paginator->paginate('Foto');
		$this->set(compact('fotos'));
	}

	public function admin_add() {
		if ($this->request->is('post') || $this->request->is('put')){
			
			if(isset($this->request->data['Foto']['arquivo']) && !empty($this->request->data['Foto']['arquivo']['name'])){
				$this->request->data['Foto']['arquivo'] = $this->Foto->upload($this->request->data['Foto']['arquivo'], 'images/fotos', 1350, 520);
			} else {
				unset($this->request->data['Foto']['arquivo']);
			}

			if ($this->Foto->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'admin/sucess');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		}		
	}

	public function admin_search() {
		$this->render(false);
		$foto = $this->Foto->find('all', array(
			'conditions' => array(
				'Foto.nome LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Foto.id', 'Foto.nome'),
			'recursive' => -1
		));
		$foto = Set::classicExtract($foto, '{n}.Foto');
		$fotos = array_map(function($foto) {
			return array(
				'id' => $foto['id'],
				'value' => $foto['nome']
			);
		}, $foto);
		echo json_encode($fotos);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			$foto = $this->Foto->findById($id);

			if($this->Foto->delete($id)){
				if(isset($foto['Foto']['arquivo']) && !empty($foto['Foto']['arquivo'])){
					@unlink(WWW_ROOT . 'images/fotos/'.$foto['Foto']['arquivo']);
				}
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}