<?php
App::uses('AppController', 'Controller');

class AssociadosController extends AppController {

	public $paginate = array(
		'limit' => 20,
		'order' => array(
			'Associado.id' => 'desc'
		)
	);

	public function index() {
		$this->LoadModel('Destino');
		$destinos = $this->Destino->find('all');

		$associados = $this->Associado->find('all', array(
			'order' => 'RAND()',
		));

		$this->set(compact('associados', 'destinos'));
	}

	public function reserva() {
		$this->layout = 'ajax';

		if(isset($this->request->data['Associado']['Hostel'])){
			$associado = $this->Associado->findById($this->request->data['Associado']['Hostel']);

			$de = implode('-', array_reverse(explode('/', $this->request->data['Associado']['de'])));
			$ate = implode('-', array_reverse(explode('/', $this->request->data['Associado']['ate'])));

			if($associado){
				if(isset($associado['Associado']['reserva']) && !empty($associado['Associado']['reserva'])){
					if(strpos('https://reservation.booking.expert', $associado['Associado']['reserva']) === true){
						header('Location:'.$associado['Associado']['reserva'].'checkin='.$de.'&checkout='.$ate);
					} else {
						header('Location:'.$associado['Associado']['reserva']);
					}
					exit;
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				return $this->redirect(array('action' => 'index'));
			}
		}
	}

	public function interna() {
		$this->LoadModel('Destino');

		if ($this->request->is('post') || $this->request->is('put')){
			$associados = $this->Associado->find('all', array(
				'conditions' => array(
					'Associado.destino_id' => $this->request->data['Associado']['CidadeId']
				),
				'order' => 'RAND()',
			));

			$dest = $this->Destino->find('first', array(
				'conditions' => array(
					'Destino.id' => $this->request->data['Associado']['CidadeId']
				)
			));
		}
		else
		if(isset($this->request->params['slug']) && !empty($this->request->params['slug'])){
			$associados = $this->Associado->find('all', array(
				'conditions' => array(
					'Destino.slug' => $this->request->params['slug']
				),
				'order' => 'RAND()',
			));

			$dest = $this->Destino->find('first', array(
				'conditions' => array(
					'Destino.slug' => $this->request->params['slug']
				)
			));
		}

		$destinos = $this->Destino->find('all');
		
		$this->set(compact('associados', 'destinos', 'dest'));
	}

	public function getAssociados(){
		$this->layout = 'ajax';
		
		if(isset($this->request->data['cidade'])){
			$associados = $this->Associado->find('all', array(
				'conditions' => array(
					'Associado.destino_id' => $this->request->data['cidade'],
				),
				'order' => array('Associado.nome'),
				'recursive' => -1
			));

			$this->set(compact('associados'));
		}
	}

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Associado.id' => $this->request->data['Associado']['id'],
						'Associado.nome LIKE "%'.$this->request->data['Associado']['nome'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$associados = $this->Paginator->paginate('Associado');
		$this->set(compact('associados'));
	}

	public function admin_add() {
		if ($this->request->is('post') || $this->request->is('put')){
			
			if(isset($this->request->data['Associado']['arquivo']) && !empty($this->request->data['Associado']['arquivo']['name'])){
				$this->request->data['Associado']['arquivo'] = $this->Associado->upload($this->request->data['Associado']['arquivo'], 'images/associados', 150, 150);
			} else {
				unset($this->request->data['Associado']['arquivo']);
			}

			if ($this->Associado->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'admin/sucess');
				return $this->redirect(array('controller' => 'associados', 'action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		}

		$this->LoadModel('Destino');
		$destinos = $this->Destino->find('list');

		$this->set(compact('destinos'));
	}

	public function admin_edit($id = null) {
		$this->request->data = $this->Associado->find('first', array(
			'conditions' => array(
				'Associado.id' => $id,
			),
			'recursive' => -1
		));

		$this->LoadModel('Destino');
		$destinos = $this->Destino->find('list');

		$this->set(compact('destinos'));

		$this->render('admin_add');
	}

	public function admin_search() {
		$this->render(false);
		$associado = $this->Associado->find('all', array(
			'conditions' => array(
				'Associado.nome LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Associado.id', 'Associado.nome'),
			'recursive' => -1
		));
		$associado = Set::classicExtract($associado, '{n}.Associado');
		$associados = array_map(function($associado) {
			return array(
				'id' => $associado['id'],
				'value' => $associado['nome']
			);
		}, $associado);
		echo json_encode($associados);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			$foto = $this->Associado->findById($id);

			if($this->Associado->delete($id)){
				if(isset($foto['Associado']['arquivo']) && !empty($foto['Associado']['arquivo'])){
					@unlink(WWW_ROOT . 'images/associados/'.$foto['Associado']['arquivo']);
				}
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}