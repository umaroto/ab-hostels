<?php
App::import('Vendor', 'Swift', array('file' => 'SwiftMailer' . DS . 'swift_required.php'));

class SwiftMailerComponent extends Component {
	var $__controller = null;
	var $__plugins = array();
	var $layout = 'default';
	var $viewPath = 'Emails';
	var $sendAs = 'html';
	var $bodyCharset = 'utf-8';
	var $subjectCharset = 'utf-8';
	var $smtpType = null;
	var $smtpUsername = 'envio@abhostels.com.br';
	var $smtpPassword = 'D5lB#=-z]CjI';
	var $smtpHost = 'mail.abhostels.com.br';
	var $smtpPort = 587;
	var $smtpTimeout = 10;
	var $sendmailCmd = null;
	var $from = 'envio@abhostels.com.br';
	var $fromName = 'Innside';
	var $to = 'abhostels@gmail.com;';
	var $cc = null;
	var $bcc = null;
	var $attachments = array();
	var $readNotifyReceipt = null;
	var $replyTo = null;

	var $maxLineLength = 78;
	var $postErrors = array();

	function initialize($controller) {
		$this->__controller = $controller;
	}

	function _emailBodyPart($template, $type = 'html') {
		$viewClass = $this->__controller->viewClass;
		if ($viewClass != 'View') {
			if (strpos($viewClass, '.') !== false) {
				list($plugin, $viewClass) = explode('.', $viewClass);
			}
			$viewClass = $viewClass . 'View';
			App::import('View', $this->__controller->viewClass);
		}
		$View = new $viewClass($this->__controller, false);
		$View->layout = $this->layout;
		$content = $View->element('../'.$this->viewPath.DS.$type.DS.$template);
		$View->layoutPath = $this->viewPath.DS.$type;
		$content = $View->renderLayout($content);
		// Run content check callback
		$this->__runCallback($content, 'checkContent');
		return $content;
	}

	function send($view = 'default', $subject = '', $method = 'smtp', $batch = false) {
		// Check subject charset, asuming we are by default using "utf-8"
		if (strtolower($this->subjectCharset) != 'utf-8') {
			if (function_exists('mb_convert_encoding')) {
				//outlook uses subject in diferent encoding, this is the case to change it
				$subject = mb_convert_encoding($subject, $this->subjectCharset, 'utf-8');
			}
		}
		// Check if swift mailer is imported
		if (!class_exists('Swift_Message')) {
			throw new Exception('SwiftMailer was not included, check the path and filename');
		}
		// Create message
		$message = Swift_Message::newInstance($subject);
		// Run Init Callback
		$this->__runCallback($message, 'initializeMessage');
		$message->setCharset($this->subjectCharset);
		// Add html text
		if ($this->sendAs == 'both' || $this->sendAs == 'html') {
			$html_part = $this->_emailBodyPart($view, 'html');
			$message->addPart($html_part, 'text/html', $this->bodyCharset);
			unset($html_part);
		}
		// Add plain text or an alternative
		if ($this->sendAs == 'both' || $this->sendAs == 'text') {
			$text_part = $this->_emailBodyPart($view, 'text');
			$message->addPart($text_part, 'text/plain', $this->bodyCharset);
			unset($text_part);
		}
		// Add attachments if any
		if (!empty($this->attachments)) {
			foreach($this->attachments as $attachment) {
				if (!file_exists($attachment)) {
					continue;
				}
				$message->attach(Swift_Attachment::fromPath($attachment));
			}
		}
		// On read notification if supported
		if (!empty($this->readNotifyReceipt)) {
			$message->setReadReceiptTo($this->readNotifyReceipt);
		}
		$message->setMaxLineLength($this->maxLineLength);
		// Set the FROM address/name.
		$message->setFrom($this->from, $this->fromName);
		// Add all TO recipients.
		if (!empty($this->to)) {
			if (is_array($this->to)) {
				foreach($this->to as $address => $name) {
					$message->addTo($address, $name);
				}
			}
			else {
				$message->addTo($this->to);
			}
		}
		// Add all CC recipients.
		if (!empty($this->cc)) {
			if (is_array($this->cc)) {
				foreach($this->cc as $address => $name) {
					$message->addCc($address, $name);
				}
			}
			else {
				$message->addCc($this->cc);
			}
		}
		// Add all BCC recipients.
		if (!empty($this->bcc)) {
			if (is_array($this->bcc)) {
				foreach($this->bcc as $address => $name) {
					$message->addBcc($address, $name);
				}
			}
			else {
				$message->addBcc($this->bcc);
			}
		}
		// Set REPLY TO addresses
		if (!empty($this->replyTo)) {
			if (is_array($this->replyTo)) {
				foreach($this->replyTo as $address => $name) {
					$message->addReplyTo($address, $name);
				}
			}
			else {
				$message->addReplyTo($this->replyTo);
			}
		}
		// Initializing mail method object with sending parameters
		$transport = null;
		switch ($method) {
			case 'smtp':
				$transport = Swift_SmtpTransport::newInstance($this->smtpHost, $this->smtpPort, $this->smtpType);
				$transport->setTimeout($this->smtpTimeout);
				if (!empty($this->smtpUsername)) {
					$transport->setUsername($this->smtpUsername);
					$transport->setPassword($this->smtpPassword);
				}
				break;
			case 'sendmail':
				$transport = Swift_SendmailTransport::newInstance($this->sendmailCmd);
				break;
			case 'native': default:
				$transport = Swift_MailTransport::newInstance();
				break;
		}
		// Initialize Mailer
		$mailer = Swift_Mailer::newInstance($transport);
		// Load plugins if any
		if (!empty($this->__plugins)) {
			foreach($this->__plugins as $name => $args) {
				$plugin_class = "Swift_Plugins_{$name}";
				if (!class_exists($plugin_class)) {
					throw new Exception("SwiftMailer library does not support this plugin: {$plugin_class}");
				}
				$plugin = null;
				switch(count($args)) {
					case 1:
						$plugin = new $plugin_class($args[0]);
						break;
					case 2:
						$plugin = new $plugin_class($args[0], $args[1]);
						break;
					case 3:
						$plugin = new $plugin_class($args[0], $args[1], $args[2]);
						break;
					case 4:
						$plugin = new $plugin_class($args[0], $args[1], $args[2], $args[3]);
						break;
					default:
						throw new Exception('SwiftMailer component plugin can register maximum of 4 arguments');
				}
				$mailer->registerPlugin($plugin);
			}
		}
		// Run Send Callback
		$this->__runCallback($message, 'beforeSend');
		// Attempt to send the email.
		if ($batch){
			return $mailer->batchSend($message, $this->postErrors);
		} else {
			return $mailer->send($message, $this->postErrors);
		}
	}

	function init_swiftmail($method = 'smtp'){
		// Initializing mail method object with sending parameters
		$transport = null;
		switch ($method) {
			case 'smtp':
				$transport = Swift_SmtpTransport::newInstance($this->smtpHost, $this->smtpPort, $this->smtpType);
				$transport->setTimeout($this->smtpTimeout);
				if (!empty($this->smtpUsername)) {
					$transport->setUsername($this->smtpUsername);
					$transport->setPassword($this->smtpPassword);
				}
				break;
			case 'sendmail':
				$transport = Swift_SendmailTransport::newInstance($this->sendmailCmd);
				break;
			case 'native': default:
				$transport = Swift_MailTransport::newInstance();
				break;
		}
		// Initialize Mailer
		$mailer = Swift_Mailer::newInstance($transport);
		// Load plugins if any
		if (!empty($this->__plugins)) {
			foreach($this->__plugins as $name => $args) {
				$plugin_class = "Swift_Plugins_{$name}";
				if (!class_exists($plugin_class)) {
					throw new Exception("SwiftMailer library does not support this plugin: {$plugin_class}");
				}
				$plugin = null;
				switch(count($args)) {
					case 1:
						$plugin = new $plugin_class($args[0]);
						break;
					case 2:
						$plugin = new $plugin_class($args[0], $args[1]);
						break;
					case 3:
						$plugin = new $plugin_class($args[0], $args[1], $args[2]);
						break;
					case 4:
						$plugin = new $plugin_class($args[0], $args[1], $args[2], $args[3]);
						break;
					default:
						throw new Exception('SwiftMailer component plugin can register maximum of 4 arguments');
				}
				$mailer->registerPlugin($plugin);
			}
		}
		return $mailer;
	}

	function fastsend($view = 'default', $subject = '', $mailer) {
		// Check if $mailer exists
		if (!$mailer) {
			throw new Exception('SwiftMailer mailer does not exist, need to start it');
		}
		// Check subject charset, asuming we are by default using "utf-8"
		if (strtolower($this->subjectCharset) != 'utf-8') {
			if (function_exists('mb_convert_encoding')) {
				//outlook uses subject in diferent encoding, this is the case to change it
				$subject = mb_convert_encoding($subject, $this->subjectCharset, 'utf-8');
			}
		}
		// Check if swift mailer is imported
		if (!class_exists('Swift_Message')) {
			throw new Exception('SwiftMailer was not included, check the path and filename');
		}
		// Create message
		$message = Swift_Message::newInstance($subject);
		// Run Init Callback
		$this->__runCallback($message, 'initializeMessage');
		$message->setCharset($this->subjectCharset);
		// Add html text
		if ($this->sendAs == 'both' || $this->sendAs == 'html') {
			$html_part = $this->_emailBodyPart($view, 'html');
			$message->addPart($html_part, 'text/html', $this->bodyCharset);
			unset($html_part);
		}
		// Add plain text or an alternative
		if ($this->sendAs == 'both' || $this->sendAs == 'text') {
			$text_part = $this->_emailBodyPart($view, 'text');
			$message->addPart($text_part, 'text/plain', $this->bodyCharset);
			unset($text_part);
		}
		// Add attachments if any
		if (!empty($this->attachments)) {
			foreach($this->attachments as $attachment) {
				if (!file_exists($attachment)) {
					continue;
				}
				$message->attach(Swift_Attachment::fromPath($attachment));
			}
		}
		// On read notification if supported
		if (!empty($this->readNotifyReceipt)) {
			$message->setReadReceiptTo($this->readNotifyReceipt);
		}
		$message->setMaxLineLength($this->maxLineLength);
		// Set the FROM address/name.
		$message->setFrom($this->from, $this->fromName);
		// Add all TO recipients.
		if (!empty($this->to)) {
			if (is_array($this->to)) {
				foreach($this->to as $address => $name) {
					$message->addTo($address, $name);
				}
			}
			else {
				$message->addTo($this->to);
			}
		}
		// Add all CC recipients.
		if (!empty($this->cc)) {
			if (is_array($this->cc)) {
				foreach($this->cc as $address => $name) {
					$message->addCc($address, $name);
				}
			}
			else {
				$message->addCc($this->cc);
			}
		}
		// Add all BCC recipients.
		if (!empty($this->bcc)) {
			if (is_array($this->bcc)) {
				foreach($this->bcc as $address => $name) {
					$message->addBcc($address, $name);
				}
			}
			else {
				$message->addBcc($this->bcc);
			}
		}
		// Set REPLY TO addresses
		if (!empty($this->replyTo)) {
			if (is_array($this->replyTo)) {
				foreach($this->replyTo as $address => $name) {
					$message->addReplyTo($address, $name);
				}
			}
			else {
				$message->addReplyTo($this->replyTo);
			}
		}
		// Run Send Callback
		$this->__runCallback($message, 'beforeSend');
		// Attempt to send the email.
		return $mailer->send($message, $this->postErrors);
	}

	function registerPlugin() {
		if (func_num_args()) {
			$args = func_get_args();
			$this->__plugins[array_shift($args)] = $args;
			return true;
		}
		return false;
	}

	function __runCallback(&$object, $type) {
		$call = '__'.$type.'On'.Inflector::camelize($this->__controller->action);
		if (method_exists($this->__controller, $call)) {
			$this->__controller->{$call}($object);
		}
	}
}
?>