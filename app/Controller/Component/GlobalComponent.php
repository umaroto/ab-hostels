<?php
class GlobalComponent extends Component {

	public static function formataData($data, $tipo = 'pt'){

		$dt = explode(' ', $data);

		switch ($tipo) {
			case 'pt':
				$data = explode('-', $dt[0]);
				switch ($data[1]) {
					case "01": $data[1] = 'Janeiro'; break;
					case "02": $data[1] = 'Fevereiro'; break;
					case "03": $data[1] = 'Março'; break;
					case "04": $data[1] = 'Abril'; break;
					case "05": $data[1] = 'Maio'; break;
					case "06": $data[1] = 'Junho'; break;
					case "07": $data[1] = 'Julho'; break;
					case "08": $data[1] = 'Agosto'; break;
					case "09": $data[1] = 'Setembro'; break;
					case "10": $data[1] = 'Outubro'; break;
					case "11": $data[1] = 'Novembro'; break;
					case "12": $data[1] = 'Dezembro'; break; 
				}
				return $data[2].' de '.$data[1].' de '.$data[0];
				break;
			case 'mysql':
				return implode('-', array_reverse(explode('/', $data)));
				break;
			case 'mysql_hour_full':
				return implode('-', array_reverse(explode('/', $dt[0]))) . ' ' . substr($dt[1], 0, 5) . ':00';
				break;	
			case 'html':
				return implode('/', array_reverse(explode('-', $data)));
				break;
			case 'pt_hour':
				return implode('/', array_reverse(explode('-', $dt[0]))) . ' as ' . substr($dt[1], 0, 5) . 'h';
				break;
			case 'pt_hour_simple':
				return implode('/', array_reverse(explode('-', $dt[0]))) . ' ' . substr($dt[1], 0, 5);
				break;
			case 'mes':
				switch ($data) {
					case "01": return 'Janeiro'; break;
					case "02": return 'Fevereiro'; break;
					case "03": return 'Março'; break;
					case "04": return 'Abril'; break;
					case "05": return 'Maio'; break;
					case "06": return 'Junho'; break;
					case "07": return 'Julho'; break;
					case "08": return 'Agosto'; break;
					case "09": return 'Setembro'; break;
					case "10": return 'Outubro'; break;
					case "11": return 'Novembro'; break;
					case "12": return 'Dezembro'; break; 
				}
			case 'dia':
				$data = date('w', strtotime($data));
				switch ($data) {
					case "0": return 'DOM'; break;
					case "1": return 'SEG'; break;
					case "2": return 'TER'; break;
					case "3": return 'QUA'; break;
					case "4": return 'QUI'; break;
					case "5": return 'SEX'; break;
					case "6": return 'SAB'; break;
				}
		}
	}
}