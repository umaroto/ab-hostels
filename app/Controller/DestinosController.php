<?php
App::uses('AppController', 'Controller');

class DestinosController extends AppController {

	public $paginate = array(
		'limit' => 10,
		'order' => array(
			'Destino.id' => 'desc'
		)
	);

	private function checkSlug($slug, $id){
		$destino = false;

		do {
			$exist = $this->Destino->find('first', array(
				'conditions' => array(
					'Destino.slug' => $slug,
					'Destino.id != ' . $id
				)
			));

			if($exist){
				$slug .= '_';
				$destino = true;
			} else {
				$destino = false;
			}
		} while ($destino == true);

		return $slug;
	}

	public function index() {
		$destinos = $this->Destino->find('all', array(
			'order' => 'RAND()',
		));
		
		$this->set(compact('destinos'));
	}

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Destino.id' => $this->request->data['Destino']['id'],
						'Destino.cidade LIKE "%'.$this->request->data['Destino']['cidade'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$destinos = $this->Paginator->paginate('Destino');
		$this->set(compact('destinos'));
	}

	public function admin_add() {
		if ($this->request->is('post') || $this->request->is('put')){
			
			if(isset($this->request->data['Destino']['arquivo']) && !empty($this->request->data['Destino']['arquivo']['name'])){
				$this->request->data['Destino']['arquivo'] = $this->Destino->upload($this->request->data['Destino']['arquivo'], 'images/destinos', 380, 600);
			} else {
				unset($this->request->data['Destino']['arquivo']);
			}

			$id = isset($this->request->data['Destino']['id']) && !empty($this->request->data['Destino']['id']) ? $this->request->data['Destino']['id'] : 0;
			$this->request->data['Destino']['slug'] = $this->checkSlug(strtolower(Inflector::slug($this->request->data['Destino']['cidade'], '-')), $id);

			if ($this->Destino->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'admin/sucess');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		}
	}

	public function admin_edit($id = null) {
		$this->request->data = $this->Destino->find('first', array(
			'conditions' => array(
				'Destino.id' => $id,
			),
			'recursive' => -1
		));

		$this->render('admin_add');
	}

	public function admin_search() {
		$this->render(false);
		$destino = $this->Destino->find('all', array(
			'conditions' => array(
				'Destino.titulo LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Destino.id', 'Destino.titulo'),
			'recursive' => -1
		));
		$destino = Set::classicExtract($destino, '{n}.Destino');
		$destinos = array_map(function($destino) {
			return array(
				'id' => $destino['id'],
				'value' => $destino['titulo']
			);
		}, $destino);
		echo json_encode($destinos);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			$foto = $this->Destino->findById($id);

			if($this->Destino->delete($id)){
				if(isset($foto['Destino']['arquivo']) && !empty($foto['Destino']['arquivo'])){
					@unlink(WWW_ROOT . 'images/destinos/'.$foto['Destino']['arquivo']);
				}
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}