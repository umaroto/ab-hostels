<?php
App::uses('AuthComponent', 'Controller/Component');

class Associado extends AppModel {
	public $displayField = 'nome';

	public $belongsTo = array(
		'Destino' => array(
			'className' => 'Destino',
			'foreignKey' => 'destino_id'
		)
	);
}