<?php
App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel {

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notBlank'),
				'message' => 'Nome de usuário obrigatório'
			)
		),
		'username' => array(
			'required' => array(
				'rule' => array('notBlank'),
				'message' => 'Usuário obrigatório'
			)
		),
		'email' => array(
			'required' => array(
				'rule' => array('notBlank'),
				'message' => 'Nome obrigatório'
			)
		),
	);

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
	}
}