<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	public function upload($imagem = array(), $dir = 'uploads', $largura = null, $altura = null) {
		$dir = WWW_ROOT . $dir . DS;
		if(($imagem['error']!=0) and ($imagem['size']==0)) {
			throw new NotImplementedException('Alguma coisa deu errado, o upload retornou erro '.$imagem['error'].' e tamanho '.$imagem['size']);
		}
		$this->checa_dir($dir);
		$imagem = $this->checa_nome($imagem, $dir);
		$this->move_arquivos($imagem, $dir, $largura, $altura);
		return $imagem['name'];
	}

	public function curriculo($arquivo = array(), $dir = 'uploads'){
		$dir = WWW_ROOT . $dir . DS;
		$this->checa_dir($dir);
		$imagem = $this->checa_nome($imagem, $dir);
		$this->move_arquivos($imagem, $dir);
		return $imagem['name'];
	}

	public function checa_dir($dir)
	{
		App::uses('Folder', 'Utility');
		$folder = new Folder();
		if (!is_dir($dir)){
			$folder->create($dir);
		}
	}

	public function checa_nome($imagem, $dir) {
		$imagem_info = pathinfo($dir.$imagem['name']);
		$imagem_nome = $this->trata_nome($imagem_info['filename']).'.'.$imagem_info['extension'];
		$extensoes_validas = array('jpg', 'jpeg', 'pdf', 'docx', 'doc');
		if(!in_array(strtolower($imagem_info['extension']), $extensoes_validas)){
			throw new NotImplementedException('Extensão inválida');
		}

		$conta = 2;
		while (file_exists($dir.$imagem_nome)) {
			$imagem_nome= $this->trata_nome($imagem_info['filename']).'-'.$conta;
			$imagem_nome .= '.'.$imagem_info['extension'];
			$conta++;
		}
		$imagem['name'] = $imagem_nome;
		return $imagem;
	}

	public function trata_nome($imagem_nome) {
		$imagem_nome = strtolower(Inflector::slug($imagem_nome,'-'));
		return $imagem_nome;
	}

	public function move_arquivos($imagem, $dir, $largura = null, $altura = null) {
		App::uses('File', 'Utility');
		$arquivo = new File($imagem['tmp_name']);
		$arquivo->copy($dir.$imagem['name']);
		$arquivo->close();

		if(isset($largura) && !empty($largura)){
			$image = imagecreatefromjpeg($dir.$imagem['name']);
			$filename = $dir.$imagem['name'];

			$thumb_width = $largura;
			$thumb_height = $altura;

			$width = imagesx($image);
			$height = imagesy($image);

			$original_aspect = $width / $height;
			$thumb_aspect = $thumb_width / $thumb_height;

			if ( $original_aspect >= $thumb_aspect )
			{
			   // If image is wider than thumbnail (in aspect ratio sense)
			   $new_height = $thumb_height;
			   $new_width = $width / ($height / $thumb_height);
			}
			else
			{
			   // If the thumbnail is wider than the image
			   $new_width = $thumb_width;
			   $new_height = $height / ($width / $thumb_width);
			}

			$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

			// Resize and crop
			imagecopyresampled($thumb,
			                   $image,
			                   0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
			                   0 - ($new_height - $thumb_height) / 2, // Center the image vertically
			                   0, 0,
			                   $new_width, $new_height,
			                   $width, $height);
			imagejpeg($thumb, $filename, 100);
		}
	}

	public function getLastQuery() {
		$dbo = $this->getDatasource();
		$logs = $dbo->getLog();
		return $logs;
	}
}
