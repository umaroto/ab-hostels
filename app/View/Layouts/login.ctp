<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Sistema Administrativo">
		<meta name="author" content="Renato Castanheiro">
		<meta name="robots" content="noindex, nofollow" />

		<link href="<?php echo Router::url('/', array('full' => true));?>images/admin/favicon.ico" type="image/x-icon" rel="icon">
		<link href="<?php echo Router::url('/', array('full' => true));?>images/admin/favicon.ico" type="image/x-icon" rel="shortcut icon">

		<?php
		echo $this->Html->css('admin/admin.min');
		echo $this->Html->css('admin/booles');
		echo $this->Html->css('../js/admin/plugins/sweetalert2/sweetalert2.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>

		<title><?php echo $this->fetch('title'); ?></title>

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

		<link href="https://fonts.googleapis.com/css?family=Yeseva+One&display=swap" rel="stylesheet">
	</head>


	<body>		
		<div id="page-container">
            <main id="main-container">

                <div class="bg-image">
                    <div class="hero-static bg-white-95">
                        <div class="content">
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-6 col-xl-4">
                                    <!-- Sign In Block -->
                                    <?php echo $this->fetch('content');?>
                                </div>
                            </div>
                        </div>
                        <div class="content content-full font-size-sm text-muted text-center">
                            <strong>Booles v2.0</strong> &copy; <span data-toggle="year-copy"></span>
                        </div>
                    </div>
                </div>
            </main>
        </div>		
		<?php
		echo $this->Html->script(array(
			'admin/oneui.core.min', 
			'admin/oneui.app.min', 

			'admin/plugins/jquery-validation/jquery.validate.min',
			'admin/plugins/es6-promise/es6-promise.auto.min',
        	'admin/plugins/sweetalert2/sweetalert2.min',
        	'admin/plugins/bootstrap-notify/bootstrap-notify.min',

        	'admin/pages/be_comp_dialogs.min',
        	'admin/pages/op_auth_signin.min',
		));
		?>
        <?php
		echo $this->fetch('scriptBottom');
		?>
	</body>
</html>