<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php echo $this->fetch('title'); ?></title>

		<!-- Bootstrap core CSS -->
		<link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,700&display=swap" rel="stylesheet">

		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Satisfy&display=swap" rel="stylesheet">

		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" href="/favicon.ico">

		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css">
		
		<?php
		echo $this->Html->charset();
		echo $this->Html->css(array('style', 'owl.carousel.min'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

		if(isset($og_url)){
			?>
			<meta property="og:url" content="<?php echo $og_url;?>" />
			<?php
		}
		if(isset($og_type)){
			?>
			<meta property="og:type" content="<?php echo $og_type;?>" />
			<?php
		}
		if(isset($og_title)){
			?>
			<meta property="og:title" content="<?php echo $og_title;?>" />
			<?php
		}
		if(isset($og_description)){
			?>
			<meta property="og:description" content="<?php echo $og_description;?>" />
			<?php
		}
		if(isset($og_image)){
			?>
			<meta property="og:image" content="<?php echo $og_image;?>" />
			<?php
		}
		?>
	</head>
	<body class="bg-light" id="body_document">
		<?php echo $this->Session->flash(); ?>
		<header>
			<div class="navbar navbar shadow-sm">
				<div class="container d-flex justify-content-between">
					<a href="<?php echo Router::url('/', array('full' => true));?>" class="navbar-brand d-flex align-items-center">
						<?php echo $this->Html->Image('logo.png');?>
					</a>
					<nav id="menu" class="navigation desktop" role="navigation">
						<?php $class = $this->request->controller == 'pages' && $this->request->action == 'index' ? ' active' : '';?>
						<a class="scrollLink<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>">Home</a>

						<?php $class = $this->request->controller == 'pages' && $this->request->action == 'institucional' ? ' active' : '';?>
						<a class="scrollLink<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>institucional">Institucional</a>

						<?php $class = $this->request->controller == 'destinos' ? ' active' : '';?>
						<a class="scrollLink<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>destinos">Destinos</a>

						<?php $class = $this->request->controller == 'associados' ? ' active' : '';?>
						<a class="scrollLink<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>associados">Associados</a>

						<?php $class = $this->request->controller == 'parceiros' ? ' active' : '';?>
						<a class="scrollLink<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>parceiros">Parceiros</a>

						<?php $class = $this->request->controller == 'blogs' ? ' active' : '';?>
						<a class="scrollLink<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>noticias">Atualidades e Notícias</a>

						<?php $class = $this->request->controller == 'socios' ? ' active' : '';?>
						<a class="scrollLink<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>socios">Associe-se</a>
					</nav>

					<a href="javascript:void(0);" class="mobile" id="menu_mobile">
						<?php echo $this->Html->Image('menu.png');?>
					</a>
				</div>
			</div>
			<div class="container-fluid busca-associado desktop">
				<div class="row">
					<div class="container">
						<div class="col-lg-12">
							<div class="row">
								<?php
								echo $this->Form->create('Destinos', array('url' => array('controller' => 'associados', 'action' => 'reserva'), 'class' => 'form-inline', 'role' => 'form', 'autocomplete' => 'off'));
									?>
									<div class="col-lg-3">
										<select id="AssociadoCidade" name="data[Associado][Cidade]" class="form-control">
											<option value="0">Selecione o Destino</option>
											<?php
											foreach ($destinos_busca as $key => $destino) {
												echo '<option value="'.$destino['Destino']['id'].'">'.$destino['Destino']['cidade'].'</option>';
											}
											?>
										</select>
									</div>
									<div class="col-lg-3">
										<select id="AssociadoHostel" name="data[Associado][Hostel]" class="form-control">
											<option value="0">Selecione o Hostel</option>
										</select>
									</div>
									<div class="col-lg-2">
										<input placeholder="Check-in" id="AssociadoDe" type="text" name="data[Associado][de]" class="form-control datepicker">
									</div>
									<div class="col-lg-2">
										<input placeholder="Check-out" id="AssociadoAte" type="text" name="data[Associado][ate]" class="form-control datepicker">
									</div>
									<div class="col-lg-2">
										<button type="submit" class="btn"><?php echo __('Reservar')?></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="div_menu_mobile" class="navigation">
				<a class="scrollLink" href="<?php echo Router::url('/', array('full' => true));?>">Home</a>
				<a class="scrollLink" href="<?php echo Router::url('/', array('full' => true));?>institucional">Institucional</a>
				<a class="scrollLink" href="<?php echo Router::url('/', array('full' => true));?>destinos">Destinos</a>
				<a class="scrollLink" href="<?php echo Router::url('/', array('full' => true));?>associados">Associados</a>
				<a class="scrollLink" href="<?php echo Router::url('/', array('full' => true));?>parceiros">Parceiros</a>
				<a class="scrollLink" href="<?php echo Router::url('/', array('full' => true));?>noticias">Atualidades e Notícias</a>
				<a class="scrollLink" href="<?php echo Router::url('/', array('full' => true));?>socios">Associe-se</a>
			</div>
		</header>

		<?php echo $this->fetch('content');?>

		<div id="contato-suspenso">
			<div class="header">Quer se associar?</div>
			<div class="contato-body">
				<div class="row">
					<div class="col-lg-12 mt-2">
						<form action="<?php echo Router::url('/', array('full' => true));?>" method="post" id="contact-form">
							<div class="row" id="fields">								
								<div class="form-box col-lg-12 mb-3">
									<label>Nome <small>*</small></label>
									<input type="text" id="nomeForm" name="contato_nome" class="form-control">
								</div>
								<div class="form-box col-lg-12 mb-3">
									<label>Email <small>*</small></label>
									<input type="text" id="emailForm" name="contato_email" class="form-control">
								</div>
								<div class="form-box col-lg-12 mb-3">
									<label>Assunto <small>*</small></label>
									<input type="text" id="assuntoForm" name="contato_assunto" class="form-control">
								</div>

								<div class="form-box col-lg-12 mb-3">
									<label>Mensagem <small>*</small></label>
									<textarea id="mensagemForm" name="contato_mensagem" rows="7" class="form-control"></textarea>
								</div>
								<!-- End Box -->
								<div class="clearfix"></div>

								<div class="form-box col-lg-12 mb-3">
									<input type="submit" value="Enviar" class="btn submit btn-dark">	
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<footer>
			<div class="container">
				<div class="row" id="contato-info">
					<div class="col-lg-4 mt-4 mb-2">
						<h4>Fale Conosco</h4>
						<p class="info"><i class="fas fa-phone"></i> (11) 3327-2015 / (11) 3327-2151</p>
						<p class="info"><i class="fas fa-envelope"></i> <a href="mailto:abhostels@gmail.com" target="_blank">abhostels@gmail.com</a></p>
						<p class="info"><i class="fab fa-facebook"></i> <a href="https://www.facebook.com/ABHOSTELS/" target="_blank">Facebook</a></p>
						<p class="info"><i class="fab fa-instagram"></i> <a href="https://www.instagram.com/abhostels/" target="_blank">Instagram</a></p>
					</div>
					<div class="col-lg-4 mt-4 mb-2">
						<h4>Parceiros Institucionais</h4>
						<div class="share-social">
							<a href="http://abih.com.br/" target="_blank">
								<?php echo $this->Html->Image('parceiros-home/abih.jpg');?>
							</a>
							<a href="https://cadastur.turismo.gov.br" target="_blank">
								<?php echo $this->Html->Image('parceiros-home/cadastur.jpg');?>
							</a>
							<a href="http://hoteisrio.com.br/" target="_blank">
								<?php echo $this->Html->Image('parceiros-home/hoteisrio.jpg');?>
							</a>
							<a href="http://www.turismo.gov.br" target="_blank">
								<?php echo $this->Html->Image('parceiros-home/mtur.jpg');?>
							</a>
							<a href="http://www.riohost.org/" target="_blank">
								<?php echo $this->Html->Image('parceiros-home/riohost.jpg');?>
							</a>
						</div>
					</div>
					<div class="col-lg-4 mt-4 mb-2">
						<h4>Parceiros Comerciais</h4>
						<div class="share-social">
							<a href="http://abih.com.br/" target="_blank">
								<?php echo $this->Html->Image('parceiros-home/ame.jpg');?>
							</a>
							<a href="http://booles.com.br" target="_blank">
								<?php echo $this->Html->Image('parceiros-home/booles.jpg');?>
							</a>
							<a href="https://innsidehotelaria.com.br/" target="_blank">
								<?php echo $this->Html->Image('parceiros-home/innside.jpg');?>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="desenvolvido">
				Desenvolvido por <a href="http://booles.com.br/br/ga/abhostels" target="_blank"><?php echo $this->Html->Image('booles.png');?> Booles</a>
			</div>
		</footer>

		<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
		<script src="<?php echo Router::url('/', array('full' => true));?>js/owl.carousel.min.js"></script>

		<script src="<?php echo Router::url('/', array('full' => true));?>js/jquery.counterup.min.js"></script>

		<script src="<?php echo Router::url('/', array('full' => true));?>js/jquery.maskedinput.min.js"></script>

		<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function(){
				$('#menu_mobile').bind('click', function(){
					if($('#div_menu_mobile').hasClass('down')){
						$('#div_menu_mobile').slideUp();
						$('#div_menu_mobile').removeClass('down');
					} else {
						$('#div_menu_mobile').slideDown();
						$('#div_menu_mobile').addClass('down');
					}
				});

				$('#AssociadoCidade').bind('change', function(){
					$.post("<?php echo Router::url('/', array('full' => true));?>associados/getAssociados", {
						cidade: $(this).val(),
					}).done(function( data ) {
						$('#AssociadoHostel').html(data);
					});
				});

				$('#AssociadoCidadeMobile').bind('change', function(){
					$.post("<?php echo Router::url('/', array('full' => true));?>associados/getAssociados", {
						cidade: $(this).val(),
					}).done(function( data ) {
						$('#AssociadoHostelMobile').html(data);
					});
				});

				$('#DestinosReservaForm').bind('submit', function(){
					erro = false;

					if($('#AssociadoHostel').val() == 0){
						$('#AssociadoHostel').addClass('is-invalid');
						erro = true;
					} else {
						$('#AssociadoHostel').removeClass('is-invalid');
					}

					if($('#AssociadoDe').val() == ''){
						$('#AssociadoDe').addClass('is-invalid');
						erro = true;
					} else {
						$('#AssociadoDe').removeClass('is-invalid');
					}

					if($('#AssociadoAte').val() == ''){
						$('#AssociadoAte').addClass('is-invalid');
						erro = true;
					} else {
						$('#AssociadoAte').removeClass('is-invalid');
					}

					if(erro == false){
						return true;
					} else {
						return false;
					}
				});

				$('#DestinosReservaFormMobile').bind('submit', function(){
					erro = false;

					if($('#AssociadoHostelMobile').val() == 0){
						$('#AssociadoHostelMobile').addClass('is-invalid');
						erro = true;
					} else {
						$('#AssociadoHostelMobile').removeClass('is-invalid');
					}

					if($('#AssociadoDeMobile').val() == ''){
						$('#AssociadoDeMobile').addClass('is-invalid');
						erro = true;
					} else {
						$('#AssociadoDeMobile').removeClass('is-invalid');
					}

					if($('#AssociadoAteMobile').val() == ''){
						$('#AssociadoAteMobile').addClass('is-invalid');
						erro = true;
					} else {
						$('#AssociadoAteMobile').removeClass('is-invalid');
					}

					if(erro == false){
						return true;
					} else {
						return false;
					}
				});

				$('#contact-form').bind('submit', function(){
					erro = false;

					if($('#nomeForm').val() == ''){
						$('#nomeForm').addClass('is-invalid');
						erro = true;
					} else {
						$('#nomeForm').removeClass('is-invalid');
					}

					if($('#emailForm').val() == ''){
						$('#emailForm').addClass('is-invalid');
						erro = true;
					} else {
						$('#emailForm').removeClass('is-invalid');
					}

					if($('#assuntoForm').val() == ''){
						$('#assuntoForm').addClass('is-invalid');
						erro = true;
					} else {
						$('#assuntoForm').removeClass('is-invalid');
					}

					if($('#mensagemForm').val() == ''){
						$('#mensagemForm').addClass('is-invalid');
						erro = true;
					} else {
						$('#mensagemForm').removeClass('is-invalid');
					}

					if(erro == false){
						return true;
					} else {
						return false;
					}
				});

				$('#pop a.close').bind('click', function(){
					$('#pop').fadeOut('slow', function(){
						$('#mask').fadeOut('fast');
					});
				});

				$('#contato-suspenso .header').bind('click', function(){
					$('#contato-suspenso .contato-body').slideToggle();
				});

				$( ".datepicker" ).datepicker({
					minDate: "0",
				});

				$.datepicker.regional['pt-BR'] = {
	                closeText: 'Fechar',
	                prevText: '&#x3c;Anterior',
	                nextText: 'Pr&oacute;ximo&#x3e;',
	                currentText: 'Hoje',
	                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun', 'Jul','Ago','Set','Out','Nov','Dez'],
	                dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
	                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
	                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
	                weekHeader: 'Sm',
	                dateFormat: 'dd/mm/yy',
	                firstDay: 0,
	                isRTL: false,
	                showMonthAfterYear: false,
	                yearSuffix: ''
	           	};
		        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
			});

			function getPosition(elem){
				elem = document.getElementById(elem);
				distanceScrolled = document.body.scrollTop;
				elemRect = elem.getBoundingClientRect();
				elemViewportOffset = elemRect.top;
				totalOffset = distanceScrolled + elemViewportOffset;
				return totalOffset;
			}
		</script>

		<?php echo $this->fetch('scriptBottom'); ?>
		
	</body>
</html>