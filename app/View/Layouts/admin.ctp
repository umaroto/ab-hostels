<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Sistema Administrativo">
		<meta name="author" content="Renato Castanheiro">
		<meta name="robots" content="noindex, nofollow" />

		<link href="<?php echo Router::url('/', array('full' => true));?>images/admin/favicon.ico" type="image/x-icon" rel="icon">
		<link href="<?php echo Router::url('/', array('full' => true));?>images/admin/favicon.ico" type="image/x-icon" rel="shortcut icon">

		<?php

		echo $this->Html->css('admin/admin.min');
		echo $this->Html->css('admin/booles');
		echo $this->Html->css('../js/admin/plugins/sweetalert2/sweetalert2.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>

		<title><?php echo $this->fetch('title'); ?></title>

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
	</head>

	<body>

		<?php echo $this->Session->flash(); ?>

		<div id="page-container" class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed">

			<?php echo $this->element('admin/sidebar');?>

			<?php echo $this->element('admin/header');?>

			<?php echo $this->fetch('content');?>

			<?php echo $this->element('admin/footer');?>

		</div>

		<?php
		echo $this->Html->script(array(
			'admin/oneui.core.min', 
			'admin/oneui.app.min',

			'admin/plugins/jquery-validation/jquery.validate.min',
			'admin/plugins/es6-promise/es6-promise.auto.min',
        	'admin/plugins/sweetalert2/sweetalert2.min',
        	'admin/plugins/bootstrap-notify/bootstrap-notify.min',

        	'admin/pages/op_auth_signin.min',
		));
		?>

		<script type="text/javascript">
			$(document).ready(function() {
				var e = Swal.mixin({
					buttonsStyling: !1,
					customClass: {
						confirmButton: 'btn btn-danger m-1',
						cancelButton: 'btn btn-danger m-1',
						input: 'form-control'
					}
				});

				$('.js-swal-confirm').on('click', function(n) {
					var id = $(this).attr('id').split('_')[1];

					e.fire({
						title: 'Tem certeza?',
						text: 'Você não será capaz de desfazer essa ação!',
						type: 'warning',
						showCancelButton: !0,
						customClass: {
							confirmButton: 'btn btn-danger m-1',
							cancelButton: 'btn btn-secondary m-1'
						},
						confirmButtonText: 'Sim, Apague!',
						cancelButtonText: 'Cancelar',
						html: !1,
						preConfirm: function(e) {
							return new Promise(function(e) {
								setTimeout(function() {
									e()
								}, 50)
							})
						}
					}).then(function(n) {
						if(n.value){
							$.post(
								'<?php echo Router::url('/', array('full' => true));?>admin/<?php echo $this->request->controller;?>/delete',
								{
									id: id
								}
							).done(function( data ) {
								if(data > 0){
									e.fire('Apagado', 'Registro deletado com sucesso.', 'success');
									$('#row_'+data).removeClass().addClass('bounceOutLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
										$(this).remove();
									});
								} else {
									e.fire('Ação cancelada!', 'Houve um erro ao excluir o registro.', 'error');
								}
							});
							
						} else {
							'cancel' === n.dismiss && e.fire('Ação cancelada!', 'Nada foi alterado.', 'error');
						}
					});
				});
			});
		</script>

		<?php echo $this->fetch('scriptBottom'); ?>
	</body>
</html>