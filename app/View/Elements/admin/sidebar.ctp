<nav id="sidebar" aria-label="Main Navigation">
	<!-- Side Header -->
	<div class="content-header bg-white-5">
		<!-- Logo -->
		<a class="font-w600 text-dual" href="index.html">
			<span class="smini-hide">
				<span class="font-w700 font-size-h5">B</span>
			</span>
			<i class="fa fa-circle-notch text-primary"></i>
			<i class="fa fa-circle-notch text-primary"></i>
			<span class="smini-hide">
				<span class="font-w700 font-size-h5">les </span> <span class="font-w400"> 2.0</span>
			</span>
		</a>
		<!-- END Logo -->
	</div>
	<!-- END Side Header -->

	<!-- Side Navigation -->
	<div class="content-side content-side-full">
		<ul class="nav-main">
			<!-- <li class="nav-main-item">
				<a class="nav-main-link" href="be_pages_dashboard.html">
					<i class="nav-main-link-icon si si-speedometer"></i>
					<span class="nav-main-link-name">Dashboard</span>
				</a>
			</li> -->

			<?php $li = $this->request->controller == 'destinos' ? ' open' : '';?>
			<li class="nav-main-item<?php echo $li;?>">
				<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
					<i class="nav-main-link-icon si si-globe"></i>
					<span class="nav-main-link-name">Destinos</span>
				</a>
				<ul class="nav-main-submenu">
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'destinos' && $this->request->action == 'admin_add' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/destinos/add">
							<span class="nav-main-link-name">Novo Destino</span>
						</a>
					</li>
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'destinos' && $this->request->action == 'admin_index' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/destinos">
							<span class="nav-main-link-name">Listagem</span>
						</a>
					</li>
				</ul>
			</li>

			<?php $li = $this->request->controller == 'associados' ? ' open' : '';?>
			<li class="nav-main-item<?php echo $li;?>">
				<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
					<i class="nav-main-link-icon si si-users"></i>
					<span class="nav-main-link-name">Associados</span>
				</a>
				<ul class="nav-main-submenu">
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'associados' && $this->request->action == 'admin_add' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/associados/add">
							<span class="nav-main-link-name">Novo Associado</span>
						</a>
					</li>
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'associados' && $this->request->action == 'admin_index' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/associados">
							<span class="nav-main-link-name">Listagem</span>
						</a>
					</li>
				</ul>
			</li>

			<?php $li = $this->request->controller == 'socios' ? ' open' : '';?>
			<li class="nav-main-item<?php echo $li;?>">
				<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
					<i class="nav-main-link-icon si si-users"></i>
					<span class="nav-main-link-name">Associe-se</span>
				</a>
				<ul class="nav-main-submenu">
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'socios' && $this->request->action == 'admin_index' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/socios">
							<span class="nav-main-link-name">Listagem</span>
						</a>
					</li>
				</ul>
			</li>
			
			<?php $li = $this->request->controller == 'parceiros' ? ' open' : '';?>
			<li class="nav-main-item<?php echo $li;?>">
				<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
					<i class="nav-main-link-icon si si-like"></i>
					<span class="nav-main-link-name">Parceiros</span>
				</a>
				<ul class="nav-main-submenu">
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'parceiros' && $this->request->action == 'admin_add' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/parceiros/add">
							<span class="nav-main-link-name">Novo Parceiro</span>
						</a>
					</li>
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'parceiros' && $this->request->action == 'admin_index' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/parceiros">
							<span class="nav-main-link-name">Listagem</span>
						</a>
					</li>
				</ul>
			</li>

			<?php $li = $this->request->controller == 'fotos' ? ' open' : '';?>
			<li class="nav-main-item<?php echo $li;?>">
				<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
					<i class="nav-main-link-icon si si-camera"></i>
					<span class="nav-main-link-name">Fotos Home</span>
				</a>
				<ul class="nav-main-submenu">
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'fotos' && $this->request->action == 'admin_add' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/fotos/add">
							<span class="nav-main-link-name">Nova Foto</span>
						</a>
					</li>
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'fotos' && $this->request->action == 'admin_index' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/fotos">
							<span class="nav-main-link-name">Listagem</span>
						</a>
					</li>
				</ul>
			</li>

			<?php $li = $this->request->controller == 'blogs' ? ' open' : '';?>
			<li class="nav-main-item<?php echo $li;?>">
				<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
					<i class="nav-main-link-icon si si-notebook"></i>
					<span class="nav-main-link-name">Blog</span>
				</a>
				<ul class="nav-main-submenu">
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'blogs' && $this->request->action == 'admin_add' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/blogs/add">
							<span class="nav-main-link-name">Novo Post</span>
						</a>
					</li>
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'blogs' && $this->request->action == 'admin_index' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/blogs">
							<span class="nav-main-link-name">Listagem</span>
						</a>
					</li>
				</ul>
			</li>

			<?php $li = $this->request->controller == 'depoimentos' ? ' open' : '';?>
			<li class="nav-main-item<?php echo $li;?>">
				<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
					<i class="nav-main-link-icon si si-bubbles"></i>
					<span class="nav-main-link-name">Depoimentos</span>
				</a>
				<ul class="nav-main-submenu">
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'depoimentos' && $this->request->action == 'admin_add' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/depoimentos/add">
							<span class="nav-main-link-name">Novo Depoimento</span>
						</a>
					</li>
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'depoimentos' && $this->request->action == 'admin_index' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/depoimentos">
							<span class="nav-main-link-name">Listagem</span>
						</a>
					</li>
				</ul>
			</li>

			<?php $li = $this->request->controller == 'newsletters' ? ' open' : '';?>
			<li class="nav-main-item<?php echo $li;?>">
				<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
					<i class="nav-main-link-icon si si-envelope"></i>
					<span class="nav-main-link-name">Newsletters</span>
				</a>
				<ul class="nav-main-submenu">
					<li class="nav-main-item">
						<?php $class = $this->request->controller == 'newsletters' && $this->request->action == 'admin_index' ? ' active' : '';?>
						<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/newsletters">
							<span class="nav-main-link-name">Listagem</span>
						</a>
					</li>
					<li class="nav-main-item">
						<a class="nav-main-link" target="_blank" href="<?php echo Router::url('/', array('full' => true));?>admin/newsletters/exporta">
							<span class="nav-main-link-name">Baixar CSV</span>
						</a>
					</li>
				</ul>
			</li>
			
			<?php
			if($this->Session->read('Auth.User.role') <= 1){
				?>
				<?php $li = $this->request->controller == 'users' ? ' open' : '';?>
				<li class="nav-main-item<?php echo $li;?>">
					<a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
						<i class="nav-main-link-icon si si-lock"></i>
						<span class="nav-main-link-name">Usuários</span>
					</a>
					<ul class="nav-main-submenu">
						<li class="nav-main-item">
							<?php $class = $this->request->controller == 'users' && $this->request->action == 'admin_add' ? ' active' : '';?>
							<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/users/add">
								<span class="nav-main-link-name">Novo Usuário</span>
							</a>
						</li>
						<li class="nav-main-item">
							<?php $class = $this->request->controller == 'users' && $this->request->action == 'admin_index' ? ' active' : '';?>
							<a class="nav-main-link<?php echo $class;?>" href="<?php echo Router::url('/', array('full' => true));?>admin/users">
								<span class="nav-main-link-name">Listagem</span>
							</a>
						</li>
					</ul>
				</li>
				<?php
			}
			?>			
		</ul>
	</div>
	<!-- END Side Navigation -->
</nav>