<?php
echo $this->Html->scriptBlock("
    $(document).ready(function() {
    	var e = Swal.mixin({
            buttonsStyling: !1,
            customClass: {
                confirmButton: 'btn btn-danger m-1',
                cancelButton: 'btn btn-danger m-1',
                input: 'form-control'
            }
        });
    	e.fire('Oops...', '".$message."', 'error')
    });", array('block' => 'scriptBottom'));