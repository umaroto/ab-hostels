<div class="row m-t-40">
	<div class="col-sm-12 col-md-5">
		<div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">
			<?php echo $this->Paginator->counter('Página <strong>{:page}</strong> de <strong>{:pages}</strong>');?>
		</div>
	</div>

	<div class="col-sm-12 col-md-7">
		<div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
	        <ul class="pagination">
				<?php
				echo $this->Paginator->numbers(array(
					'modulus' => '8',
					'first' => 'Primeira',
					'last' => ' Última',
					'tag' => 'li',
					'class' => 'paginate_button page-item',
					'currentClass' => 'active',
					'currentTag' => 'a',
					'separator' => null
				));
				?>
			</ul>
		</div>
	</div>
</div>