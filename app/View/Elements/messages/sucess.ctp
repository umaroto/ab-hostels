<div id="mask"></div>

<div id="pop">
	<?php
	echo $this->Html->link(
		'<i class="fa fa-times" aria-hidden="true"></i>',
		'javascript:void(0);',
		array('class' => 'close', 'escape' => false)
	);
	?>
	<p class="sucess"><?php echo $message; ?></p>
</div>