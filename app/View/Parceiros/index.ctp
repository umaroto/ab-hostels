<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="parceiros-interna">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- <h3 class="text-center mb-4">Conheça os Hostels Associados da ABHostels</h3> -->
					<h3 class="text-center mb-5">Conheça os parceiros da ABHostels</h3>
				</div>
				<?php
				foreach ($parceiros as $key => $parceiro) {
					?>
					<div class="col-6 col-lg-3">
						<a href="javascript:void(0);" class="parceiro-img" rel="parceiro-<?php echo $parceiro['Parceiro']['id'];?>">
							<?php echo $this->Html->image('parceiros/'.$parceiro['Parceiro']['arquivo']);?>
						</a>
					</div>
					<?php
				}

				foreach ($parceiros as $key => $parceiro) {
					?>
					<div class="col-lg-12 mt-3 parceiro-texto" id="parceiro-<?php echo $parceiro['Parceiro']['id'];?>">
						<hr />
						<h3><?php echo $parceiro['Parceiro']['nome'];?></h3>
						<p>
							<?php echo $parceiro['Parceiro']['texto'];?><br>
							<a href="<?php echo $parceiro['Parceiro']['site'];?>" target="_blank">Acessar website &raquo;</a>
						</p>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</section>
</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$('.carousel-destinos').owlCarousel({
			loop:true,
			margin:20,
			nav:true,
			responsive:{
		        0:{
		            items:1
		        },
		        1000:{
		            items:3
		        }
		    }
		});

		$('.parceiro-texto').hide();

		$('.parceiro-img').bind('click', function(){
			var rel = $(this).attr('rel');
			$('.parceiro-texto').hide();
			
			$('#'+rel).slideDown();

			$('html, body').animate({
				scrollTop: $('#'+rel).offset().top - 150
			}, 500);
		});
	});", array('block' => 'scriptBottom'));