<main id="main-container">
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb breadcrumb-alt">
						<li class="breadcrumb-item">Admin</li>
						<li class="breadcrumb-item" aria-current="page">
							<a class="link-fx" href=""><?php echo ucfirst($this->request->controller);?></a>
						</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
		<!-- Basic -->
        <div class="block">
            <div class="block-header laranja-bg-color">
                <h3 class="block-title text-white"><?php echo isset($this->request->data['Parceiro']) ? 'Editar' : 'Novo';?> <?php echo substr($this->request->controller, 0, -1);?></h3>
            </div>
           <div class="block-content block-content-full">
                <?php 
                echo $this->Form->create('ParceiroAdd', array('type' => 'file', 'url' => array('controller' => $this->request->controller, 'action' => 'add'), 'role' => 'form'));
                    echo $this->Form->input('Parceiro.id');
                    ?>
                    <div class="row push">
                        <div class="col-lg-6 col-xl-5">
                            <div class="form-group">
                                <label for="ParceiroNome">Nome</label>
                                <?php 
                                echo $this->Form->input('Parceiro.nome', array('required', 'class' => 'form-control', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label for="ParceiroTexto">Texto</label>
                                <?php 
                                echo $this->Form->input('Parceiro.texto', array('required', 'rows' => 4, 'class' => 'form-control', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label for="ParceiroSite">Site</label>
                                <?php 
                                echo $this->Form->input('Parceiro.site', array('required', 'class' => 'form-control', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label for="ParceiroArquivo">Logo (255px  x 255px) <small>(formato jpg, com no máximo 2MB)</small></label>
                                <?php 
                                echo $this->Form->input('Parceiro.arquivo', array('type' => 'file', 'class' => 'form-control', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            

                            <br class="clear" />
                            <button type="submit" class="btn btn-sm btn-primary">Salvar</button>
                            
                            <!--
                            <div class="form-group">
                                <label for="ParceiroTexto">Textarea</label>
                                <textarea class="form-control" id="example-textarea-input" name="example-textarea-input" rows="4" placeholder="Textarea content.."></textarea>
                            </div>

                            <div class="form-group">
                                <label for="example-select">Select</label>
                                <select class="form-control" id="example-select" name="example-select">
                                    <option value="0">Please select</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                    <option value="4">Option #4</option>
                                    <option value="5">Option #5</option>
                                    <option value="6">Option #6</option>
                                    <option value="7">Option #7</option>
                                    <option value="8">Option #8</option>
                                    <option value="9">Option #9</option>
                                    <option value="10">Option #10</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="example-select-multiple">Multiple Select</label>
                                <select class="form-control" id="example-select-multiple" name="example-select-multiple" size="5" multiple>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                    <option value="4">Option #4</option>
                                    <option value="5">Option #5</option>
                                    <option value="6">Option #6</option>
                                    <option value="7">Option #7</option>
                                    <option value="8">Option #8</option>
                                    <option value="9">Option #9</option>
                                    <option value="10">Option #10</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Checkboxes</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="example-checkbox-default1" name="example-checkbox-default1">
                                    <label class="form-check-label" for="example-checkbox-default1">Option 1</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="example-checkbox-default2" name="example-checkbox-default2">
                                    <label class="form-check-label" for="example-checkbox-default2">Option 2</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="example-checkbox-default3" name="example-checkbox-default3" disabled>
                                    <label class="form-check-label" for="example-checkbox-default3">Option 3</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="d-block">Inline Checkboxes</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" value="" id="example-checkbox-inline1" name="example-checkbox-inline1">
                                    <label class="form-check-label" for="example-checkbox-inline1">Option 1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" value="" id="example-checkbox-inline2" name="example-checkbox-inline2">
                                    <label class="form-check-label" for="example-checkbox-inline2">Option 2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" value="" id="example-checkbox-inline3" name="example-checkbox-inline3" disabled>
                                    <label class="form-check-label" for="example-checkbox-inline3">Option 3</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Radios</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="example-radios-default1" name="example-radios-default" value="option1" checked>
                                    <label class="form-check-label" for="example-radios-default1">Option 1</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="example-radios-default2" name="example-radios-default" value="option2">
                                    <label class="form-check-label" for="example-radios-default2">Option 2</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="example-radios-default3" name="example-radios-default" value="option2" disabled>
                                    <label class="form-check-label" for="example-radios-default3">Option 3</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="d-block">Inline Radios</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline1" name="example-radios-inline" value="option1" checked>
                                    <label class="form-check-label" for="example-radios-inline1">Option 1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline2" name="example-radios-inline" value="option2">
                                    <label class="form-check-label" for="example-radios-inline2">Option 2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline3" name="example-radios-inline" value="option2" disabled>
                                    <label class="form-check-label" for="example-radios-inline3">Option 3</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="d-block" for="example-file-input">File Input</label>
                                <input type="file" id="example-file-input" name="example-file-input">
                            </div>
                            -->
                        </div>                        
                        <?php
                        if(isset($this->request->data['Parceiro']['arquivo'])){
                            ?>
                            <div class="col-lg-6 col-xl-5 text-right">
                                <div class="form-group">
                                    <label for="ParceiroArquivo">Imagem Atual</label>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo $this->Html->Image('parceiros/'.$this->request->data['Parceiro']['arquivo'], array('style' => 'width:150px;'));
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Basic -->
	</div>
	<!-- END Page Content -->

</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		
	});", array('block' => 'scriptBottom'));