<?php echo $this->Session->flash(); ?>

<div class="block block-themed block-fx-shadow mb-0">
    <div class="block-header">
        <h3 class="block-title">Login</h3>
    </div>
    <div class="block-content">
        <div class="p-sm-3 px-lg-4 py-lg-5">
            <h1 class="mb-2 yeseva laranja-color uppercase">Booles</h1>
            <p>Bem vindo</p>
            
            <?php
            echo $this->Form->create('User', array('class' => 'js-validation-signin', 'role' => 'form'));
            	?>
                <div class="py-3">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-alt form-control-lg" id="login-username" name="login-username" placeholder="Usuário">
                    </div>
                    <div class="form-group">
                    	<input type="password" class="form-control form-control-alt form-control-lg" id="login-password" name="login-password" placeholder="Senha">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-xl-5">
                        <button type="submit" class="btn btn-block btn-primary">
                            <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Entrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>