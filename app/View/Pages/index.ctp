<main role="main">
	<section class="jumbotron text-center">
		<div class="owl-carousel carousel-home">
			<?php
			foreach ($fotos as $key => $foto) {
				?>
				<div class="item" style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/fotos/<?php echo $foto['Foto']['arquivo'];?>');"></div>
				<?php
			}
			?>
		</div>

		<div class="container">
			<div class="box-carousel col-lg-6">
				Os melhores<br>Hostels do país estão na ABHostels!<br>
				<a href="<?php echo Router::url('/', array('full' => true));?>associados">Confira nossos Associados!</a>
			</div>
		</div>
	</section>

	<div class="container-fluid busca-associado mobile">
		<div class="col-lg-12">
			<div class="row">
				<div class="container">
					<div class="col-lg-12">
						<div class="row">
							<?php
							echo $this->Form->create('Destinos', array('url' => array('controller' => 'associados', 'action' => 'reserva'), 'class' => 'form-inline', 'role' => 'form', 'id' => 'DestinosReservaFormMobile', 'autocomplete' => 'off'));
								?>
								<div class="col-lg-3">
									<select id="AssociadoCidadeMobile" name="data[Associado][Cidade]" class="form-control">
										<option value="0">Selecione o Destino</option>
										<?php
										foreach ($destinos_busca as $key => $destino) {
											echo '<option value="'.$destino['Destino']['id'].'">'.$destino['Destino']['cidade'].'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-lg-3">
									<select id="AssociadoHostelMobile" name="data[Associado][Hostel]" class="form-control">
										<option value="0">Selecione o Hostel</option>
									</select>
								</div>
								<div class="col-lg-2">
									<input id="AssociadoDeMobile" placeholder="Check-in"  type="text" name="data[Associado][de]" class="form-control datepicker">
								</div>
								<div class="col-lg-2">
									<input id="AssociadoAteMobile" placeholder="Check-out"  type="text" name="data[Associado][ate]" class="form-control datepicker">
								</div>
								<div class="col-lg-2">
									<button type="submit" class="btn"><?php echo __('Reservar')?></button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section id="bem-vindo">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 bemvindo-white">
					ASSOCIAÇÃO BRASILEIRA DE HOSTELS E NOVAS HOSPITALIDADES
				</div>
				<div class="col-lg-6 bemvindo-black">
					<i class="far fa-smile-beam"></i>  SEJA BEM VINDO!
				</div>
			</div>
		</div>
	</section>

	<section id="sobre">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3>Sobre nós</h3>
					<p>
						A Associação Brasileira de Hostels e Novas Hospitalidades, a ABHOSTELS, foi fundada em novembro de 2016, na ocasião do II Fórum CNTur de Hostels, durante o Congresso Internacional de Gastronomia, Hospitalidade e Turismo. A associação tem como principal objetivo fortalecer o setor realizando ações de integração e promoção junto aos empresários do segmento. Importante também para os membros dessa rede é trabalhar a constante difusão do conceito do hostel e novas hospitalidades para os nossos viajantes a nível nacional.<br><br>
						<a href="<?php echo Router::url('/', array('full' => true));?>socios">Associe-se</a> 
						<a href="<?php echo Router::url('/', array('full' => true));?>institucional">Institucional</a> 
					</p>
				</div>
			</div>
		</div>
	</section>

	<!-- paralax -->
	<section id="counter">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="bloco">
						<span class="circle"><i class="far fa-money-bill-alt"></i></span>
						<div class="number">
							<span class="number-text legenda">+ de R$</span>
							<span class="faturamento">0</span>
							<span class="number-text legenda">Milhões</span>
						</div>
						<div class="number-text">Faturamento / ano</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="bloco">
						<span class="circle"><i class="fas fa-bed"></i></span>
						<div class="number">
							<span class="number-text legenda">+ de</span>
							<span class="leitos">0</span>
						</div>
						<div class="number-text">Leitos</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="bloco">
						<span class="circle"><i class="fas fa-chart-bar"></i></span>
						<div class="number">
							<span class="variaveis">0</span>
							<span class="number-text legenda">%</span>
						</div>
						<div class="number-text">Ocupação Média</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="bloco">
						<span class="circle"><i class="fas fa-home"></i></span>
						<div class="number">
							<span class="number-text legenda">R$</span>
							<span class="diarias">0</span>
							<span class="number-text legenda">,00</span>
						</div>
						<div class="number-text">Diária Média</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="newsletter">
		<div class="container-fluid">
			<div class="row">
				<div class="container">
					<div class="row">
						<?php
						echo $this->Form->create('News', array('url' => array('controller' => 'newsletters', 'action' => 'add'), 'class' => 'form-inline', 'role' => 'form'));
							?>
							<div class="col-lg-2 offset-lg-2">	
								<label for="formNews" class=""><?php echo __('RECEBA NOSSA NEWS')?>:</label>
							</div>
							<div class="col-lg-4">	
								<input type="text" class="form-control-plaintext" id="formNews" name="data[Newsletter][email]" placeholder="email@exemplo.com.br">
							</div>
							<div class="col-lg-2">
								<button type="submit" class="btn"><?php echo __('QUERO RECEBER')?></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="destinos">	
		<div class="container">
			<div class="row">
				<?php
				foreach ($destinos as $key => $destino) {
					?>
					<div class="col-lg-3">
						<a href="<?php echo Router::url('/', array('full' => true));?>associados/<?php echo $destino['Destino']['slug'];?>">
							<?php echo $this->Html->image('destinos/'.$destino['Destino']['arquivo']);?>
							<div class="destino-suspenso"><?php echo $destino['Destino']['cidade'];?></div>
						</a>
					</div>
					<?php
				}
				?>
				<div class="col-lg-3">
					<h3>Destinos<br>Populares</h3>
					<p>A AB Hostels possui Associados em todas partes do Brasil como São Paulo, Rio de Janeiro, Ubatuba, Belo Horizonte e Florianópolis.</p>
					<a href="<?php echo Router::url('/', array('full' => true));?>destinos" class="ver-mais">Veja outros destinos</a>
				</div>
			</div>
		</div>
	</section>

	<section id="depoimentos">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3>Depoimentos</h3>
					<p class="text-muted">Veja o que falam sobre nós:</p>
				</div>
				<div class="col-lg-12">
					<div class="testimonials owl-carousel" id="carousel-depoimentos">
						<?php
						foreach ($depoimentos as $key => $depoimento) {
							?>
							<div class="testimonial-item">
								<p>
									<?php echo $this->Html->image('depoimentos/' . $depoimento['Depoimento']['arquivo']);?>
									"<?php echo nl2br($depoimento['Depoimento']['texto']);?>"
								</p>
								<div class="testimonials-arrow"></div>
								<div class="author">
									<a href="javascript:void(0);"><span class="color"><?php echo $depoimento['Depoimento']['nome'];?></span></a>, <?php echo $depoimento['Depoimento']['cargo'];?>, <?php echo $depoimento['Depoimento']['empresa'];?>
								</div>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="noticias">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="mb-4">Atualidades e Notícias do Setor</h3>
				</div>
				<?php
				foreach ($blogs as $key => $blog) {
					?>
					<div class="col-lg-4">
						<a href="<?php echo Router::url('/', array('full' => true));?>noticias/<?php echo $blog['Blog']['slug'];?>">
							<?php echo $this->Html->image('blog/' . $blog['Blog']['arquivo']);?>
							<h4 class="mt-2"><?php echo $blog['Blog']['titulo'];?></h4>
						</a>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</section>
</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {

		$( window ).resize(function() {
			altura = $( window ).height();
			diff = $( 'header' ).height();
			$('.jumbotron').height(altura - diff);
			$('.jumbotron .lead').css('margin-top', (altura - 50) / 2);
			$('.jumbotron .owl-nav').css('margin-top', - (altura - 50) / 2);
		});


		$('#NewsAddForm').bind('submit', function(){
			if($('#formNews').val() == ''){
				alert('Preencha o campo email corretamente');
				$('#formNews').addClass('is-invalid');
				return false;
			} else {
				return true;
			}
		});

		$('#carousel-depoimentos').owlCarousel({
			loop:true,
			margin:10,
			nav:true,
			responsive:{
		        0:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		});

		$('.carousel-home').owlCarousel({
			loop:true,
			margin:0,
			nav:true,
			responsive:{
		        0:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		});
		
		var contador = true;

		$(window).scroll(function() {
			if(getPosition('counter') <= 250 && contador == true){
				 $('.faturamento').countTo({
		            from: 0,
		            to: 451,
		            speed: 1000,
		            refreshInterval: 50,
		        });
				$('.leitos').countTo({
		            from: 0,
		            to: 65645,
		            speed: 1000,
		            refreshInterval: 50,
		        });
		        $('.variaveis').countTo({
		            from: 0,
		            to: 50,
		            speed: 1000,
		            refreshInterval: 50,
		        });
		        $('.diarias').countTo({
		            from: 0,
		            to: 62,
		            speed: 1000,
		            refreshInterval: 50,
		        });

				contador = false;
			}
		});	

		$(window).trigger('resize');
		$(window).trigger('scroll');

	});", array('block' => 'scriptBottom'));