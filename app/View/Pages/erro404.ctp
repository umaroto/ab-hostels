<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="noticias">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2><?php echo $message; ?></h2>
					<?php
					if (Configure::read('debug') > 0):
						?>			
						<p class="error">
							<strong><?php echo __d('cake', 'Error'); ?>: </strong>
							<?php echo __d('cake', 'An Internal Error Has Occurred.'); ?>
						</p>
						<?php
						echo $this->element('exception_stack_trace');
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
</main>