<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="destinos-interna">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 mt-5 mb-5">
					<div class="row">
						<div class="col-md-12">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col-auto institucional d-lg-block">
									<?php echo $this->Html->image('institucional/leo_barroso.jpg');?>
								</div>
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">Leo Barroso<br><span class="text-primary">Presidente</span></strong>
									<p class="card-text mb-auto">Carioca da gema e aos 38 anos de vida, já tem mais de 20 anos de experiência como operador e empreendedor no mercado hoteleiro e hosteleiro brasileiro. Fundador e atual presidente da ABHostels (Associação Brasileira de Hostels e Novas Hospitalidades), fundador e ex-presidente da RioHost (Associação de Cama e Café e Albergues do Estado do Rio de Janeiro), atual Diretor de Hostels e Novas Hospitalidades do HoteisRio (Sindicato Patronal dos Meios de Hospedagem do Município do Rio de Janeiro), conselheiro da Equipotel e sócio-fundador da Rede Bamboo Hostels, com unidades nas cidades do Rio e de Búzios.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col-auto institucional d-lg-block mobile">
									<?php echo $this->Html->image('institucional/francielle.jpg');?>
								</div>
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">Francielle Zuffo<br><span class="text-primary">Vice Presidente</span></strong>
									<p class="card-text mb-auto">Atua no setor hoteleiro há 15 anos. Iniciou seu trabalho no Hostel Roma, um dos mais antigos de Curitiba, no Paraná, e desde então tem presença ativa junto ao trade e às frentes políticas da capital paranaense. Em seu currículo, carrega trabalhos como Coordenadora de Eventos da Associação dos Comerciantes do Centro Histórico de Curitiba, Diretora de Comunicação da ABIH-Pr (Associação Brasileira de Indústria de Hotéis) e Relações Públicas do Mercado Municipal de Curitiba. Atualmente tem forte presença no mercado de eventos públicos e corporativos, detendo grande rede de networking.</p>
								</div>
								<div class="col-auto institucional d-lg-block desktop">
									<?php echo $this->Html->image('institucional/francielle.jpg');?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">João Paulo Paiva Martins Amorim<br><span class="text-primary">Conselho fiscal</span></strong>
									<p class="card-text mb-auto">joaopauloakihostel@gmail.com</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">Eduardo Perroni<br><span class="text-primary">Suplente de conselho</span></strong>
									<p class="card-text mb-auto">eduardo@namoahostel.com.br</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">Guilherme Carames Beskow<br><span class="text-primary">Secretário</span></strong>
									<p class="card-text mb-auto">guilherme@maracahostel.com.br</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">Bruna Steckling<br><span class="text-primary">Suplente de conselho</span></strong>
									<p class="card-text mb-auto">stecklingb89@gmail.com</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">Camilla Gonçalves da Costa<br><span class="text-primary">Suplente de conselho</span></strong>
									<p class="card-text mb-auto">millagc@gmail.com</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">André Felipe Binutti Marciano<br><span class="text-primary">Conselho fiscal</span></strong>
									<p class="card-text mb-auto">andrebmarciano@gmail.com</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">Gabriel Correa<br><span class="text-primary">Conselho fiscal</span></strong>
									<p class="card-text mb-auto">correal.gabriel@gmail.com</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 text-success">Diogo Marcel Araujo<br><span class="text-primary">Tesoureiro</span></strong>
									<p class="card-text mb-auto">diogoiguassu@gmail.com</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>