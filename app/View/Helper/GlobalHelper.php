<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class GlobalHelper extends Helper {

	public function formataData($data){
		return implode('/', array_reverse(explode('-', $data)));
	}

	public function data_ptBR($data){
		if(empty($data)){
			return '';
		}
		
		$dt = explode(' ', $data);
		$sub = explode('-', $dt[0]);
		$hora = explode(':', $dt[1]);
		return $sub[2].'/'.$sub[1].'/'.$sub[0].' '.$hora[0].':'.$hora[1];
	}

	public function data_extenso($data){
		$dt = explode(' ', $data);
		$sub = explode('-', $dt[0]);
		$hora = explode(':', $dt[1]);
		return $sub[2].'/'.$sub[1].'/'.$sub[0].' às '.$hora[0].':'.$hora[1];
	}

	public function dataExtensoTipo($data, $tipo = 'm'){
		if($tipo == 'm'){
			switch (date('m', strtotime($data))) {
				case '01': return 'Janeiro'; break;
				case '02': return 'Fevereiro'; break;
				case '03': return 'Março'; break;
				case '04': return 'Abril'; break;
				case '05': return 'Maio'; break;
				case '06': return 'Junho'; break;
				case '07': return 'Julho'; break;
				case '08': return 'Agosto'; break;
				case '09': return 'Setembro'; break;
				case '10': return 'Outubro'; break;
				case '11': return 'Novembro'; break;
				case '12': return 'Dezembro'; break;
			}
		} else {
			return date($tipo, strtotime($data));
		}
	}

	public function dataMes($data){
		switch ($data) {
			case '01': return 'Janeiro'; break;
			case '02': return 'Fevereiro'; break;
			case '03': return 'Março'; break;
			case '04': return 'Abril'; break;
			case '05': return 'Maio'; break;
			case '06': return 'Junho'; break;
			case '07': return 'Julho'; break;
			case '08': return 'Agosto'; break;
			case '09': return 'Setembro'; break;
			case '10': return 'Outubro'; break;
			case '11': return 'Novembro'; break;
			case '12': return 'Dezembro'; break;
		}
	}
	
	public function formataTipo($tipo){
		switch ($tipo) {
			case 'Aguardando':
			case 'Duplicado':
				return '<button type="button" class="btn btn-info waves-effect waves-light w-md">'.$tipo.'</button>';
				break;
			case 'Incompleto':
				return '<button type="button" class="btn btn-warning waves-effect waves-light w-md">'.$tipo.'</button>';
				break;
			case 'Completo':
				return '<button type="button" class="btn btn-success waves-effect waves-light w-md">'.$tipo.'</button>';
				break;
			case 'Inativo':
			case 'Desclassificado':
				return '<button type="button" class="btn btn-danger waves-effect waves-light w-md">'.$tipo.'</button>';
				break;
		}
	}

	public function formataNivel($nivel){
		switch ($nivel) {
      		case 0:
	        	return '<span class="badge badge-success">Super</span>';
	        	break;
			case 1:
				return '<span class="badge badge-primary">Admin</span>';
				break;
			case 2:
				return '<span class="badge badge-warning">Usuário</span>';
				break;
			default:
				return '<span class="badge badge-warning">Usuário</span>';
				break;
		}
	}
	
	public function formataAtivo($nivel){
		switch ($nivel) {
      		case 'S':
	        	return '<span class="badge badge-success">Ativo</span>';
	        	break;
			case 'N':
				return '<span class="badge badge-danger">Inativo</span>';
				break;
			default:
				return '<span class="badge badge-danger">Inativo</span>';
				break;
		}
	}
}
