<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="associados-interna">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="text-center mb-5">Conheça os Hostels Associados da ABHostels</h3>
				</div>
				<?php
				foreach ($associados as $key => $associado) {
					?>
					<div class="col-6 col-lg-2">
						<a href="javascript:void(0);" class="associado-img" rel="associado-<?php echo $associado['Associado']['id'];?>">
							<?php echo $this->Html->image('associados/'.$associado['Associado']['arquivo']);?>
						</a>
					</div>
					<?php
				}

				foreach ($associados as $key => $associado) {
					?>
					<div class="col-lg-12 mt-3 associado-texto" id="associado-<?php echo $associado['Associado']['id'];?>">
						<hr />
						<h3><?php echo $associado['Associado']['nome'];?></h3>
						<p>
							<?php echo $associado['Associado']['texto'];?><br>
							<a href="<?php echo $associado['Associado']['site'];?>" target="_blank">Acessar website &raquo;</a>
						</p>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</section>

</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$('.carousel-destinos').owlCarousel({
			loop:true,
			margin:20,
			nav:true,
			responsive:{
		        0:{
		            items:1
		        },
		        1000:{
		            items:3
		        }
		    }
		});

		$('.associado-texto').hide();

		$('.associado-img').bind('click', function(){
			var rel = $(this).attr('rel');
			$('.associado-texto').hide();
			
			$('#'+rel).slideDown();

			$('html, body').animate({
				scrollTop: $('#'+rel).offset().top - 150
			}, 500);
		});
	});", array('block' => 'scriptBottom'));