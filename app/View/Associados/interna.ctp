<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="associados-interna">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="text-center mb-5">Conheça os Hostels Associados da ABHostels em <?php echo $dest['Destino']['cidade'];?></h3>
				</div>
				<?php
				foreach ($associados as $key => $associado) {
					?>
					<div class="col-6 col-lg-2">
						<a href="javascript:void(0);" class="associado-img" rel="associado-<?php echo $associado['Associado']['id'];?>">
							<?php echo $this->Html->image('associados/'.$associado['Associado']['arquivo']);?>
						</a>
					</div>
					<?php
				}

				foreach ($associados as $key => $associado) {
					?>
					<div class="col-lg-12 mt-3 associado-texto" id="associado-<?php echo $associado['Associado']['id'];?>">
						<hr />
						<h3><?php echo $associado['Associado']['nome'];?></h3>
						<p>
							<?php echo $associado['Associado']['texto'];?><br>
							<a href="<?php echo $associado['Associado']['site'];?>" target="_blank">Acessar website &raquo;</a>
						</p>
					</div>
					<?php
				}
				?>

				<div class="col-lg-12 mb-5">
					<div class="row sobre-icones">
						<div class="col-lg-4 text-center icon_box">
							<?php
							$link = isset($dest['Destino']['link_destinos']) && !empty($dest['Destino']['link_destinos']) ? $dest['Destino']['link_destinos'] : 'javascript:void(0);';
							?>
							<a href="<?php echo $link;?>" target="_blank">
								<div class="image_wrapper">
									<?php echo $this->Html->image('icon1.png');?>
								</div>
								<div class="desc_wrapper">
									<h4 class="title">Dicas</h4>
								</div>
							</a>
						</div>
						<div class="col-lg-4 text-center icon_box">
							<?php
							$link = isset($dest['Destino']['link_pontos']) && !empty($dest['Destino']['link_pontos']) ? $dest['Destino']['link_pontos'] : 'javascript:void(0);';
							?>
							<a href="<?php echo $link;?>" target="_blank">
								<div class="image_wrapper">
									<?php echo $this->Html->image('icon2.png');?>
								</div>
								<div class="desc_wrapper">
									<h4 class="title">Pontos Turísticos</h4>
								</div>
							</a>
						</div>
						<div class="col-lg-4 text-center icon_box">
							<?php
							$link = isset($dest['Destino']['link_gastronomia']) && !empty($dest['Destino']['link_gastronomia']) ? $dest['Destino']['link_gastronomia'] : 'javascript:void(0);';
							?>
							<a href="<?php echo $link;?>" target="_blank">
								<div class="image_wrapper">
									<?php echo $this->Html->image('icon3.png');?>
								</div>
								<div class="desc_wrapper">
									<h4 class="title">Gastronomia e Eventos</h4>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$('.associado-texto').hide();

		$('.associado-img').bind('click', function(){
			var rel = $(this).attr('rel');
			$('.associado-texto').hide();
			
			$('#'+rel).slideDown();

			$('html, body').animate({
				scrollTop: $('#'+rel).offset().top - 150
			}, 500);
		});
	});", array('block' => 'scriptBottom'));