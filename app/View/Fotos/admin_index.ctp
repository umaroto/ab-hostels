<main id="main-container">
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb breadcrumb-alt">
						<li class="breadcrumb-item">Admin</li>
						<li class="breadcrumb-item" aria-current="page">
							<a class="link-fx" href=""><?php echo ucfirst($this->request->controller);?></a>
						</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
        <div class="row items-push">
        	<?php
			foreach ($fotos as $k => $foto) :
				?>
	            <div class="col-md-4 animated fadeIn" id="row_<?php echo $foto['Foto']['id'];?>">
	                <div class="options-container fx-item-rotate-l fx-overlay-slide-left">
	                	<?php
                        echo $this->Html->Image('fotos/'.$foto['Foto']['arquivo'], array('class' => 'img-fluid options-item'));
                        ?>
	                    <div class="options-overlay bg-black-75">
	                        <div class="options-overlay-content">
	                          	<button id="delete_<?php echo $foto['Foto']['id'];?>" type="button" class="js-swal-confirm btn btn-sm btn-danger" data-toggle="tooltip" title="Delete">
									<i class="fa fa-fw fa-times"></i> Apagar
								</button>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <?php
			endforeach;
			?>
        </div>
        <!-- END Image Default -->

		<?php
		echo $this->Element('admin/pagination');
		?>
	</div>
	<!-- END Page Content -->

</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		
	});", array('block' => 'scriptBottom'));