<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="bem-vindo">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 bemvindo-white mini">
					<h3>Por que ficar em um Hostel?</h3>
					Foi-se o tempo em que os hostels eram coisa de mochileiros guerreiros e batalhadores, atrás de preços baixos e dispostos a enfrentar banheiros sujos, quartos gigantes cheios de camas e uma enorme bagunça de desconhecidos para poder viajar, conhecer o mundo e diferentes culturas. Atualmente, é possível ficar em hostels e ter a mesma qualidade de serviços oferecidos em pousadas e hotéis.<br>Mais do que uma acomodação barata, os hostels envolvem uma filosofia de viagem: quem lá se hospeda não está só interessado em conhecer os pontos turísticos, mas ter contato e vivência da cultura local. Os hostels costumam ser lugares amigáveis e modernos, cheios de áreas comuns, com ambientes que estimulam a interação entre os hóspedes.
				</div>
				<div class="col-lg-6 bemvindo-black mini">
					As localizações dos hostels costumam ser imbatíveis! A maioria deles fica em lugares privilegiados, facilitando a circulação de seus hóspedes pela cidade e o acesso a todos os tipos de passeios, lojas e conveniências. Muitos têm pontos de ônibus quase na porta ou a alguns passos de distância, além de possuir sempre uma atração ou ponto turístico próximo. No hostel também sempre é possível contratar algum passeio ou contar com uma boa indicação!
				</div>
			</div>
		</div>
	</section>

	<section id="destinos-interna">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="text-center mt-5 mb-4">Clique e Confira nossos Associados!</h3>
				</div>
				<div class="col-lg-12 carousel-destinos owl-carousel owl-theme">
					<?php
					foreach ($destinos as $key => $destino) {
						?>
						<div class="item">
							<a href="<?php echo Router::url('/', array('full' => true));?>associados/<?php echo $destino['Destino']['slug'];?>">
								<?php echo $this->Html->image('destinos/'.$destino['Destino']['arquivo']);?>
								<div class="destino-suspenso"><?php echo $destino['Destino']['cidade'];?></div>
							</a>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$('.carousel-destinos').owlCarousel({
			loop:true,
			margin:20,
			nav:true,
			responsive:{
		        0:{
		            items:1
		        },
		        1000:{
		            items:3
		        }
		    }
		});
	});", array('block' => 'scriptBottom'));