<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="noticias">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="mb-4">Atualiadades e Notícias do Setor</h3>
				</div>
			</div>

			<div class="row grid">
				<?php
				if(empty($blogs)){
					?>
					<div class="col-lg-12">
						<p>Nenhuma história encontrada</p>
					</div>
					<?php
				}

				foreach ($blogs as $key => $blog) {
					?>
					<div class="col-lg-4 noticia-block grid-item">
						<div>
							<?php
							if(isset($blog['Blog']['arquivo']) && !empty($blog['Blog']['arquivo'])){
								echo '<a href="'.Router::url('/', array('full' => true)) . 'noticias/' . $blog['Blog']['slug'].'">';
									echo $this->Html->Image('blog/'.$blog['Blog']['arquivo']);
								echo '</a>';
							}
							?>
							<h4><?php echo $blog['Blog']['titulo'];?></h4>
							<small><?php echo $this->Global->data_ptBR($blog['Blog']['created']);?></small>
						
							<p><?php echo substr(strip_tags($blog['Blog']['texto']), 0, 250);?>...</p>
							<a href="<?php echo Router::url('/', array('full' => true)) . 'noticias/' . $blog['Blog']['slug'];?>">Continuar lendo >></a>
						</div>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</section>
</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		setTimeout(function(){
			$('.grid').isotope({
				itemSelector: '.grid-item',
			});
		}, 2000);
	});", array('block' => 'scriptBottom'));