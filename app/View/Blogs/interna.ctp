<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="noticias" class="noticias-interna">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="mb-4"><?php echo $blog['Blog']['titulo'];?></h3>
					<?php
					if(isset($blog['Blog']['arquivo']) && !empty($blog['Blog']['arquivo']))
						echo '<div class="image-post">'.$this->Html->Image('blog/'.$blog['Blog']['arquivo']).'</div>';
					?>					
					<small>Publicado em <?php echo $this->Global->data_ptBR($blog['Blog']['created']);?></small><br><br>
				
					<?php echo $blog['Blog']['texto'];?>

					<br>

					<div class="fb-share-button" data-href="<?php echo Router::url('/', array('full' => true));?>noticias/<?php echo $blog['Blog']['slug'];?>" data-layout="button_count"></div>

					<br><br>
					<a href="javascript:history.back()"><< Voltar</a>
				</div>
			</div>			
		</div>
	</section>
</main>

<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>