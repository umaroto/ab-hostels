<main id="main-container">
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb breadcrumb-alt">
						<li class="breadcrumb-item">Admin</li>
						<li class="breadcrumb-item" aria-current="page">
							<a class="link-fx" href=""><?php echo ucfirst($this->request->controller);?></a>
						</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
		<!-- Full Table -->
		<div class="block">
			<div class="block-content">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-vcenter">
						<thead>
							<tr>
								<th class="sorting">
									<?php
									echo $this->Paginator->sort(
										'email',
										'Email',
										array('escape' => false)
									);
									?>
								</th>
								<th class="sorting">
									<?php
									echo $this->Paginator->sort(
										'created',
										'Data de Cadastro',
										array('escape' => false)
									);
									?>
								</th>
								<th class="text-center" style="width: 100px;">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($newsletters as $k => $newsletter) :
								?>
								<tr id="row_<?php echo $newsletter['Newsletter']['id'];?>">									
									<td class="font-w600 font-size-sm">
										<a href="<?php echo Router::url(array('action' => 'edit', $newsletter['Newsletter']['id']));?>"><?php echo $newsletter['Newsletter']['email'];?></a>
									</td>
									<td class="font-size-sm">
										<?php echo $this->Global->data_ptBR($newsletter['Newsletter']['created'], 'pt-br');?></a>
									</td>
									<td class="text-center">
										<div class="btn-group">
											<a class="btn btn-sm btn-primary" href="<?php echo Router::url(array('action' => 'edit', $newsletter['Newsletter']['id']));?>" data-toggle="tooltip"  title="Editar">
												<i class="fa fa-fw fa-pencil-alt"></i>
											</a>
											 <button id="delete_<?php echo $newsletter['Newsletter']['id'];?>" type="button" class="js-swal-confirm btn btn-sm btn-danger" data-toggle="tooltip" title="Delete">
												<i class="fa fa-fw fa-times"></i>
											</button>
										</div>
									</td>
								</tr>
								<?php
							endforeach;
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END Full Table -->

		<?php
		echo $this->Element('admin/pagination');
		?>
	</div>
	<!-- END Page Content -->

</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		
	});", array('block' => 'scriptBottom'));