<?php
echo '"ID","Email","Criado Em"';

foreach ($newsletters as $k => $newsletter) :
	$newsletter['Newsletter']['email'] = strip_tags($newsletter['Newsletter']['email']);
	$newsletter['Newsletter']['email'] = trim(preg_replace('/\s\s+/', ' ', $newsletter['Newsletter']['email']));

	$vogais = array(
		'À', 'à', 'Á', 'á', 'Â', 'â', 'Ã', 'ã', 'Ä', 'ä', 'Å', 'å', 'Æ', 'æ', 
		'Ç', 'ç', 
		'Ð', 'ð', 'È', 'è', 'É', 'é', 'Ê', 'ê', 'Ë', 'ë', 
		'Ì', 'ì', 'Í', 'í', 'Î', 'î', 'Ï', 'ï', 
		'Ñ', 'ñ', 
		'Ò', 'ò', 'Ó', 'ó', 'Ô', 'ô', 'Õ', 'õ', 'Ö', 'ö', 'Ø', 'ø', 'Œ', 'œ', 
		'ß', 
		'Þ', 'þ', 
		'Ù', 'ù', 'Ú', 'ú', 'Û', 'û', 'Ü', 'ü', 
		'Ý', 'ý', 'Ÿ', 'ÿ',
		' '
	);
	$ent = array(
	 	'&Agrave;', '&agrave;', '&Aacute;', '&aacute;', '&Acirc;', '&acirc;', '&Atilde;', '&atilde;', '&Auml;', '&auml;', '&Aring;', '&aring;', '&AElig;', '&aelig;', 
		'&Ccedil;', '&ccedil;', 
		'&ETH;', '&eth;', '&Egrave;', '&egrave;', '&Eacute;', '&eacute;', '&Ecirc;', '&ecirc;', '&Euml;', '&euml;', 
		'&Igrave;', '&igrave;', '&Iacute;', '&iacute;', '&Icirc;', '&icirc;', '&Iuml;', '&iuml;', 
		'&Ntilde;', '&ntilde;', 
		'&Ograve;', '&ograve;', '&Oacute;', '&oacute;', '&Ocirc;', '&ocirc;', '&Otilde;', '&otilde;', '&Ouml;', '&ouml;', '&Oslash;', '&oslash;', '&OElig;', '&oelig;', 
		'&szlig;', 
		'&THORN;', '&thorn;', 
		'&Ugrave;', '&ugrave;', '&Uacute;', '&uacute;', '&Ucirc;', '&ucirc;', '&Uuml;', '&uuml;', 
		'&Yacute;', '&yacute;', '&Yuml;', '&yuml;',
		'&nbsp;'
	);
	$newsletter['Newsletter']['email'] = str_replace($ent, $vogais, $newsletter['Newsletter']['email']);

	foreach ($newsletter['Newsletter'] as &$cell):
		$cell = '"' . preg_replace('/"/','""',$cell) . '"';
	endforeach;
	echo "\n" . implode(',', $newsletter['Newsletter']);
endforeach;
?>