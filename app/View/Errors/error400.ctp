<?php
if (Configure::read('debug') > 0):
	?>
	<h2><?php echo $message; ?></h2>
	<p class="error">
		<strong><?php echo __d('cake', 'Error'); ?>: </strong>
		<?php printf(
			__d('cake', 'The requested address %s was not found on this server.'),
			"<strong>'{$url}'</strong>"
		); ?>
	</p>
	<?php
	echo $this->element('exception_stack_trace');
endif;
?>
<section id="cinza">
	<!-- Destaques -->
	<div class="container">
		<div class="row">
			<div class="col text-center erro">
				<?php echo $this->Html->Image('erro.png');?>
			</div>
		</div>
	</div>
</section>
