<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo 'ID: ' . $this->request->data['Socio']['id'] .  ' - ' . $this->request->data['Socio']['nome'];?></h4>
</div>
<div class="modal-body">
	<div class="hostels">
		<?php
		echo $this->Form->create('Socio', array('type' => 'file', 'url' => array('controller' => 'destinos', 'action' => 'add'), 'class' => 'form-horizontal ', 'role' => 'form'));
			echo $this->Form->input('Socio.id');
			?>

			<div class="form-group">
				<label for="SocioNome" class="col-sm-1 control-label">Nome</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('Socio.nome', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="SocioEmpresa" class="col-sm-1 control-label">Empresa</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('Socio.empresa', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="SocioTelefone" class="col-sm-1 control-label">Telefone</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('Socio.telefone', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="SocioEmail" class="col-sm-1 control-label">Email</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('Socio.email', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>

			<?php
			if(isset($this->request->data['Socio']['alvara'])){
				?>
				<div class="form-group">
					<label for="SocioArquivo" class="col-sm-1 control-label"></label>
					<div class="col-sm-11">
						<a href="<?php echo Router::url('/', array('full' => true)) . 'images/socios/' . $this->request->data['Socio']['alvara'];?>" target="_blank">Baixar Alvará</a>
					</div>
				</div>
				<?php
			}
			?>

			<br class="clear" />
			<button type="submit" class="btn btn-default waves-effect waves-light btn-md">Salvar</button>
		</form>
	</div>
</div>