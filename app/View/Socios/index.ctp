<main role="main">
	<section class="mini-header">
		<div style="background-image: url('<?php echo Router::url('/', array('full' => true));?>images/bg_interna.jpg');"></div>
	</section>

	<section id="noticias">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h3 class="mb-4">Associe-se</h3>
					<?php
					echo $this->Form->create('', array('type' => 'file', 'class' => 'profile_form ', 'name' => 'form_contato', 'id' => 'form_contato', 'role' => 'form'));
						?>

						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="socio_nome">Nome</label>
								<input type="text" class="form-control" id="socio_nome" name="data[Socio][nome]">
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="socio_empresa">Empresa</label>
								<input type="text" class="form-control" id="socio_empresa" name="data[Socio][empresa]">
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="socio_telefone">Telefone</label>
								<input type="text" class="form-control" id="socio_telefone" name="data[Socio][telefone]">
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="socio_email">Email</label>
								<input type="email" class="form-control" id="socio_email" name="data[Socio][email]">
							</div>
						</div>						

						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="socio_alvara">Anexar Alvará <small>* PDF, DOC, DOCX, JPG</small></label>
								<input type="file" class="form-control" id="socio_alvara" name="data[Socio][alvara]">
							</div>
						</div>

						<button type="submit" class="btn-profile">ENVIAR</button>
						<span id="message-error" class="msg-error"></span>
					</form>
					<!-- ENDBLOCK FORM -->	

				</div>
				<div class="offset-lg-2 col-lg-4">
					<h3 class="mb-2">Estatuto</h3>
					Clique para baixar o estatuto da associação<br>
					<a href="<?php echo Router::url('/', array('full' => true));?>files/estatuto.pdf">
						<i style="font-size: 32px; padding-top: 15px;" class="fas fa-download"></i>
					</a>
					<br><br>
					<h3 class="mt-3 mb-2">Ata da assembleia de fundação</h3>
					Clique para baixar a ata da assembléia de fundação<br>
					<a href="<?php echo Router::url('/', array('full' => true));?>files/assembleia.pdf">
						<i style="font-size: 32px; padding-top: 15px;" class="fas fa-download"></i>
					</a>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$('#form_contato').on('submit', function(){
			var erros = 0;
			if($('#socio_nome').val() == ''){
				$('#socio_nome').addClass('validation-error');
				erros = 1;
			}else{
				$('#socio_nome').removeClass('validation-error');
			}
			if($('#socio_empresa').val() == ''){
				$('#socio_empresa').addClass('validation-error');
				erros = 1;
			}else{
				$('#socio_empresa').removeClass('validation-error');
			}
			if($('#socio_telefone').val() == ''){
				$('#socio_telefone').addClass('validation-error');
				erros = 1;
			}else{
				$('#socio_telefone').removeClass('validation-error');
			}
			if($('#socio_email').val() == ''){
				$('#socio_email').addClass('validation-error');
				erros = 1;
			}else{
				$('#socio_email').removeClass('validation-error');
			}
			if($('#socio_alvara').val() == ''){
				$('#socio_alvara').addClass('validation-error');
				erros = 1;
			}else{
				$('#socio_alvara').removeClass('validation-error');
			}

			if(erros == 1){
				$('#message-error').html('*Os campos em vermelho possuem erros');
				event.preventDefault();
			}else{
				$(this).submit();
			}

		})

		$('#socio_telefone').mask('(99) 99999-9999');
	});", array('block' => 'scriptBottom'));