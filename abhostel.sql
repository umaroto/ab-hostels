-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 04-Set-2019 às 03:14
-- Versão do servidor: 5.6.44-cll-lve
-- versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abhostel`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `associados`
--

CREATE TABLE `associados` (
  `id` int(11) NOT NULL,
  `arquivo` varchar(255) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `texto` text,
  `site` varchar(255) DEFAULT NULL,
  `reserva` varchar(255) DEFAULT NULL,
  `destino_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `associados`
--

INSERT INTO `associados` (`id`, `arquivo`, `nome`, `texto`, `site`, `reserva`, `destino_id`, `created`) VALUES
(9, 'the-hostel-paulista.jpg', 'The Hostel Paulista', 'lorem ipsum lorem ipsum lorem ipsum', 'https://thehostel.com.br/the-hostel-paulista', 'https://thehostel.com.br/the-hostel-paulista', 1, '2019-08-13 18:44:06'),
(11, '851537a4-2b29-46f4-8dd0-9776c90a13c6.jpg', 'Adrena Sport Hostel', 'Teste', 'http://adrenasporthostel.com.br/', 'http://adrenasporthostel.com.br/', 1, '2019-08-30 18:27:22'),
(12, 'fb232e6161a46b96ff0783ea8268396d.jpg', 'Green Haven Hostel', '', 'http://www.greenhavenhostel.com/', 'http://www.greenhavenhostel.com/', 6, '2019-08-30 18:31:46'),
(13, 'bamboo-logo.jpg', 'Bamboo Rio Hostel', '', 'http://bamboorio.com.br/', 'https://hqbox.hqbeds.com.br/bamboorio/pt-br/hqb/qbQlGDOZGj/availability?arrival=&departure=', 5, '2019-08-30 18:34:38'),
(14, 'capturar.jpg', 'Explorer Hostel', '', 'https://hostelexplorer.com.br/', 'https://hostelexplorer.com.br/', 1, '2019-08-30 18:35:12'),
(15, 'acai.jpg', 'Açai Hostel', '', '', '', 1, '2019-08-30 19:36:07'),
(16, 'elmist.jpg', 'El Misti', '', 'https://www.elmistihostels.com/', 'https://www.elmistihostels.com/', 5, '2019-08-30 19:36:18'),
(17, 'logo-bambu-hostels-brazil.jpg', 'Bambu', '', 'https://bambuhostels.com/', 'https://bambuhostels.com/', 10, '2019-08-30 19:36:29'),
(18, 'logo-maracahostel-1.jpg', 'Maraca Hostel', '', 'https://maracahostel.com.br/', 'https://maracahostel.com.br/', 5, '2019-08-30 19:36:45'),
(19, 'logomenor.jpg', 'Aki Hostel', '', 'https://www.akihostel.com.br/home/', 'https://hotels.cloudbeds.com/reservas/dcu2x1#checkin=2019-08-30&checkout=2019-08-31', 1, '2019-08-30 19:36:55'),
(20, 'logo-no-property.jpg', 'Local Hostel', '', 'https://www.localhostel.com.br/', 'https://www.localhostel.com.br/', 4, '2019-08-30 19:37:07'),
(21, 'logo-yolo-site-novo-212x300.jpg', 'Yolo Hostel', '', 'https://www.booking.com/hotel/br/yolo-hostel.pt-br.html?aid=354415&label=yolo-hostel-1gusZAOgvRhDUeqn0BZTYgS104330739507%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap1t1%3Aneg%3Afi%3Atikwd-72036530874%3Alp1001773%3Ali%3Adec%3Adm&sid=a8615c6a8f1cbfac81db1c32520d6dbf&dest_', 'https://www.booking.com/hotel/br/yolo-hostel.pt-br.html?aid=354415&label=yolo-hostel-1gusZAOgvRhDUeqn0BZTYgS104330739507%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap1t1%3Aneg%3Afi%3Atikwd-72036530874%3Alp1001773%3Ali%3Adec%3Adm&sid=a8615c6a8f1cbfac81db1c32520d6dbf&dest_', 8, '2019-08-30 19:37:20'),
(22, 'the-hostel-vila-mariana.jpg', 'The Hostel Vila Mariana', '', 'https://thehostel.com.br/the-hostel-vila-mariana', 'https://thehostel.com.br/the-hostel-vila-mariana', 1, '2019-08-30 19:37:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `arquivo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `blogs`
--

INSERT INTO `blogs` (`id`, `arquivo`, `slug`, `titulo`, `texto`, `created`) VALUES
(1, 'img-0053.jpg', 'hostel-x-hotel-que-diferenca-faz-o-s', 'HoStel x Hotel: que diferença faz o \"s\"?', '<p class=\"font_7\">O &ldquo;S&rdquo; no meio da palavra hotel d&aacute; origem a uma nova ideia: os hostels. Esses lugares, que est&atilde;o se instalando de vez no Brasil, trazem para o turismo o mesmo que a letra &ldquo;s&rdquo; traz para as palavras: a ideia de mais, de plural, de multiplicar, de compartilhar, de interagir e fazer novas amizades.&nbsp;</p>\r\n<p class=\"font_7\">&nbsp;</p>\r\n<p class=\"font_7\">Em um hostel voc&ecirc; compartilha experi&ecirc;ncias, multiplica seus aprendizados, soma novas ideias e faz muitos amigos!</p>\r\n<p class=\"font_7\">&nbsp;</p>\r\n<p class=\"font_7\">A diferen&ccedil;a desse novo modelo de hospedagem para os hot&eacute;is, &eacute; que voc&ecirc; n&atilde;o vai ficar sozinho nem mesmo se viajar sem conhecer mais ningu&eacute;m em S&atilde;o Paulo. Aqui, as &aacute;reas comuns e atividades internas&nbsp;s&atilde;o compartilhadas e voc&ecirc; vai se sentir de f&eacute;rias na casa de um velho amigo.&nbsp;</p>\r\n<p class=\"font_7\">&nbsp;</p>\r\n<p class=\"font_7\">Tudo aqui dentro &eacute; feito&nbsp;para proporcionar a intera&ccedil;&atilde;o, conviv&ecirc;ncia com pessoas que voc&ecirc; acabou de conhecer, criar novas rela&ccedil;&otilde;es, vivenciar novas aventuras, fazer as coisas que voc&ecirc; quiser, ficar a vontade e se sentir seguro como em sua casa.</p>\r\n<p class=\"font_7\">&nbsp;</p>\r\n<p class=\"font_7\">Para que tudo isso ocorra bem e esse novo modelo de hospedagem conquiste o gosto do brasileiro &eacute; preciso tamb&eacute;m que as pessoas saibam compartilhar os espa&ccedil;os e as experi&ecirc;ncias sempre respeitando ao pr&oacute;ximo e aqui abaixo v&atilde;o algumas dicas de comportamento para quando estiver em um Hostel:</p>\r\n<p class=\"font_7\">&nbsp;</p>\r\n<ul class=\"font_7\">\r\n<li>\r\n<p class=\"font_7\">Sempre que chegar no seu quarto ap&oacute;s &aacute;s 22:00 horas evite ascender as luzes ou fazer muito barulho, pois pode ter algu&eacute;m que j&aacute; esta dormindo e ter&aacute; &nbsp;um compromisso no dia seguinte;</p>\r\n</li>\r\n<li>\r\n<p class=\"font_7\">Durante a noite utilize as luzes do celular ou a luz individual da sua cama, pois assim voc&ecirc; n&atilde;o incomoda quem j&aacute; dormiu;</p>\r\n</li>\r\n<li>\r\n<p class=\"font_7\">Se voc&ecirc; for uma pessoa que costuma roncar durante a noite, fica ligado pois algu&eacute;m ir&aacute; te cutucar durante a noite para virar e diminuir o barulho. Caso precise todas nossas unidades oferecem quartos privativos;</p>\r\n</li>\r\n<li>\r\n<p class=\"font_7\">Nunca fa&ccedil;a julgamentos ou tenha&nbsp;preconceitos dentro de um Hostel quanto a etnia, cor, classe social e op&ccedil;&atilde;o sexual, todos devem respeitar um ao outro para ter um bom conv&iacute;vio;</p>\r\n</li>\r\n<li>\r\n<p class=\"font_7\">Sempre que for tomar banho, se seque dentro do box, assim voc&ecirc; n&atilde;o molha o banheiro e mant&eacute;m ele limpo para o pr&oacute;ximo que for usar. Lembre que voc&ecirc; tamb&eacute;m foi pr&oacute;ximo de algu&eacute;m;</p>\r\n</li>\r\n<li>\r\n<p class=\"font_7\">Utilize sua cama ou os ganchos para colocar sua toalha para secar;&nbsp;</p>\r\n</li>\r\n<li>\r\n<p class=\"font_7\">Quando for tomar banho n&atilde;o esque&ccedil;a de levar sua toalha, sua roupa &iacute;ntima, e um shorts, assim voc&ecirc; consegue pendurar tudo no box e at&eacute; se trocar dentro do box;</p>\r\n</li>\r\n</ul>', '2019-01-31 16:56:26'),
(4, 'ubatuba-660x330.jpg', 'prefeitura-de-ubatuba-cria-regras-para-quem-aluga-a-propria-casa-para-temporada', 'Prefeitura de Ubatuba cria regras para quem aluga a própria casa para temporada', '<p>O setor hoteleiro de Ubatuba conseguiu uma importante vit&oacute;ria contra as loca&ccedil;&otilde;es informais de quartos e resid&ecirc;ncias por temporada e os aplicativos e sites que facilitam esses neg&oacute;cios, como o popular AirBNB. Aluguel &ldquo;por temporada&rdquo; na defini&ccedil;&atilde;o da lei seria aluguel por um per&iacute;odo de at&eacute; 45 dias ininterruptos.</p>\r\n<p>Inclu&iacute;do de &uacute;ltima hora na pauta da sess&atilde;o da &uacute;ltima ter&ccedil;a-feira (12), junto com outros projetos pol&ecirc;micos, o projeto aprovado cria uma s&eacute;rie de exig&ecirc;ncias aos quais os propriet&aacute;rios de im&oacute;veis com mais de tr&ecirc;s acomoda&ccedil;&otilde;es com leitos dispon&iacute;veis dever&atilde;o se adequar caso queiram oferec&ecirc;-los para alugar. Os sites de agenciamento de hospedagem que aceitarem im&oacute;veis n&atilde;o autorizados pela Prefeitura sofrer&atilde;o multa di&aacute;ria por im&oacute;vel anunciado.</p>\r\n<p>Quando entrar em vigor, para oferecer hospedagem nas modalidades Cama e Caf&eacute; (ou Bad and Breakfast) e residencial ser&aacute; preciso abrir uma empresa, inscrev&ecirc;-la no Cadastur do Minist&eacute;rio do Turismo, conseguir alvar&aacute;s do Corpo de Bombeiros e dos &oacute;rg&atilde;os de Vigil&acirc;ncia Sanit&aacute;ria, pagar as taxas dos sindicatos patronal e laboral.</p>\r\n<p>A empresa deve ainda possuir seguro com cobertura para os h&oacute;spedes e ter procedimentos para combater a explora&ccedil;&atilde;o sexual infantil.</p>\r\n<p>Em trinta dias ap&oacute;s a publica&ccedil;&atilde;o da Lei, a Prefeitura dever&aacute; regulamentar as taxas que ser&atilde;o cobradas desses &ldquo;empreendimentos&rdquo; que passar&atilde;o a ser considerados im&oacute;veis comerciais.</p>\r\n<p>Os propriet&aacute;rios dever&atilde;o ainda fornecer ao munic&iacute;pio relat&oacute;rios mensais sobre suas atividades.</p>\r\n<p>O projeto aprovado por unanimidade na C&acirc;mara prev&ecirc; ainda que caso o im&oacute;vel fique dentro de um condom&iacute;nio, ser&aacute; preciso que conste no estatuto do mesmo a autoriza&ccedil;&atilde;o para que os propriet&aacute;rios aluguem os im&oacute;veis por temporada. &Eacute; preciso ainda que o mesmo ato constitutivo autorize expressamente a entrada da fiscaliza&ccedil;&atilde;o no condom&iacute;nio.</p>\r\n<p>Na justificativa do projeto, o Secret&aacute;rio de Turismo, Luiz Antonio Bischof, afirma que o Munic&iacute;pio &ldquo;tem o dever e objetivo de proporcionar um turismo adequado aos visitantes face &agrave;s suas belezas naturais, de forma respons&aacute;vel e de qualidade&rdquo;. Segundo ele, o objetivo do projeto seria &ldquo;estabelecer os m&iacute;nimos padr&otilde;es de seguran&ccedil;a e confiabilidade na execu&ccedil;&atilde;o dos servi&ccedil;os&rdquo;.</p>\r\n<p>Segundo o site da Prefeitura, Luiz Antonio Bischof &ldquo;&eacute; empres&aacute;rio do setor tur&iacute;stico, desde 1982 e tamb&eacute;m do ramo da constru&ccedil;&atilde;o civil, desde 1970. &Eacute; diretor financeiro do Sindicato de Hot&eacute;is, Bares e Restaurantes do Litoral Norte h&aacute; tr&ecirc;s gest&otilde;es. Bischof j&aacute; foi funcion&aacute;rio da Secretaria de Turismo no per&iacute;odo de 1968 a 1970. Entre suas diversas ocupa&ccedil;&otilde;es em &oacute;rg&atilde;os ligados ao turismo destacam-se a presid&ecirc;ncia da Companhia Municipal de Turismo na gest&atilde;o 2001/2003, o cargo de Delegado da Federa&ccedil;&atilde;o de Hot&eacute;is do Estado de S&atilde;o Paulo.&rdquo;</p>\r\n<p>LEIA O PROJETO NA &Iacute;NTEGRA: Projeto de Lei n&ordm; 112 de 2017 (OBS: Este projeto foi aprovado com uma emenda do vereador Claudinei Xavier, PSDB, que prop&ocirc;s incluir no artigo 1&ordm; as palavras &ldquo;com leitos dispon&iacute;veis&rdquo;, ficando o texto com a seguinte reda&ccedil;&atilde;o: &ldquo;[&hellip;] com mais de tr&ecirc;s acomoda&ccedil;&otilde;es com leitos dispon&iacute;veis [&hellip;])</p>\r\n<p>Fonte: http://informarubatuba.com.br</p>', '2019-08-04 20:39:38'),
(5, 'img-20171130-173217420.jpg', 'hostels-buscam-apoio-no-ministerio-publico-federal-para-resolver-questao-de-regulamentacao-do-setor', 'Hostels buscam apoio no Ministério Publico Federal para resolver questão de regulamentação do setor', '<p>O Minist&eacute;rio Publico Federal atrav&eacute;s do protocolo 20170098768 impetrado obedecendo a legisla&ccedil;&atilde;o da Constitui&ccedil;&atilde;o Federal Brasileira. vai buscar junto ao Governo Federal informa&ccedil;&otilde;es para que haja a concretiza&ccedil;&atilde;o do processo de regulamenta&ccedil;&atilde;o da categoria de hostels e novas hospitalidades que vem sedo trabalhada por empres&aacute;rios e empreendedores de turismo de hospedagem h&aacute; v&aacute;rios anos.</p>\r\n<p>O pedido que esta tramitando no MPF, foi elaborado pela Associa&ccedil;&atilde;o Nacional do Turismo Afro Brasileiro &ndash; ANTAB e pela coordena&ccedil;&atilde;o do projeto Guetto Brazil Holding, entidades parceiras da Associa&ccedil;&atilde;o Brasileira de Hostels e Novas Hospitalidades, dirigida pelo empres&aacute;rio Jo&atilde;o Paulo Amorim.</p>\r\n<p>A meta &eacute; que atrav&eacute;s desta a&ccedil;&atilde;o, seja definida as normas regulat&oacute;rias necess&aacute;rias para que o setor ganhe a notoriedade merecida. Os hostels que s&atilde;o um p&oacute;los de experi&ecirc;ncia, de cultura e etnias s&atilde;o apontados hoje por muitas institui&ccedil;&otilde;es e profissionais da &aacute;rea de turismo, como atrativos importantes de integra&ccedil;&atilde;o de g&ecirc;neros e ra&ccedil;as.</p>\r\n<p>Diversas atividades de promo&ccedil;&atilde;o e informa&ccedil;&atilde;o sobre o seguimento est&atilde;o sendo preparadas entre elas uma s&eacute;rie de audi&ecirc;ncias p&uacute;blicas, sobre o seguimento no estado de S&atilde;o Paulo &aacute; partir de Mar&ccedil;o de 2018.</p>\r\n<p>A presid&ecirc;ncia da ABHOSTEL informa ainda que em Janeiro estar&aacute; com uma comiss&atilde;o em Bras&iacute;lia conversando parlamentares e minist&eacute;rios, tratando sobre as necessidades existentes sobre pol&iacute;ticas p&uacute;blicas e investimentos no mercado de novas hospitalidades no Brasil a partir de 2018.</p>\r\n<p>Fonte:- Francisco Henrique Silvino</p>\r\n<p>Portal ANTAB</p>', '2019-08-04 20:47:49'),
(6, 'unnamed-5.jpg', 'oab-rj-recebe-audiencia-publica-da-comissao-de-turismo-da-camara', 'OAB/RJ recebe audiência pública da Comissão de Turismo da Câmara', '<div>Fonte: reda&ccedil;&atilde;o da Tribuna do Advogado</div>\r\n<div>&nbsp;</div>\r\n<div><strong>Vitor Fraga</strong></div>\r\n<div>O Plen&aacute;rio Evandro Lins e Silva recebeu nesta segunda-feira, dia 26, uma audi&ecirc;ncia p&uacute;blica da Comiss&atilde;o de Turismo da C&acirc;mara dos Deputados. O evento teve como pauta a apresenta&ccedil;&atilde;o do painel&nbsp;<em>Hostels, novas hospitalidades e o turismo do futuro</em>, promovido pela Comiss&atilde;o de Turismo (CT) da OAB/RJ. O encontro, cujo objetivo foi debater o cen&aacute;rio atual e futuro para o setor de turismo, foi transmitido pelo canal da&nbsp;<a href=\"https://www.youtube.com/user/oabriodejaneiro/videos\" target=\"_blank\" rel=\"noopener\" data-saferedirecturl=\"https://www.google.com/url?hl=pt-BR&amp;q=https://www.youtube.com/user/oabriodejaneiro/videos&amp;source=gmail&amp;ust=1522782887499000&amp;usg=AFQjCNERX6yKPI-RfUOIVz8OEhVO2dkbrQ\"><em><strong>Ordem no YouTube.</strong></em></a></div>\r\n<div>&nbsp;</div>\r\n<div>O deputado federal Otavio Leite (PSDB/RJ), membro da Comiss&atilde;o de Turismo da C&acirc;mara e autor do requerimento para a realiza&ccedil;&atilde;o da audi&ecirc;ncia na sede da Seccional, comandou a mesa de abertura. &ldquo;Esta &eacute; uma sess&atilde;o oficial da C&acirc;mara, com o objetivo de que o&nbsp;<em>trade</em>&nbsp;do Rio de Janeiro possa ouvir e compreender melhor esse fen&ocirc;meno das novas modalidades de hospedagem. Trata-se de um segmento importante da sociedade, que tem cada vez mais peso, e conjuga uma atividade que promove o que nos une, que &eacute; o fortalecimento da bandeira do turismo&rdquo;, disse Leite, que tamb&eacute;m &eacute; advogado, na abertura.</div>\r\n<div>&nbsp;</div>\r\n<div>\r\n<p>Tamb&eacute;m integrante da mesa, o presidente da CT da OAB/RJ, Hamilton Vasconcellos, refor&ccedil;ou a import&acirc;ncia da realiza&ccedil;&atilde;o da audi&ecirc;ncia no estado. &ldquo;Nossa ideia &eacute; que as pessoas entendam a causa. Precisamos explicar muito bem em que essas novas modalidades afetam a vida cotidiana das pessoas. A presen&ccedil;a do deputado Otavio Leite d&aacute; ainda mais import&acirc;ncia ao evento&rdquo;, afirmou.</p>\r\n<p>O debate abordou a quest&atilde;o das novas formas de hospitalidade, especificamente os hostels, e seu crescente potencial econ&ocirc;mico. O evento foi realizado em parceria com o Sindicato Estadual dos Guias de Turismo do Rio de Janeiro (Sindegtur), a Associa&ccedil;&atilde;o Brasileira de Hostels e Novas Hospitalidades (ABHostels) e o Rio Host.</p>\r\n</div>', '2019-09-03 21:50:00'),
(7, 'ubatuba-660x330-2.jpg', 'prefeitura-de-ubatuba-cria-regras-para-quem-aluga-a-propria-casa-para-temporada_', 'Prefeitura de Ubatuba cria regras para quem aluga a própria casa para temporada', '<p>O setor hoteleiro de Ubatuba conseguiu uma importante vit&oacute;ria contra as loca&ccedil;&otilde;es informais de quartos e resid&ecirc;ncias por temporada e os aplicativos e sites que facilitam esses neg&oacute;cios, como o popular AirBNB. Aluguel &ldquo;por temporada&rdquo; na defini&ccedil;&atilde;o da lei seria aluguel por um per&iacute;odo de at&eacute; 45 dias ininterruptos.</p>\r\n<p>Inclu&iacute;do de &uacute;ltima hora na&nbsp;pauta da sess&atilde;o da &uacute;ltima ter&ccedil;a-feira (12), junto com outros projetos pol&ecirc;micos, o projeto aprovado cria uma s&eacute;rie de exig&ecirc;ncias aos quais os propriet&aacute;rios de im&oacute;veis com mais de tr&ecirc;s acomoda&ccedil;&otilde;es com leitos dispon&iacute;veis dever&atilde;o se adequar caso queiram oferec&ecirc;-los para alugar. Os sites de agenciamento de hospedagem que aceitarem im&oacute;veis n&atilde;o autorizados pela Prefeitura sofrer&atilde;o multa di&aacute;ria por im&oacute;vel anunciado.</p>\r\n<p>Quando entrar em vigor, para oferecer hospedagem nas modalidades Cama e Caf&eacute; (ou Bad and Breakfast) e residencial ser&aacute; preciso abrir uma empresa, inscrev&ecirc;-la no Cadastur do Minist&eacute;rio do Turismo, conseguir alvar&aacute;s do Corpo de Bombeiros e dos &oacute;rg&atilde;os de Vigil&acirc;ncia Sanit&aacute;ria, pagar as taxas dos sindicatos patronal e laboral.</p>\r\n<p>A empresa deve ainda possuir seguro com cobertura para os h&oacute;spedes e ter procedimentos para combater a explora&ccedil;&atilde;o sexual infantil.</p>\r\n<p>Em trinta dias ap&oacute;s a publica&ccedil;&atilde;o da Lei, a Prefeitura dever&aacute; regulamentar as taxas que ser&atilde;o cobradas desses &ldquo;empreendimentos&rdquo; que passar&atilde;o a ser considerados im&oacute;veis comerciais.</p>\r\n<p>Os propriet&aacute;rios dever&atilde;o ainda fornecer ao munic&iacute;pio relat&oacute;rios mensais sobre suas atividades.</p>\r\n<p>O projeto aprovado por unanimidade na C&acirc;mara prev&ecirc; ainda que caso o im&oacute;vel fique dentro de um condom&iacute;nio, ser&aacute; preciso que conste no estatuto do mesmo a autoriza&ccedil;&atilde;o para que os propriet&aacute;rios aluguem os im&oacute;veis por temporada. &Eacute; preciso ainda que o mesmo ato constitutivo autorize expressamente a entrada da fiscaliza&ccedil;&atilde;o no condom&iacute;nio.</p>\r\n<p>Na justificativa do projeto, o Secret&aacute;rio de Turismo, Luiz Antonio Bischof, afirma que o Munic&iacute;pio&nbsp;<em>&ldquo;tem o dever e objetivo de proporcionar um turismo adequado aos visitantes face &agrave;s suas belezas naturais, de forma respons&aacute;vel e de qualidade&rdquo;</em>. Segundo ele, o objetivo do projeto seria&nbsp;<em>&ldquo;estabelecer os m&iacute;nimos padr&otilde;es de seguran&ccedil;a e confiabilidade na execu&ccedil;&atilde;o dos servi&ccedil;os&rdquo;</em>.</p>\r\n<p>Segundo o site da Prefeitura,&nbsp;Luiz Antonio Bischof&nbsp;&nbsp;<em>&ldquo;&eacute;&nbsp;empres&aacute;rio do setor tur&iacute;stico, desde 1982 e tamb&eacute;m do ramo da constru&ccedil;&atilde;o civil, desde 1970. &Eacute; diretor financeiro do Sindicato de Hot&eacute;is, Bares e Restaurantes do Litoral Norte h&aacute; tr&ecirc;s gest&otilde;es.&nbsp;Bischof j&aacute; foi funcion&aacute;rio da Secretaria de Turismo no per&iacute;odo de 1968 a 1970. Entre suas diversas ocupa&ccedil;&otilde;es em &oacute;rg&atilde;os ligados ao turismo destacam-se a presid&ecirc;ncia da Companhia Municipal de Turismo na gest&atilde;o 2001/2003, o cargo de Delegado da Federa&ccedil;&atilde;o de Hot&eacute;is do Estado de S&atilde;o Paulo.&rdquo;&nbsp;</em></p>\r\n<p>LEIA O PROJETO NA &Iacute;NTEGRA:&nbsp;<a href=\"http://informarubatuba.com.br/wp-content/uploads/2017/12/Projeto-de-Lei-n%C2%BA-112-de-2017.pdf\" target=\"_blank\" rel=\"noopener noreferrer\">Projeto de Lei n&ordm; 112 de 2017</a>&nbsp;(OBS: Este projeto foi aprovado com uma emenda do vereador Claudinei Xavier, PSDB, que prop&ocirc;s incluir no artigo 1&ordm; as palavras &ldquo;com leitos dispon&iacute;veis&rdquo;, ficando o texto com a seguinte reda&ccedil;&atilde;o:&nbsp;&nbsp;&ldquo;[&hellip;]&nbsp;com mais de tr&ecirc;s acomoda&ccedil;&otilde;es&nbsp;<em><strong>com leitos dispon&iacute;veis</strong></em>&nbsp;[&hellip;])</p>\r\n<p>&nbsp;</p>\r\n<p><a href=\"Fonte: http://informarubatuba.com.br\">Fonte: http://informarubatuba.com.br</a></p>', '2019-09-03 21:51:10'),
(8, 'img-20171130-173217420-2.jpg', 'hostels-buscam-apoio-no-ministerio-publico-federal-para-resolver-questao-de-regulamentacao-do-setor_', 'Hostels buscam apoio no Ministério Publico Federal para resolver questão de regulamentação do setor', '<p>O Minist&eacute;rio Publico Federal atrav&eacute;s do protocolo&nbsp; 20170098768 impetrado obedecendo a legisla&ccedil;&atilde;o da Constitui&ccedil;&atilde;o Federal Brasileira.&nbsp; vai buscar junto ao Governo&nbsp;Federal informa&ccedil;&otilde;es para que haja a concretiza&ccedil;&atilde;o do processo de regulamenta&ccedil;&atilde;o da categoria de hostels e novas hospitalidades que vem sedo trabalhada por empres&aacute;rios e empreendedores de turismo de hospedagem h&aacute; v&aacute;rios anos.</p>\r\n<p class=\"m_-4760575833158155127gmail-MsoNormal\">O pedido que esta tramitando no MPF, foi elaborado pela Associa&ccedil;&atilde;o Nacional do Turismo Afro Brasileiro &ndash; ANTAB e pela&nbsp; coordena&ccedil;&atilde;o do projeto Guetto Brazil Holding, entidades parceiras da Associa&ccedil;&atilde;o Brasileira de Hostels e Novas Hospitalidades, dirigida pelo&nbsp; empres&aacute;rio Jo&atilde;o Paulo Amorim.</p>\r\n<p class=\"m_-4760575833158155127gmail-MsoNormal\">A meta &eacute; que atrav&eacute;s desta a&ccedil;&atilde;o, seja definida as normas regulat&oacute;rias necess&aacute;rias para que o setor ganhe a notoriedade merecida. Os hostels que s&atilde;o um p&oacute;los de experi&ecirc;ncia, de cultura e etnias s&atilde;o apontados hoje por muitas institui&ccedil;&otilde;es e profissionais da &aacute;rea de turismo, como atrativos importantes de integra&ccedil;&atilde;o de g&ecirc;neros e ra&ccedil;as.</p>\r\n<p class=\"m_-4760575833158155127gmail-MsoNormal\">Diversas atividades de promo&ccedil;&atilde;o e informa&ccedil;&atilde;o sobre o seguimento est&atilde;o sendo preparadas entre elas uma s&eacute;rie de audi&ecirc;ncias p&uacute;blicas, sobre o seguimento no estado de S&atilde;o Paulo &aacute; partir de Mar&ccedil;o de 2018.</p>\r\n<p class=\"m_-4760575833158155127gmail-MsoNormal\">A presid&ecirc;ncia da ABHOSTEL informa ainda que em Janeiro estar&aacute; com uma comiss&atilde;o em Bras&iacute;lia conversando &nbsp;&nbsp;parlamentares e minist&eacute;rios, tratando sobre as necessidades existentes sobre pol&iacute;ticas p&uacute;blicas e investimentos no mercado de novas hospitalidades no Brasil a partir de 2018.</p>\r\n<p class=\"m_-4760575833158155127gmail-MsoNormal\"><strong>Fonte:- Francisco Henrique Silvino</strong></p>', '2019-09-03 21:51:55'),
(9, 'hostel-bambu-foto-amanda-bechlin-1024x768-1024x768.jpg', 'abhostels-inicia-participacao-no-festival-das-cataratas', 'ABHOSTELS inicia participação no Festival das Cataratas', '<h4>A Associa&ccedil;&atilde;o Brasileira de Hostels e Novas Hospitalidades, a&nbsp;ABHOSTELS, inicia este ano sua participa&ccedil;&atilde;o no Festival das Cataratas, o&nbsp;maior evento de turismo de Foz do Igua&ccedil;u e o segundo maior da regi&atilde;o Sul do&nbsp;pa&iacute;s. O Festival acontece entre os dias 28 e 30 de junho no Hotel Rafain&nbsp;Palace, e re&uacute;ne agentes de viagens, operadores de turismo, hoteleiros, guias e&nbsp;demais profissionais da &aacute;rea.</h4>\r\n<h4>De acordo com o empres&aacute;rio e diretor regional da ABHOSTELS, Diogo Marcel&nbsp;Ara&uacute;jo, a participa&ccedil;&atilde;o no festival &eacute; importante para debater a&ccedil;&otilde;es que&nbsp;fortale&ccedil;am o setor. &ldquo;O Festival &eacute; um dos quatro maiores eventos de turismo do&nbsp;pa&iacute;s, por isso aproveitamos a oportunidade para divulgar o trabalho da&nbsp;associa&ccedil;&atilde;o, que apesar de nova, j&aacute; ganhou destaque em diversas cidades do&nbsp;Brasil&rdquo;, afirma.</h4>\r\n<h4>Outro bom motivo &eacute; a oportunidade de esclarecer o p&uacute;blico sobre o conceito de&nbsp;hostel e o funcionamento deste tipo de hospitalidade. &ldquo;Muitas pessoas ainda&nbsp;t&ecirc;m d&uacute;vidas sobre como funciona um hostel, e qual a diferen&ccedil;a dele com&nbsp;outros meios de hospedagem. Por isso, muitos agentes de viagens acabam&nbsp;n&atilde;o vendendo esse produto. Durante o festival teremos a oportunidade de&nbsp;esclarecer&rdquo;, disse Diogo.</h4>\r\n<h4>S&oacute;cia propriet&aacute;ria do maior hostel em container do Brasil, Karin Nisiide explica&nbsp;que por ter um custo inferior ao da hotelaria convencional, a acomoda&ccedil;&atilde;o em&nbsp;hostel ainda sofre preconceitos. &ldquo;Principalmente os brasileiros ainda acham&nbsp;que esse tipo de hospedagem &eacute; para classe baixa, que &eacute; sujo, desorganizado.&nbsp;N&oacute;s queremos desmistificar, mostrar que o hostel pode ter conforto, mas&nbsp;tamb&eacute;m a vantagem de um ambiente agrad&aacute;vel, que proporciona experi&ecirc;ncias&nbsp;e intera&ccedil;&atilde;o entre os h&oacute;spedes&rdquo;.</h4>\r\n<h4>Os empres&aacute;rios afirmam que a hospedagem em hostel ganhou maior&nbsp;visibilidade nos &uacute;ltimos cinco anos. Isso acontece porque cada vez mais os&nbsp;viajantes priorizam as intera&ccedil;&otilde;es sociais dentro do destino. &ldquo;Muito mais do que&nbsp;fazer uma simples viagem, o turista quer viver aquele destino, conhecer os&nbsp;lugares, mas tamb&eacute;m as pessoas. O turismo de experi&ecirc;ncia &eacute; um novo nicho&nbsp;de mercado, mas sempre foi a ess&ecirc;ncia dos hostels&rdquo; complementa Diogo.</h4>\r\n<h4><strong>Hospedagem</strong></h4>\r\n<h4>Ao contr&aacute;rio do que muita gente pensa, hostel n&atilde;o &eacute; somente para jovens e&nbsp;mochileiros viajantes. O mercado tem sido apontado como prefer&ecirc;ncia tamb&eacute;m&nbsp;das fam&iacute;lias, que d&atilde;o prioridade &agrave; cozinha compartilhada para fazer suas&nbsp;refei&ccedil;&otilde;es. Outro p&uacute;blico em destaque &eacute; o de executivos, que ap&oacute;s um dia de&nbsp;reuni&otilde;es, fogem do clima da hotelaria convencional e se divertem em festas,&nbsp;muito frequente nos hostels.</h4>\r\n<h4>A descontra&ccedil;&atilde;o do ambiente e a facilidade em fazer amizades tamb&eacute;m fazem&nbsp;parte dos atrativos. No hostel, o h&oacute;spede pode optar por quartos e banheiros&nbsp;compartilhados ou privativos &ndash; o pre&ccedil;o varia conforme a acomoda&ccedil;&atilde;o. &ldquo;Hostel &eacute;&nbsp;estilo de vida, e quem se hospeda em um n&atilde;o est&aacute; em busca somente da&nbsp;economia, mas da experi&ecirc;ncia, em conhecer novas pessoas, fazer amizades&rdquo;,&nbsp;ressalta Diogo, que atua na &aacute;rea h&aacute; 16 anos.</h4>\r\n<h4><strong>Exposi&ccedil;&atilde;o</strong></h4>\r\n<h4>Durante o Festival, os visitantes que passarem pelo estande da ABHOSTELS&nbsp;poder&atilde;o entender melhor como tudo funciona. &ldquo;Vamos recriar o ambiente de&nbsp;um hostel, com toda a vibe que &eacute; nosso diferencial. Ser&aacute; um ambiente&nbsp;descontra&iacute;do onde as pessoas v&atilde;o se divertir. Muitas surpresas est&atilde;o por vir&rdquo;,&nbsp;adianta Karin. Em sua 12&ordf; edi&ccedil;&atilde;o, o Festival das Cataratas deve receber mais&nbsp;de 7 mil pessoas em tr&ecirc;s dias de evento.</h4>\r\n<h4><strong>Associa&ccedil;&atilde;o</strong></h4>\r\n<h4>Fundada em setembro de 2016, a ABHOSTELS tem como compromisso o&nbsp;fortalecimento e a expans&atilde;o do setor em todo o pa&iacute;s. Em Foz do Igua&ccedil;u, os&nbsp;hostels Bambu, Green House, Poesia, Tetris Container, Iguassu Guest House e&nbsp;Made Inn representam a associa&ccedil;&atilde;o.</h4>', '2019-09-03 21:52:45'),
(10, 'img-20171130-150254197.jpg', 'ata-oficial-do-encontro-internacional-de-cultura-negocios-e-turismo-etnico', 'ATA OFICIAL DO ENCONTRO INTERNACIONAL DE CULTURA, NEGÓCIOS E TURISMO ÉTNICO', '<p>Em 30 de Novembro de 2017, no Audit&oacute;rio da Secretaria de Cultura do Estado de S&atilde;o Paulo, situada na Rua Maua, 51 centro da capital,no per&iacute;odo das 14 &aacute;s 18 horas, ocorreu a realiza&ccedil;&atilde;o do &ldquo; ENCONTRO INTERNACIONAL T&Eacute;CNICO DE CULTURA, NEG&Oacute;CIOS E TURISMO ETNICO&rdquo;, uma realiza&ccedil;&atilde;o organizada pela Associa&ccedil;&atilde;o Nacional do Turismo Afro Brasileiro -ANTAB, em parceria com a empresa FHS CONSULTORIA DE TURISMO &Eacute;TNICO &amp; NEG&Oacute;CIOS.</p>\r\n<p>A organiza&ccedil;&atilde;o do evento contou ainda com a participa&ccedil;&atilde;o da Associa&ccedil;&atilde;o Brasileira de Hostels, da<br />Bernatur Turismo &amp; Interc&acirc;mbios como parceiros diretos na execu&ccedil;&atilde;o.</p>\r\n<p>Os apoiadores da atividade foram, &aacute; Funda&ccedil;&atilde;o&nbsp;Cultural Palmares que representou tamb&eacute;m ao Minist&eacute;rio da Cultura; a Coordena&ccedil;&atilde;o de A&ccedil;&otilde;es Afirmativas da Seppir, que representou tamb&eacute;m o Minist&eacute;rio de Direitos Humanos; o Coletivo de Empres&aacute;rios Afro Brasileiro &ndash; Ceabra; &aacute; Secretaria de Cultura do Estado de S&atilde;o Paulo; &aacute; Plataforma<br />Di&aacute;spora Black; o Instituto Angolano de Com&eacute;rcio Exterior e Media&ccedil;&atilde;o; a Secretaria Municipal de Cultura de S&atilde;o Paulo; &aacute; S&atilde;o Paulo Turismo; &aacute; Frente Brasil Favela; &aacute; Favela Scene e o Conselho Municipal da Comunidade Negra de S&atilde;o Paulo.</p>\r\n<p>A abertura do evento se deu &aacute;s 14:00 horas com a sauda&ccedil;&atilde;o realizada pelo presidente da Associa&ccedil;&atilde;o Nacional do Turismo Afro Brasileiro &ndash; ANTAB, senhor FRANCISCO HENRIQUE SILVINO a todos os participantes, fazendo em seguida a composi&ccedil;&atilde;o da mesa de autoridades do evento que contou com as seguintes personalidades:- Sr. ERIVALDO DE OLIVERA SILVA &ndash; Presidente da Funda&ccedil;&atilde;o Cultural Palmares; Sra. ROSELI DE OLIVEIRA &ndash; Coordenadora de Assuntos Tem&aacute;ticos e A&ccedil;&otilde;es Afirmativas da Seppir representado o Minist&eacute;rio de Direitos<br />Humanos; Sr. BARNAB&Eacute; MANUEL &ndash; Presidente da Bernatur Turismo &amp;amp; Interc&acirc;mbios; Sr. JO&Atilde;O PAULO<br />AMORIM &ndash; Presidente da Associa&ccedil;&atilde;o Brasileira de Hostels e Novas Hospitalidades; Sr. GILSON</p>\r\n<p>FUMA&Ccedil;A &ndash; Presidente do Favela Scebe; Sr. CARLOS &ndash; Diretor da Plataforma Di&aacute;spora Black.<br />O presidente da ANTAB, abriu os trabalhos realizando um resumo sobre as atividades efetuadas pela entidade ao longo dos anos, com refer&ecirc;ncia ao Turismo &Eacute;tnico e tamb&eacute;m ao Afro Neg&oacute;cio, em seguida passou a palavra ao Presidente da Funda&ccedil;&atilde;o Cultural Palmares, senhor ERIVALDO OLIVEIRA SILVA que fez uma palestra de grande<br />riqueza colocando a necessidade de haver maior participa&ccedil;&atilde;o da comunidade negra dentro dos projetos existentes no pa&iacute;s, ele destacou com muita &ecirc;nfase ao trabalho que foi realizado na Serra da Barriga em Alagoas, tamb&eacute;m fez destaque a necessidade de implanta&ccedil;&atilde;o de pol&iacute;ticas publicas para o setor, colou a FCP a disposi&ccedil;&atilde;o para atuar<br />junto ao Governo Federal em busca de solu&ccedil;&otilde;es para a valoriza&ccedil;&atilde;o do seguimento e confirmou a presen&ccedil;a da FCP como integrante do Grupo de Trabalho formado por sugest&atilde;o dele mesmo que dar&aacute; continuidade as a&ccedil;&otilde;es e projetos que ser&atilde;o elaborados e trabalhados em prol do setor no Brasil.</p>\r\n<p>Em seguida a senhora ROSELI DE OLIVEIRA da Seppir, fez sua manifesta&ccedil;&atilde;o colocando a import&acirc;ncia do evento, para toda a popula&ccedil;&atilde;o negra brasileira, que &eacute; marginalizada em v&aacute;rios aspectos. Citou sobre a quest&atilde;o da exist&ecirc;ncia do RACISMO INSTITUCIONAL, das poucas oportunidades que existem para os negros na atividade tur&iacute;stica brasileira. Mencionou com muita propriedade a necessidade de que se haja maior cobran&ccedil;a de a&ccedil;&otilde;es por parte de todos, nas quest&otilde;es de solu&ccedil;&otilde;es imediatas para o problema do preconceito e desigualdade em todas as &aacute;reas no<br />pa&iacute;s. Ela tamb&eacute;m ressaltou a import&acirc;ncia do Grupo de Trabalho que foi idealizado e constru&iacute;do que ser&aacute; a frente de atua&ccedil;&atilde;o para o seguimento junto aos parlamentares em todas as esferas.</p>\r\n<p>O empres&aacute;rio e presidente da Associa&ccedil;&atilde;o Brasileira de Hostels e Novas Hospitalidades, senhor JO&Atilde;O PAULO AMORIM, foi o terceiro a se pronunciar fazendo refer&ecirc;ncia ao trabalho que se feito dentro do setor de hospedagens nos hostels. Ele citou com veem&ecirc;ncia que os pol&iacute;ticos precisam estar mais pr&oacute;ximos das necessidades que o setor de turismo das &ldquo;minomaiorias&rdquo; precisam para se estruturar e desenvolver.</p>\r\n<p>O presidente da Bernatur Turismo &amp; Interc&acirc;mbios, senhor BERNABE MANUEL, fez a cita&ccedil;&atilde;o de que existe muito interesse por parte dos africanos e afro americanos em conhecer o Turismo &Eacute;tnico no Brasil, ele destacou que a realiza&ccedil;&atilde;o de mais a&ccedil;&otilde;es direcionadas pode com certeza aumentar a valoriza&ccedil;&atilde;o dos produtos e destinos tur&iacute;sticos,<br />fazendo com que haja cada vez mais pessoas com inten&ccedil;&atilde;o de viajar para o Brasil em busca de novas experi&ecirc;ncias.</p>\r\n<p>Foi destaco ainda, que os quilombos s&atilde;o o maior produto de interesse em visita&ccedil;&atilde;o por parte dos africanos e afro americanos e que com o desenvolvimento de projetos de capacita&ccedil;&atilde;o junto as comunidades, pode levar com certeza para todos os moradores do espa&ccedil;o maiores oportunidades de gera&ccedil;&atilde;o de renda e sustentabilidade.</p>\r\n<p>Na sequ&ecirc;ncia o Guia de Turismo &Eacute;tnico e Presidente do Favela Scene, senhor GILSON FUMA&Ccedil;A fez men&ccedil;&atilde;o ao trabalho de Turismo Comunit&aacute;rio na Favela de Santa Marta no Rio de Janeiro, citou ainda que existe um grande potencial no setor do Turismo &Eacute;tnico que n&atilde;o &eacute; explorado.</p>\r\n<p>Uma das a&ccedil;&otilde;es mais importantes destacada por &eacute; a necessidade de se haver &aacute; integra&ccedil;&atilde;o entre as lideran&ccedil;as no Brasil que trabalham pela ascens&atilde;o do setor.</p>\r\n<p>No pronunciamento do senhor CARLOS, Diretor da Plataforma Di&aacute;spora Black, obteve-se no evento um momento especial, com a apresenta&ccedil;&atilde;o de percentuais de consumo dos afro brasileiros, n&uacute;meros de valores de consumo dos afro americanos com viagens pelo mundo, a&ccedil;&otilde;es de acolhida dentro do setor de hospedagem e a coloca&ccedil;&atilde;o de necessidade de aprimoramento de capita;c&atilde;o de parceiros para a amplia&ccedil;&atilde;o de a&ccedil;&otilde;es de trabalho dentro da categoria.</p>\r\n<p>O presidente do Instituto Angolano de Com&eacute;rcio Exterior e Media&ccedil;&atilde;o, senhor JOEL CARLOS fez tamb&eacute;m sua apresenta&ccedil;&atilde;o, mostrando com muito conhecimento que o mercado de rela&ccedil;&otilde;es comerciais entre o Brasil e os pa&iacute;ses de L&iacute;ngua Portuguesa pode crescer ainda mais com a integra&ccedil;&atilde;o cultural e do turismo. As oportunidades podem ser muito maiores e o crescimento econ&ocirc;mico not&oacute;rio. Esta proje&ccedil;&atilde;o &eacute; real, o mercado de Turismo &Eacute;tnico &eacute; uma riqueza a ser trabalhada e este evento inicia esta a&ccedil;&atilde;o direta.que trar&aacute; muito benef&iacute;cios para os quilombolas e para as<br />comunidades no Brasil.</p>\r\n<p>Ao final o presidente da ANTAB, FRANCISCO HENRIQUE SILVINO fez sua fala destacando que se faz necess&aacute;rio que os negros venham a ser reconhecidos como protagonistas dentro do turismo nacional, porque a maioria dos atrativos de destaque no cen&aacute;rio s&atilde;o atividades e realiza&ccedil;&otilde;es feitas e idealizadas pela cultura afro brasileira. Ele<br />citou que o pouco reconhecimento das a&ccedil;&otilde;es da comunidade negra, que na sua maioria s&atilde;o de fam&iacute;lias pobres e humildades, das periferias na uni&atilde;o ocorrer porque n&atilde;o s&atilde;o feitos projetos para que valorize a cultura e a hist&oacute;ria dos negros. Ele afirmou que se faz necess&aacute;rio que a negritude ocupe espa&ccedil;os na sociedade em todas as &aacute;reas.<br />Disse tamb&eacute;m que s&oacute; haver&aacute; o reconhecimento da import&acirc;ncia dos negros na sociedade, quando houver a vis&atilde;o de que a pr&oacute;pria ra&ccedil;a negra tem que ser gestora da sua arte e de tudo aquilo que produz.</p>\r\n<p>Ele destacou ainda o projeto GUETTO BRASIL HOLDING, que foi apresentado a todos os participantes do evento e que ser&aacute; efetuado junto as FAVELAS e QUILOMBOS no estado de S&atilde;o Paulo e Bahia, atrav&eacute;s de a&ccedil;&otilde;es de empreendedorismo social e qualifica&ccedil;&atilde;o profissional.</p>\r\n<p>Tamb&eacute;m fez cita&ccedil;&atilde;o a necessidade de se haver investimentos por parte da iniciativa privada e do governo nas idealiza&ccedil;&otilde;es e atividades que venham a propiciar a forma&ccedil;&atilde;o e capacita&ccedil;&atilde;o de pessoas dentro das &aacute;reas de favelas e quilombos.</p>\r\n<p>Em conversa com o presidente da FCP, ficou definido que haver&aacute; de forma imediata o inicio do trabalho conjunto entre todas as entidades e representa&ccedil;&otilde;es que estiveram no evento J&aacute; sendo determinado que a segunda edi&ccedil;&atilde;o do ENCONTRO INTERNACIONAL DE CULTURA, NEG&Oacute;CIOS E TURISMO &Eacute;TNICO, ser&aacute; feita em Novembro de 2018<br />na Assembleia Legislativa dos Deputados do Estado de S&atilde;o Paulo.</p>\r\n<p>Ficou definido tamb&eacute;m que ocorrer&aacute; AUDI&Ecirc;NCIAS P&Uacute;BLICAS em estados e munic&iacute;pios brasileiros, sobre o seguimento de TURISMO &Eacute;TNICO, EMPREENDEDORISMO E AFRO NEG&Oacute;CIO, sempre com a meta de mostrar os diversos atrativos e oportunidades que este setor pode trazer para as comunidades.</p>\r\n<p>A regulamenta&ccedil;&atilde;o de servi&ccedil;os, a realiza&ccedil;&atilde;o de eventos de qualifica&ccedil;&atilde;o profissional na &aacute;rea de turismo; a cobran&ccedil;a junto ao governo de a&ccedil;&otilde;es igualit&aacute;rias para os negros; a inclus&atilde;o do seguimento afro nas entidades e comiss&otilde;es do<br />setor em todo o Brasil, a busca de capita&ccedil;&atilde;o de receita junto aos governos para a implementa&ccedil;&atilde;o do setor; a cria&ccedil;&atilde;o de cotas que obriguem o mercado a ter negros e afro descendentes em todas as atividades organizadas no cen&aacute;rio de turismo no pa&iacute;s e a cria&ccedil;&atilde;o de linha de cr&eacute;dito para afro descendentes que queiram iniciar atividades comerciais dentro de favelas e quilombos; s&atilde;o metas que ser&atilde;o direcionadas e trabalhadas pelo Grupo de Trabalho do &ldquo; GUETTO BRASIL &ldquo; formado no evento para representar as comunidades nas FAVELAS, QUILOMBOS e o TURISMO &Eacute;TNICO no Brasil.</p>\r\n<p>O evento se encerrou &aacute;s 18:20 horas, com a delibera&ccedil;&atilde;o de que os trabalhos do GT GUETTO BRASIL se iniciam j&aacute; a partir deste dia 05 de Dezembro com a busca de condi&ccedil;&otilde;es para a participa&ccedil;&atilde;o de representantes da ANTAB e da</p>\r\n<p>ABHOSTELS na reuni&atilde;o do Conselho Nacional de Turismo em Brasilia.<br />A assinatura do TERMO DE PARCERIA entre a<br />ANTAB e a ABHOSTELS, ocorrer&aacute; dentro de uma<br />reuni&atilde;o do GT GUETTO BRASIL, que ser&aacute; agendada<br />para ocorrer na segunda quinzena do m&ecirc;s de<br />Dezembro de 2017.</p>\r\n<p> Em anexo segue a lista de composi&ccedil;&atilde;o dos<br />integrantes do GRUPO DE TRABALHO GUETTO<br />BRASIL</p>\r\n<p>Assinam o presente documento as lideran&ccedil;as que<br />realizaram todo o planejamento do evento<br />ENCONTRO INTERNACIONAL DE CULTURA,<br />NEG&Oacute;CIOS E TURISMO &Eacute;TNICO</p>\r\n<p>_________________ ___________________<br />FRANCISCO H. SILVINO JO&Atilde;O PAULO AMRIM<br />ANTAB ABHOSTELS</p>\r\n<p>_________________ ____________________<br />BERNABE MANUEL JOEL CARLOS<br />BERNATUR TURISMO INSTITUTO ANGOLANO</p>', '2019-09-03 21:54:44'),
(11, 'default.jpg', 'turismo-gera-mais-empregos-que-bancos-e-mineracao', 'Turismo gera mais empregos que bancos e mineração', '<div class=\"section the_content has_content\">\r\n<div class=\"section_wrapper\">\r\n<div class=\"the_content_wrapper\">\r\n<p>Segundo estudo do World Travel &amp; Tourism Council (WTTC), o Turismo &eacute; um dos setores que mais empregam no mundo. Somente em 2016, foram 108 milh&otilde;es de vagas diretas e um total de 292 milh&otilde;es entre diretas e indiretas.</p>\r\n<p>A expressividade empregat&iacute;cia coloca o segmento &agrave; frente da ind&uacute;stria automotiva (16 milh&otilde;es), do setor banc&aacute;rio (30 milh&otilde;es), da minera&ccedil;&atilde;o (28 milh&otilde;es), &aacute;rea qu&iacute;mica (20 milh&otilde;es) e do sistema financeiro (62 milh&otilde;es). Ou seja, o Turismo tem quatro vezes mais vagas que a &aacute;rea qu&iacute;mica e seis vezes mais que a produ&ccedil;&atilde;o de autom&oacute;veis.</p>\r\n<p>&nbsp;</p>\r\n<p>O setor gerou US$ 7,6 trilh&otilde;es para o Produto Interno Bruto (PIB) mundial, ou 10,2% do total, contribuindo mais do que os bancos (US$ 4,8 trilh&otilde;es), minera&ccedil;&atilde;o (US$ 5 trilh&otilde;es), agricultura (US$ 5,8 trilh&otilde;es), autom&oacute;veis (US$ 6,1 trilh&otilde;es) e qu&iacute;mica (US$ 6,5 trilh&otilde;es). O Turismo &eacute; superado apenas pelos segmentos de constru&ccedil;&atilde;o (US$ 10,3 trilh&otilde;es), financeiro (US$ 14,5 trilh&otilde;es) e varejo (US$ 18,1 trilh&otilde;es).</p>\r\n<p>Para se ter uma ideia da relev&acirc;ncia nas Am&eacute;ricas, que inclui Norte, Sul e Central, o Turismo &eacute; respons&aacute;vel pela gera&ccedil;&atilde;o de 42 milh&otilde;es de postos de trabalho. Ao todo, o setor gera uma economia de US$ 2,2 trilh&otilde;es, abaixo apenas de constru&ccedil;&atilde;o, financeiro e varejo na regi&atilde;o.</p>\r\n<p>Nos pr&oacute;ximos dez anos, calcula o WTTC, a ind&uacute;stria de Viagens e Turismo ir&aacute; crescer 4% mundialmente, acima dos 2,7% prospectados para o cen&aacute;rio global, com exce&ccedil;&atilde;o de bancos e finan&ccedil;as.</p>\r\n<p>&ldquo;&Eacute; f&aacute;cil aplaudir os esfor&ccedil;os ou at&eacute; mesmo criticar as quedas da ind&uacute;stria sem olhar o espectro da nossa ind&uacute;stria separada do contexto industrial como um todo. O Turismo gera 10,2% do PIB global e contribui com um entre dez empregos [&hellip;] Ainda h&aacute; desafios para o crescimento sustent&aacute;vel, e o WTTC vai continuar a alertar os setores p&uacute;blico e privado a investir e implementar pol&iacute;ticas favor&aacute;veis ao crescimento enquanto resguardam os ativos mundiais&rdquo;, declarou o presidente e diretor executivo do &oacute;rg&atilde;o, David Scowsill.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"section section-post-footer\">&nbsp;</div>\r\n<div class=\"section section-post-about\">&nbsp;</div>', '2019-09-03 21:55:23'),
(12, 'abrefestival-1024x683-1024x683.jpg', 'mais-de-50-caravanas-estao-confirmadas-para-o-festival-das-cataratas', 'Mais de 50 caravanas estão confirmadas para o Festival das Cataratas', '<h4>O 12&ordm; Festival das Cataratas dever&aacute; receber este ano um n&uacute;mero recorde de caravanas de diversas regi&otilde;es do Brasil e do exterior. S&atilde;o 52 j&aacute; confirmadas, que juntas somam mais de duas mil pessoas provenientes de mais de 60 munic&iacute;pios de diferentes regi&otilde;es brasileiras, al&eacute;m de outros tr&ecirc;s pa&iacute;ses: Uruguai, Paraguai e Argentina.</h4>\r\n<h4>Somente do estado de S&atilde;o Paulo, est&atilde;o programadas sete caravanas, provenientes da capital, S&atilde;o Jos&eacute; do Rio Preto, Franca, Campinas e Bauru. No percurso, elas ainda receber&atilde;o participantes de pelo menos mais quatro munic&iacute;pios: Ribeir&atilde;o Preto, Sorocaba, Mar&iacute;lia e Presidente Prudente.</h4>\r\n<h4>Do Rio Grande do Sul, est&atilde;o confirmadas caravanas de Porto Alegre, Rio Grande e Pelotas, com passagem por Canoas, Lajeado, Bento Gon&ccedil;alves, Passo Fundo, Erechim, Santa Maria e Cruz Alta. Do Paran&aacute;, al&eacute;m de Foz do Igua&ccedil;u e regi&atilde;o, j&aacute; est&atilde;o confirmados representantes de Curitiba, Londrina, Campo Mour&atilde;o, Ubirat&atilde;, Cascavel, Toledo, Ponta Grossa, Guarapuava, Laranjeiras do Sul, Irati e Paranagu&aacute;.</h4>\r\n<h4>Ao todo, 11 estados est&atilde;o representados nas caravanas: Paran&aacute;, Santa Catarina, Rio Grande do Sul, Minas Gerais, Goi&aacute;s, Rio de Janeiro, Rio Grande no Norte, Pernambuco, Para&iacute;ba, Maranh&atilde;o, Mato Grosso do Sul.</h4>\r\n<p>&nbsp;</p>\r\n<h4>O Festival das Cataratas ser&aacute; realizado nos dias 28, 29 e 30 de junho, no Rafain Palace Hotel &amp; Convention Center, em Foz do Igua&ccedil;u (PR). Em 2016, mais de sete mil pessoas participaram do evento, entre agentes de viagens, operadores, hoteleiros e outros fornecedores e profissionais de turismo, al&eacute;m de estudantes e professores da &aacute;rea. As caravanas do Festival s&atilde;o organizadas em conjunto com as operadoras de turismo parceiras do evento.</h4>\r\n<h4><strong>12&ordm; Festival das Cataratas</strong></h4>\r\n<h4>O Festival das Cataratas &eacute; uma realiza&ccedil;&atilde;o da Secretaria Municipal de Turismo, com organiza&ccedil;&atilde;o da De Angeli.<br />Dias: 28, 29 e 30 de junho de 2017<br />Local: Rafain Palace Hotel &amp; Convention Foz do Igua&ccedil;u &ndash; PR &ndash; BR<br /><a href=\"tel:(45)%203029-6603\" target=\"_blank\" rel=\"noopener\">045 3029 6603</a>&nbsp;/ 99975 2402</h4>', '2019-09-03 21:56:35'),
(13, 'mae-e-filha-viajam-de-carro-por-do-solistock-531314652.jpg', 'a-embratur-que-o-brasil-precisa', 'A Embratur que o Brasil precisa', '<p>O Congresso Nacional tem, neste momento, a oportunidade de contribuir de maneira definitiva para a redu&ccedil;&atilde;o do desemprego, o aumento da renda da popula&ccedil;&atilde;o e a gera&ccedil;&atilde;o de divisas para o pa&iacute;s. Essa oportunidade est&aacute; na aprova&ccedil;&atilde;o de mudan&ccedil;as que impactar&atilde;o positivamente no crescimento da ind&uacute;stria do turismo, setor que movimenta 52 segmentos da economia, na sua maioria (90%) pequenas e micro empresas.</p>\r\n<p>Falo do Projeto de Lei 7425 de 2015, que transforma a Embratur (respons&aacute;vel pela promo&ccedil;&atilde;o e apoio &agrave; comercializa&ccedil;&atilde;o dos destinos, servi&ccedil;os e produtos tur&iacute;sticos brasileiros no exterior) em uma entidade com total controle governamental, mas com modelo jur&iacute;dico de servi&ccedil;o social aut&ocirc;nomo. Ou seja, a proposta permitir&aacute; que, ao inv&eacute;s de ficar restrita totalmente aos repasses da Uni&atilde;o, a Embratur possa fazer conv&ecirc;nios, e buscar outras fontes de recursos.</p>\r\n<p>N&atilde;o estamos aqui falando de privatiza&ccedil;&atilde;o, uma vez que ela ficar&aacute; submetida a um contrato de gest&atilde;o com o Minist&eacute;rio do Turismo, bem como aos &oacute;rg&atilde;os de controle, a um conselho fiscal e a auditorias interna e externa. O que estamos propondo &eacute; que, nessa nova modalidade, a Embratur n&atilde;o fique mais ref&eacute;m do teto e do contingenciamento do Or&ccedil;amento da Uni&atilde;o. Ela voltar&aacute; a ter aportes de recursos, que lhe colocar&aacute; em patamar de competitividade com institui&ccedil;&otilde;es-refer&ecirc;ncia em promo&ccedil;&atilde;o tur&iacute;stica no mundo, a exemplo do Chile, M&eacute;xico, EUA, da Argentina, Fran&ccedil;a, Inglaterra e Alemanha.</p>\r\n<p>Com a restri&ccedil;&atilde;o or&ccedil;ament&aacute;ria que t&ecirc;m as autarquias (modelo atual da Embratur), os investimentos para promo&ccedil;&atilde;o tur&iacute;stica internacional ca&iacute;ram de US$ 112 milh&otilde;es, em 2011, para US$ 17 milh&otilde;es, no &uacute;ltimo ano. Enquanto a Argentina investiu US$ 60 milh&otilde;es, a Col&ocirc;mbia US$ 100 milh&otilde;es e o M&eacute;xico US$ 400 milh&otilde;es. Pa&iacute;ses esses, que s&atilde;o pr&oacute;ximos ao Brasil e com caracter&iacute;sticas tur&iacute;sticas parecidas com as nossas.</p>\r\n<p>No ano passado, atra&iacute;mos 6,6 milh&otilde;es de visitantes de outros pa&iacute;ses enquanto o M&eacute;xico, que possui algumas belezas naturais semelhantes ao Brasil, recebeu 35 milh&otilde;es de visitantes. Para sairmos desse patamar, &eacute; preciso investir em promo&ccedil;&atilde;o tur&iacute;stica, uma vez que cada turista internacional injeta, em m&eacute;dia, US$ 1 mil em nossa economia e o turismo &eacute; um grande gerador de emprego. Conforme o Conselho Mundial de Turismo de Viagem, o setor &eacute; respons&aacute;vel por um em cada 10 empregos no mundo.</p>\r\n<p>O Minist&eacute;rio do Turismo estabeleceu uma meta de atrair 12 milh&otilde;es de turistas estrangeiros at&eacute; 2021 e gerar receita de US$ 19 bilh&otilde;es. S&oacute; a alcan&ccedil;aremos com uma transforma&ccedil;&atilde;o dr&aacute;stica da Embratur!</p>\r\n<p>Conforme o texto do Projeto 7425/15, 0,75% do pr&ecirc;mio das loterias federais ser&aacute; destinado a Embratur, sem preju&iacute;zo as demais destina&ccedil;&otilde;es como a Seguridade Social, o FIES, o Minist&eacute;rio do Esporte, os Fundos Penitenci&aacute;rio e da Cultura, os Clubes de Futebol, a CBF, o Comit&ecirc; Paral&iacute;mpico, a Apae, a Cruz Vermelha e o Imposto de Renda.</p>\r\n<p>A proposta, que estamos analisando na C&acirc;mara dos Deputados, est&aacute; sendo aperfei&ccedil;oada com emendas parlamentares. Emendas que preveem a possibilidade de novas fontes de recursos para a Embratur, sem onerar o bolso do cidad&atilde;o e sem o uso de novos recursos p&uacute;blicos.</p>\r\n<p>&Eacute; preciso dizer tamb&eacute;m que a mudan&ccedil;a n&atilde;o afetar&aacute; os servidores de carreira da Embratur. Eles manter&atilde;o a condi&ccedil;&atilde;o de estatut&aacute;rios, com a preserva&ccedil;&atilde;o de todos os direitos e da estabilidade. Al&eacute;m disso, aqueles que optarem, e tiverem condi&ccedil;&otilde;es t&eacute;cnicas para atuar na nova ag&ecirc;ncia, ter&atilde;o a possibilidade de maior mobilidade funcional, inclusive com sal&aacute;rios maiores.</p>\r\n<p>Falando em nome dos mais de 250 parlamentares que comp&otilde;em a Frente Parlamentar Mista em Defesa do Turismo no Congresso Nacional, a qual presido, estou muito confiante na aprova&ccedil;&atilde;o deste projeto na C&acirc;mara e certo de que os senadores tamb&eacute;m ser&atilde;o sens&iacute;veis &agrave; necessidade imprescind&iacute;vel de mudan&ccedil;a no modelo de gest&atilde;o da Embratur.</p>', '2019-09-03 21:57:35'),
(14, 'capturar.JPG', 'vai-ter-carnaval-sim', 'Vai ter Carnaval sim!', '<p>Vai ter carnaval SIM!<br />Reconhecemos a import&acirc;ncia da festa popular e vamos continuar incentivando o carnaval carioca. O repasse da prefeitura para as escolas de<br />samba para o Carnaval 2018 vai chega a R$ 13 Milh&otilde;es.</p>\r\n<p>E este n&atilde;o ser&aacute; o &uacute;nico investimento na tradiconal festa popular. Todos os anos, o munic&iacute;pio tem um gasto anual alto com a mantun&ccedil;&atilde;o de estrutura<br />do Samb&oacute;dromo. Em 2017, s&oacute; para arcar com os custos da ilumina&ccedil;&atilde;o da Passarela do Samva nos dias de desfile, foram desembolsados R$: 655 mil.<br />Al&eacute;m disso, efetivos dos mais diversos &oacute;rg&atilde;os s&atilde;o mobilizados para garantir o sucesso da opera&ccedil;&atilde;o do evento.</p>\r\n<p>Esclarecemos que o remanejamento de uma parte da verba destinada &aacute;s escolas de samba do Grupo Especial foi definido em virtude das limita&ccedil;&otilde;es<br />or&ccedil;amentarias do Rio. A Redu&ccedil;&atilde;o de fastos tamb&eacute;m foi adotada em todos os &oacute;rg&atilde;os e contratos.</p>', '2019-09-03 22:05:59');
INSERT INTO `blogs` (`id`, `arquivo`, `slug`, `titulo`, `texto`, `created`) VALUES
(15, '17684278.jpg', 'maratona-internacional-de-floripa-ja-tem-3-mil-atletas-inscritos', 'Maratona Internacional de Floripa já tem 3 mil atletas inscritos', '<p>Faltando dois meses para a largada da Maratona Internacional de Florian&oacute;polis, 3.000 atletas j&aacute; confirmaram sua inscri&ccedil;&atilde;o. A prova est&aacute; marcada para o dia 27 de agosto nas dist&acirc;ncias da 42km (individual e dupla), 10 e 5 km com largada no trapiche da Avenida Beira-Mar Norte. As inscri&ccedil;&otilde;es podem ser feitas no site&nbsp;<a href=\"http://www.maratonadefloripa.com.br/\">www.maratonadefloripa.com.br</a>. Os n&uacute;meros d&atilde;o credibilidade a competi&ccedil;&atilde;o confirmando a capital catarinense como um excelente destino para o turismo esportivo.</p>\r\n<p>&ldquo;S&atilde;o eventos como a Maratona Internacional de Floripa que buscamos cada vez mais. Ameniza a sazonalidade, trazendo turistas para a cidade fora das f&eacute;rias. Isso movimenta nossa rede hoteleira e gastron&ocirc;mica, e gera servi&ccedil;os, al&eacute;m de aliar o turismo a uma atividade f&iacute;sica. S&atilde;o turistas que t&ecirc;m chances de retornar e indicar Florian&oacute;polis para outras pessoas conhecerem&rdquo;, comemora Vinicius de Luca Filho, Superintendente de Turismo de Florian&oacute;polis. At&eacute; o momento, quase 70% dos atletas inscritos s&atilde;o de fora de Santa Catarina.</p>\r\n<p>Conhecida como Ilha da Magia, Florian&oacute;polis &eacute; um reduto muito procurado por pessoas do mundo inteiro pelas belezas naturais e &oacute;timas op&ccedil;&otilde;es de lazer. E h&aacute; algum tempo a cidade tamb&eacute;m &eacute; refer&ecirc;ncia na realiza&ccedil;&atilde;o de eventos esportivos, especialmente os disputados ao ar livre. No caso da Maratona Internacional de Floripa, o apelo &eacute; ainda maior, pois a cidade conta com &oacute;tima infraestrutura e um percurso considerado como um dos mais r&aacute;pidos do Brasil.</p>\r\n<p>&ldquo;A gente sabia que a prova tinha um potencial enorme j&aacute; nesse primeiro ano. As maratonas est&atilde;o crescendo bastante no pa&iacute;s e Santa Catarina n&atilde;o poderia ficar de fora do calend&aacute;rio nacional sem nenhum evento desse porte. Floripa &eacute; o local perfeito para fazermos essa prova. Clima propicio, altimetria quase totalmente plana, belezas &iacute;mpares, diversas op&ccedil;&otilde;es de lazer e uma &oacute;tima rede hoteleira e gastron&ocirc;mica. N&atilde;o &eacute; &agrave; toa que boa parte dos atletas inscritos sejam de outros pa&iacute;ses e cidades&rdquo;, explica Anderson Tonon, organizador do evento.</p>\r\n<p>&ldquo;Estamos bem atentos a todos os detalhes para que os atletas possam ter uma experi&ecirc;ncia incr&iacute;vel durante a prova. Ser&atilde;o 19 postos de hidrata&ccedil;&atilde;o, um a cada quase 2 quil&ocirc;metros, al&eacute;m de centenas de staffs trabalhando ao longo do percurso. Teremos tendas de massagens para os atletas, uma &oacute;tima estrutura para quem corre e tamb&eacute;m para que assiste. Enfim, estamos pensando em todos os detalhes para entregar um evento de excel&ecirc;ncia&rdquo;, acrescenta Anderson Tonon.</p>', '2019-09-03 22:06:37'),
(16, '20170621-danca-tradiciona-festa-spedro.jpg', 'ubatuba-divulga-programacao-da-94a-festa-de-sao-pedro-pescador', 'Ubatuba divulga programação da 94ª Festa de São Pedro Pescador', '<p>A tradicional Festa de S&atilde;o Pedro Pescador de Ubatuba, considerada o maior evento do munic&iacute;pio, acontece de 28 de junho a 02 de julho na Pra&ccedil;a de Eventos, no Centro. Organizada h&aacute; 94 anos, a festa mais uma vez apresenta uma programa&ccedil;&atilde;o diversa e cheia de atra&ccedil;&otilde;es, que incluem a barraca da tainha, prociss&atilde;o mar&iacute;tima, corrida de canoas, apresenta&ccedil;&otilde;es de grupos tradicionais, shows locais e um grande show de encerramento com Negritude Junior.</p>\r\n<p>A festa &eacute; uma realiza&ccedil;&atilde;o da Prefeitura Municipal de Ubatuba por meio da Funda&ccedil;&atilde;o de Arte e Cultura de Ubatuba, em parceria com a Par&oacute;quia Exalta&ccedil;&atilde;o da Santa Cruz, a Col&ocirc;nia Z10 de Pescadores, a Associa&ccedil;&atilde;o dos Amigos e Remadores da Canoa Cai&ccedil;ara (AARCCA) e o Fundo Social de Solidariedade. O patroc&iacute;nio &eacute; do Governo do Estado de S&atilde;o Paulo, por meio da Secretaria Estadual de Cultura e da Companhia Municipal de Turismo (COMTUR).</p>\r\n<p><strong>Confira abaixo a programa&ccedil;&atilde;o completa:</strong></p>\r\n<p><strong>28/06 (quarta-feira)</strong><br />19h Prociss&atilde;o do Mastro de S&atilde;o Pedro Pescador (sa&iacute;da Pra&ccedil;a Exalta&ccedil;&atilde;o &agrave; Santa Cruz)<br />19h Prociss&atilde;o de Canoas Cai&ccedil;aras (sa&iacute;da prainha do Matarazzo)<br />20h Abertura oficial do evento &ndash; Palco principal<br />20h30 Trio Acalantos &ndash; Palco principal<br />21h30 Concurso Rainha dos Pescadores 2017 &ndash; Palco principal</p>\r\n<p><strong>29/06 (quinta-feira) &ldquo;Dia de S&atilde;o Pedro Pescador&rdquo;</strong><br />12h Almo&ccedil;o na Barraca da Tainha<br />14h Prociss&atilde;o mar&iacute;tima e a &ldquo;Ben&ccedil;&atilde;o dos Anzois&rdquo; (sa&iacute;da da Ilha dos Pescadores)<br />15h Missa campal &ndash; Palco principal<br />19h Congada de Bast&otilde;es de S&atilde;o Benedito do Puruba &ndash; Palco &ldquo;Vila Cai&ccedil;ara&rdquo;<br />20h Lu&iacute;s Perequ&ecirc; com &ldquo;Show P&eacute; de Moleque&rdquo; &ndash; Palco principal<br />21h Maverick Viana e Banda &ndash; Palco principal<br />22h30 Leandro Ara&uacute;jo e Banda Fam&iacute;lia &ndash; Palco principal</p>\r\n<p><strong>30/06 (sexta-feira)</strong><br />12h Almo&ccedil;o na Barraca da Tainha<br />19h Banda Lira Padre Anchieta apresenta &ldquo;Direto da F&aacute;brica&rdquo; &ndash; Palco principal<br />20h Fandango Cai&ccedil;ara &ndash; Palco &ldquo;Vila Cai&ccedil;ara&rdquo;<br />21h Banda Jump &ndash; Palco principal<br />22h30 Banda Kauze &ndash; Palco principal</p>\r\n<p><strong>Julho/2017</strong><br /><strong>01/07 (s&aacute;bado)</strong><br />9h Chegada da Folia do Divino na Ilha dos Pescadores<br />12h Almo&ccedil;o na Barraca da Tainha<br />18h Exibi&ccedil;&atilde;o do document&aacute;rio &ldquo;Filhos do Mar, a comunidade cai&ccedil;ara de Ubatuba&rdquo; &ndash; Sobrad&atilde;o do Porto<br />19h Apresenta&ccedil;&otilde;es de Puxada de Rede e Quadrilha Curti&ccedil;o do Itagu&aacute;<br />20h Dan&ccedil;a da Fita do Itagu&aacute; &ndash; Palco &ldquo;Vila Cai&ccedil;ara&rdquo;<br />20h30 Leil&atilde;o de Remos na &ldquo;Vila Cai&ccedil;ara&rdquo;<br />21h Banda Bicho Pregui&ccedil;a &ndash; Palco principal<br />22h30 Banda Naguetta &ndash; Palco principal</p>\r\n<p><strong>02/07 (domingo)</strong><br />9h Corrida de Canoas Cai&ccedil;aras &ndash; Praia do Cruzeiro<br />12h Almo&ccedil;o na Barraca da Tainha<br />12h30 Entrega da premia&ccedil;&atilde;o corrida de canoas cai&ccedil;ara e soltura de tartarugas marinhas com o Projeto Tamar<br />18h Coro da Lira &ndash; Palco principal<br />19h Grupo &ldquo;Os Cai&ccedil;aras&rdquo; de Paraty-RJ &ndash; Palco &ldquo;Vila Cai&ccedil;ara&rdquo;<br />20h Banda Suhai Rock &ndash; Palco principal<br />21h30 Negritude Junior &ndash; Palco principal</p>', '2019-09-03 22:07:21'),
(17, 'turismonorio.jpg', 'campanha-incentiva-carioca-a-fazer-turismo-no-rio-de-janeiro', 'Campanha incentiva carioca a fazer turismo no Rio de Janeiro', '<section class=\"social-sharing-top\">\r\n<div class=\"social-comments comment-click-82249\">Com o objetivo de incentivar o carioca a ser um turista dentro da pr&oacute;pria cidade, a Riotur lan&ccedil;ou a terceira fase da campanha de comunica&ccedil;&atilde;o visual de fomento ao turismo. Desde o in&iacute;cio desta semana, a nova a&ccedil;&atilde;o j&aacute; pode ser vista nos principais pontos tur&iacute;sticos do Rio. &ldquo;Fa&ccedil;a Turismo no Rio&rdquo;, &ldquo;Viva o Rio&rdquo; e &ldquo;Viva a cidade&rdquo; s&atilde;o os slogans impressos em imagens ic&ocirc;nicas, tais como Cristo Redentor, Maracan&atilde;, Ipanema e Floresta da Tijuca, entre outros.</div>\r\n</section>\r\n<div>&nbsp;</div>\r\n<div>A expectativa &eacute; que, ao estimular o morador a visitar mais os pontos tur&iacute;sticos, haja um fortalecimento da economia &ndash; afinal, a cidade conta com 6,5 milh&otilde;es de habitantes. Sem falar na gera&ccedil;&atilde;o de emprego. Somente no R&eacute;veillon passado, que caiu em um fim de semana (ou seja, foram apenas dois dias de pico de movimenta&ccedil;&atilde;o), a economia da cidade foi impactada em US$ 691 milh&otilde;es com a vinda de 865 mil turistas.</div>\r\n<div>&nbsp;</div>\r\n<div>\r\n<div>&ldquo;Queremos incentivar o carioca a conhecer melhor a pr&oacute;pria cidade. Quantos moradores daqui ainda n&atilde;o conhecem o AquaRio? Quantos ainda n&atilde;o visitaram o Cristo Redentor? Nossa ideia &eacute; instigar o carioca a ver de perto a beleza e as riquezas pr&oacute;ximas da sua casa. Esta &eacute; uma forma tamb&eacute;m de fazer os moradores se apaixonarem ainda mais pelo Rio&rdquo;, explica o presidente Marcelo Alves. &ldquo;Sem falar no desejo nosso de usar a criatividade para movimentar a economia do munic&iacute;pio&rdquo;, completa Alves.</div>\r\n<div>&nbsp;</div>\r\n<div>Desde o come&ccedil;o da gest&atilde;o Marcelo Crivella, a Riotur trabalha com um plano de marketing elaborado com o foco em uma enorme comunica&ccedil;&atilde;o dentro da pr&oacute;pria cidade, mostrando a import&acirc;ncia de se tratar bem o turista. &Agrave;s v&eacute;speras do Carnaval, os principais acessos tur&iacute;sticos da cidade receberam galhardetes com as inscri&ccedil;&otilde;es &ldquo;Carioca, abrace um turista&rdquo; e &ldquo;Carioca, receba bem o turista&rdquo;. &ldquo;Nosso povo &eacute; alegre, &eacute; carism&aacute;tico, &eacute; um bom anfitri&atilde;o. O desejo, naquele momento, foi pedir que ele cuidasse bem do turista, pois se o turista for bem tratado, ele volta. E n&atilde;o volta sozinho&rdquo;, afirma o presidente da Riotur. &ldquo;Precisamos entender que o turismo &eacute; uma solu&ccedil;&atilde;o para a cidade&rdquo;, completa.</div>\r\n<div>&nbsp;</div>\r\n<div>Com o tema &ldquo;O maior cart&atilde;o-postal do Rio &eacute; o carioca&rdquo;, a segunda fase da campanha apresentou, em maio, personagens identificados com a cidade, representando diferentes setores de servi&ccedil;os e da cadeia produtiva do turismo, como o gari Renato Sorriso; a inspetora da Guarda Municipal, Tatiana Mendes; o taxista e mestre de bateria da Unidos da Tijuca, Casagrande; o porteiro h&aacute; 40 anos do Copacabana Palace, Caf&uacute;; al&eacute;m de um gar&ccedil;om e uma camareira que h&aacute; d&eacute;cadas atendem com o encantamento do carioca os nossos turistas. O motivo? Pesquisas feitas com turistas revelaram que a volta deles para a cidade se deve muito &agrave; forma como s&atilde;o bem recebidos pelo carioca e a associa&ccedil;&atilde;o imediata entre o clima descontra&iacute;do do Rio e a simpatia do seu povo.</div>\r\n<div>&nbsp;</div>\r\n<div>&ldquo;A Riotur ser&aacute; incans&aacute;vel em a&ccedil;&otilde;es de fomento ao turismo. Vamos valorizar o povo carioca, pela sua hospitalidade, e mostrar ao turista que aqui ele ser&aacute; sempre bem tratado&rdquo;, diz Marcelo Alves.</div>\r\n</div>', '2019-09-03 22:08:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `depoimentos`
--

CREATE TABLE `depoimentos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `cargo` varchar(255) NOT NULL,
  `empresa` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `arquivo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `depoimentos`
--

INSERT INTO `depoimentos` (`id`, `nome`, `cargo`, `empresa`, `texto`, `arquivo`) VALUES
(2, 'Joao Luiz', 'CEO', 'Grupo ACV', 'A ABHostels está sempre conseguindo boas parcerias que nos ajudam a reduzir custos, inovar com novas tecnologias, aumentar nossas vendas, e além disso está sempre nos representando nos órgãos públicos, lutando por nosso espaço no mercado. ', 'avatar-2.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `destinos`
--

CREATE TABLE `destinos` (
  `id` int(11) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `link_destinos` varchar(500) DEFAULT NULL,
  `link_pontos` varchar(500) DEFAULT NULL,
  `link_gastronomia` varchar(500) DEFAULT NULL,
  `arquivo` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `destinos`
--

INSERT INTO `destinos` (`id`, `cidade`, `slug`, `link_destinos`, `link_pontos`, `link_gastronomia`, `arquivo`, `created`) VALUES
(1, 'São Paulo', 'sao-paulo', '', 'http://cidadedesaopaulo.com/v2/?lang=pt', 'https://www.tripadvisor.com.br/Attraction_Review-g303631-d8842016-Reviews-Free_Food_Tour-Sao_Paulo_State_of_Sao_Paulo.html', 'saopaulo-2.jpg', '2019-08-02 18:32:44'),
(2, 'Florianópolis', 'florianopolis', NULL, NULL, NULL, 'florianopolis.jpg', '2019-08-02 18:42:43'),
(3, 'Belo Horizonte', 'belo-horizonte', NULL, NULL, NULL, 'belohorizonte.jpg', '2019-08-02 18:42:53'),
(4, 'Manaus', 'manaus', 'https://freesider.com.br/viajar/conheca-agora-os-melhores-destinos-da-amazonia/', 'https://www.estadosecapitaisdobrasil.com/amazonas/pontos-turisticos-do-amazonas/', 'http://www.amazonas.am.gov.br/o-amazonas/gastronomia/', 'praia-de-ponta-negra-manaus.jpg', '2019-08-02 00:00:00'),
(5, 'Rio de Janeiro', 'rio-de-janeiro', '', '', '', '01-rio-de-janeiro-4.jpg', '2019-08-31 00:38:21'),
(6, 'Ubatuba', 'ubatuba', '', '', '', 'ubatuba-goins-3.jpg', '2019-08-31 00:40:08'),
(7, 'Curitiba', 'curitiba', '', '', '', 'jardim-botanico-de-curitiba.jpg', '2019-08-31 00:43:29'),
(8, 'Buzios', 'buzios', '', '', '', 'buzios.jpg', '2019-08-31 00:45:02'),
(9, 'Paraty', 'paraty', '', '', '', '5-motivos-para-visitar-paraty-rio-de-janeiro-viagem-dn-3.jpg', '2019-08-31 00:46:41'),
(10, 'Foz do Iguaçu', 'foz-do-iguacu', '', '', '', 'pr-foz-do-iguacu-cataratas-credito-shutterstock-337634303-1.jpg', '2019-08-31 00:47:34'),
(11, 'Fernando de Noronha', 'fernando-de-noronha', '', '', '', 'capa-onde-fica-fernando-de-noronha-1.jpg', '2019-08-31 00:50:02'),
(12, 'Salvador', 'salvador', '', '', '', 'mapa-turistico-de-salvador-salvador.jpg', '2019-08-31 01:02:15'),
(13, 'Vitória', 'vitoria', '', '', '', 'praia-de-camburi-vitoria-setur-yuri-barichivich.jpg', '2019-08-31 01:03:30'),
(14, 'Brasília', 'brasilia', '', '', '', 'brasilia.jpg', '2019-08-31 01:05:03'),
(15, 'Goiânia', 'goiania', '', '', '', 'centro-oeste-1-1.jpg', '2019-08-31 01:06:18'),
(16, 'Recife', 'recife', '', '', '', 'ground-zero-2014135-1280.jpg', '2019-08-31 01:08:29'),
(17, 'Fortaleza', 'fortaleza', '', '', '', 'a-cidade-de-fortaleza.jpg', '2019-08-31 01:09:15'),
(18, 'Petrópolis', 'petropolis', '', '', '', 'catedral-de-petropolis-rio-de-janeiro.jpeg', '2019-08-31 01:09:58'),
(19, 'Porto Alegre', 'porto-alegre', '', '', '', 'porto-alegre.jpg', '2019-08-31 01:12:10'),
(20, 'Natal', 'natal', '', '', '', 'natal-rn1.jpg', '2019-08-31 01:13:34'),
(21, 'Cuiabá', 'cuiaba', '', '', '', 'gopr0004-mp4-16-26-14-09-still006.jpg', '2019-08-31 01:15:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE `fotos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `fotos`
--

INSERT INTO `fotos` (`id`, `nome`, `arquivo`, `created`) VALUES
(1, 'RJ', 'rj.jpg', '2019-08-06 22:18:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `created`) VALUES
(1, 'renato.cast@gmail.com', '2019-01-30 18:40:55'),
(2, 'ceo@azulcomverde.com.br', '2019-01-30 18:40:59'),
(3, 'sugestao848@gmail.com', '2019-04-09 12:41:55'),
(4, 'marketing700@live.com', '2019-04-13 05:03:27'),
(5, 'promocao368@msn.com', '2019-04-15 18:52:03'),
(6, 'softwarewhatscontacts@gmail.com', '2019-04-18 01:48:49'),
(7, 'premiosebrae@gmail.com', '2019-04-18 04:28:59'),
(8, 'oportunidade269@hotmail.com', '2019-04-18 14:07:19'),
(9, 'promover871@live.com', '2019-04-22 11:16:55'),
(10, 'promocao935@terra.com.br', '2019-04-22 23:04:35'),
(11, 'divulgaformulario@gmail.com', '2019-04-23 05:35:32'),
(12, 'gdetrafego@gmail.com', '2019-04-24 03:04:34'),
(13, 'promovebox@gmail.com', '2019-04-24 15:47:15'),
(14, 'grupolistaparedencao@gmail.com', '2019-04-26 15:10:01'),
(15, 'catarina445@msn.com', '2019-04-26 18:00:13'),
(16, 'madalenanogueira5897@gmail.com', '2019-04-28 18:44:28'),
(17, 'henrique120@gmail.com', '2019-05-05 23:07:52'),
(18, 'mariaisis_anuncio917@live.com', '2019-05-06 22:40:12'),
(19, 'lucca851@gmail.com', '2019-05-07 21:00:07'),
(20, 'divulgamaisbb@gmail.com', '2019-05-10 15:02:48'),
(21, 'naorespondaaqui@email.com', '2019-05-13 22:53:22'),
(22, 'vendas203@promovebox.info', '2019-05-15 08:08:39'),
(23, 'jandokabr@gmail.com', '2019-05-15 13:08:20'),
(24, '', '2019-05-26 15:26:36'),
(26, 'qingquanlong-zyq@qqllight.com', '2019-05-31 11:32:13'),
(27, 'cjustisilva@gmail.com', '2019-06-03 17:15:38'),
(29, 'rodrigo.seomarketing@gmail.com', '2019-06-06 06:18:33'),
(30, 'agencia@hd3publicidade.com.br', '2019-06-06 08:26:24'),
(31, 'thiago.sferreira@fco.net.br', '2019-06-27 14:28:47'),
(32, 'leonardoalkk@gmail.com', '2019-06-28 14:30:02'),
(33, 'terbioss@gmail.com', '2019-07-01 03:17:28'),
(34, 'clods-7@outlook.com', '2019-07-03 22:52:04'),
(35, 'sprotasios@gmail.com', '2019-07-17 22:35:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `parceiros`
--

CREATE TABLE `parceiros` (
  `id` int(11) NOT NULL,
  `arquivo` varchar(255) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `site` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `parceiros`
--

INSERT INTO `parceiros` (`id`, `arquivo`, `nome`, `texto`, `site`, `created`) VALUES
(7, '2-2.jpg', 'AME Digital', 'Todo associado ABHostels terá 10% de cashback e taxa de 1,89% no crédito.', 'https://www.amedigital.com/', '2019-08-31 01:20:44'),
(8, 'booles.jpg', 'Booles Tecnologia', 'Todo Associado ABHostels terá 50% de desconto nos planos da Booles Tecnologia.', 'http://booles.com.br/br/', '2019-08-31 01:22:43'),
(10, '1.jpg', 'Abresei gastronomia, hospedagem e turismo ', '', 'http://abresi.com.br/', '2019-09-03 21:23:46'),
(11, 'cntur.jpg', 'CNTur Confederação Nacional do Turismo', '', 'http://cntur.com.br/', '2019-09-03 21:26:43'),
(12, '2.jpg', 'São Paulo Turismo Eventos - Culturais  - Negócios', '', 'http://spturis.com/v7/', '2019-09-03 21:30:19'),
(13, 'cultura-centralizado.jpg', 'Prefeitura de São Paulo', '', 'http://www.cultura.sp.gov.br/', '2019-09-03 21:41:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `socios`
--

CREATE TABLE `socios` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `empresa` varchar(255) NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `alvara` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `socios`
--

INSERT INTO `socios` (`id`, `nome`, `empresa`, `telefone`, `email`, `alvara`, `created`) VALUES
(1, 'Teste Teste', 'Teste', '(11) 11111-1111', 'teste@fatosocial.com', 'logos.jpg', '2019-09-04 09:48:27'),
(2, 'Teste Teste', 'Teste', '(11) 11111-1111', 'teste@fatosocial.com', 'logos-2.jpg', '2019-09-04 09:48:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `role` int(11) NOT NULL,
  `active` enum('S','N') NOT NULL DEFAULT 'S'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`, `created`, `modified`, `role`, `active`) VALUES
(1, 'renato.cast@gmail.com', 'cac1db82abd7c29808619fa69eec93a7c4b26d42', 'Renato Castanheiro', 'renato.cast@gmail.com', '2014-08-27 00:00:00', '2018-08-09 20:09:42', 0, 'S'),
(2, 'ceo@azulcomverde.com.br', '606aa86f55ab138dd4ebec1caad6cafe8dab3b66', 'João', 'ceo@azulcomverde.com.br', '2018-12-12 17:54:37', '2018-12-12 17:54:37', 1, 'S'),
(3, 'thiago.sferreira@fco.net.br', '786c4bbd4109d13d3f2a2f7ae2bbe62172b151cc', 'Thiago de Souza', 'thiago.sferreira@fco.net.br', '2019-03-13 19:19:46', '2019-03-13 19:19:46', 1, 'S');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `associados`
--
ALTER TABLE `associados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depoimentos`
--
ALTER TABLE `depoimentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destinos`
--
ALTER TABLE `destinos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `x_email` (`email`);

--
-- Indexes for table `parceiros`
--
ALTER TABLE `parceiros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `associados`
--
ALTER TABLE `associados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `depoimentos`
--
ALTER TABLE `depoimentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `destinos`
--
ALTER TABLE `destinos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `fotos`
--
ALTER TABLE `fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `parceiros`
--
ALTER TABLE `parceiros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `socios`
--
ALTER TABLE `socios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
